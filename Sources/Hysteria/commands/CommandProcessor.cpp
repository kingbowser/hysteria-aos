#include "CommandProcessor.h"

#include <Hysteria/StringTools.h>

namespace Hysteria {

    using CommandPair   = std::pair<String, Command*>;
    using StringVector  = std::vector<String>;

    using ClientPlayer  = spades::client::ClientPlayer;

    void CommandProcessor::addCommand(Command* command) {
        this->commands.insert( CommandPair(command->getCommandName(), command) );
    }

    Command* CommandProcessor::getCommand(String name) const {
        return this->commands.at(name);
    }

    void CommandProcessor::removeCommand(String const commandName) {
        this->commands.erase(commandName);
    }

    CommandReturnState CommandProcessor::dispatchCommand(String const commandline, ClientPlayer* player) const {
        StringVector lineTokens = SplitString(commandline);
        Command* destination    = this->getCommand(lineTokens[0]);

        if (destination) {
            StringVector argumentTokens(lineTokens.begin() + 1, lineTokens.end());
            return (*destination)(argumentTokens, player);
        } else {
            throw CommandNotFoundException(lineTokens[0], commandline);
        }
    }
}
