#pragma once

#include <string>
#include <vector>

#include "../../Client/ClientPlayer.h"

namespace Hysteria {

    using String        = std::string;
    using StringVector  = std::vector<String>;

    using ClientPlayer  = spades::client::ClientPlayer;

    enum CommandReturnState {
        OK,
        FAILURE
    };

    class Command {

    public:

        virtual String getCommandName() const = 0;

        virtual CommandReturnState operator() (StringVector&, ClientPlayer*) const = 0;

    };

}

