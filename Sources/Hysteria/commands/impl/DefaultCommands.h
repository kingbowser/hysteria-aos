#pragma once

#include <Hysteria/commands/Command.h>

namespace Hysteria {
    namespace Commands {

        /**
         * Teleport to given coordinates.
         * Arguments are formated like so:
         *
         * [+|-]\d [+|-]\d [+|-]\d
         *
         * In the order of of X, Y, and Z (Up/Down)
         *
         * If a sign is specified, the provided coordinate is considered relative to the current coordinates.
         * If no sign is specified, the corresponding coordinate will be set the the specified value.
         *
         * The character '.' will be treated as '+0' if specified in place of a parameter.
         *
         * Each parameter is optional, but still positional.
         *
         * To teleport five blocks upward, the parameters would be `. . -5`
         */
        class TeleportCommand : public Command {
            virtual String getCommandName() const;

            virtual CommandReturnState operator()(StringVector&, ClientPlayer*) const;
        };

        /**
         * Teleport to a player, attempt to kill them (using your weapon), and then teleport back
         *
         * The players name must be specified.
         */
        class TelekillCommand : public Command {
            virtual String getCommandName() const;

            virtual CommandReturnState operator()(StringVector&, ClientPlayer*) const;
        };

        class TeleplayerCommand : public Command {
            virtual String getCommandName() const;

            virtual CommandReturnState operator()(StringVector&, ClientPlayer*) const;
        };

        /**
         * When intel is available on the map (and not held by another player), this will teleport to the intel,
         * capture it, and then teleport to your teams base.
         *
         * No parameters are specified.
         */
        class TelecaptureCommand : public Command {
            virtual String getCommandName() const;

            virtual CommandReturnState operator()(StringVector&, ClientPlayer*) const;
        };

        /**
         * Teleport to the nearest home base
         */
        class TelehomeCommand : public Command {
            virtual String getCommandName() const;

            virtual CommandReturnState operator()(StringVector&, ClientPlayer*) const;
        };

        /**
         * Set variables
         */
        class SetCommand : public Command {
            virtual String getCommandName() const;

            virtual CommandReturnState operator()(StringVector&, ClientPlayer*) const;
        };

    }
}
