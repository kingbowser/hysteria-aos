#include <Client/Client.h>
#include <Client/World.h>
#include <Client/NetClient.h>

// Team information
#include <Client/CTFGameMode.h>

// Settings API
#include <Core/Settings.h>

#include <Hysteria/Hysteria.h>
#include <Hysteria/StringTools.h>
#include <Hysteria/commands/impl/DefaultCommands.h>

#include <iostream>
#include <stack>

namespace Hysteria {
    namespace Commands {

        using String        = std::string;
        using StringVector  = std::vector<String>;
        using ClientPlayer  = spades::client::ClientPlayer;
        using Player        = spades::client::Player;
        using Vector3       = spades::Vector3;
        using World         = spades::client::World;
        using OutOfRange    = std::out_of_range;

        static inline bool StringFindIgnoreCase(String const hay, String const needle) {
            String lowercaseHay = StringToLowerCase(hay);
            String lowercaseNeedle = StringToLowerCase(needle);

            return lowercaseHay.find(lowercaseNeedle) != String::npos;
        }

        static inline Player* FindPlayerByName(String const name, World* world) {
            for (Player* worldPlayer : world->GetPlayers()) {
                if (worldPlayer) {
                    if (StringFindIgnoreCase(worldPlayer->GetName(), name)) {
                        return worldPlayer;
                    }
                }
            }

            return nullptr;
        }

        // Teleport implementation

        String TeleportCommand::getCommandName() const {
            return "tppos";
        }

        static inline bool IsRelativeParameter(String const param) {
            char const sign = param[0];
            return sign == '-' || sign == '+';
        }

        static inline float ApplyParameterWithBase(String const param, float const base) {
            static String const IGNORE_POSITIONAL = ".";

            if (param == IGNORE_POSITIONAL) {
                return base;
            } else if (IsRelativeParameter(param)) {
                float const modifier = atof(param.c_str());
                return base + modifier;
            } else {
                return atof(param.c_str());
            }
        }

        CommandReturnState TeleportCommand::operator()(StringVector& args, ClientPlayer* clientPlayer) const {

            Player* player          = clientPlayer->GetPlayer();
            Vector3 playerPosition  = player->GetPosition();

            float xDestination = playerPosition.x;
            float yDestination = playerPosition.y;
            float zDestination = playerPosition.z;

            try {
                String xPositional = args.at(0);
                xDestination = ApplyParameterWithBase(xPositional, xDestination);
            } catch (OutOfRange) {}

            try {
                String yPositional = args.at(1);
                yDestination = ApplyParameterWithBase(yPositional, yDestination);
            } catch (OutOfRange) {}

            try {
                String zPositional = args.at(2);
                zDestination = ApplyParameterWithBase(zPositional, zDestination);
            } catch (OutOfRange) {}

            {
                Vector3 newPosition(xDestination, yDestination, zDestination);
                player->SetPosition(newPosition);
            }

            return OK;

        }

        // Telekill implementation

        using PlayerStack   = std::stack<Player*>;

        String TelekillCommand::getCommandName() const {
            return "telefrag";
        }

        CommandReturnState TelekillCommand::operator()(StringVector& args, ClientPlayer* clientPlayer) const {
            using PlayerSet  = std::set<Player*>;

            try {
                PlayerSet players;
                World* world = clientPlayer->GetPlayer()->GetWorld();

                // This is inefficient and has _logarithmic_ complexity (!!)
                for (Player* worldPlayer : world->GetPlayers()) {
                    if (worldPlayer) {
                        std::cout << "Checking player " << worldPlayer->GetName() << std::endl;

                        for (String nameSpec : args) {
                            std::cout << "\t" << nameSpec;

                            if (StringFindIgnoreCase(worldPlayer->GetName(), nameSpec)
                                && worldPlayer != clientPlayer->GetPlayer()
                                && (players.find(worldPlayer) == players.end())) {

                                std::cout << " [matched]";

                                players.insert(worldPlayer);
                            }

                            std::cout << std::endl;
                        }
                    }
                }

                {
                    using Client = spades::client::Client;

                    Player* self = clientPlayer->GetPlayer();

                    // This is a naughty trick, but for all intents and purposes will never fail
                    Client* client = (Client*) self->GetWorld()->GetListener();
                    Vector3 const originalPosition = self->GetPosition();

                    // Kill em'
                    for (Player* target : players) {

                        std::cout << "Attacking target " << target->GetName() << std::endl;

                        // Move to that player's position and force a position update
                        self->SetPosition(target->GetPosition());
//                        client->GetNetClient()->SendPosition();

                        // Tell the server that we shot that player in the head
                        client->BulletHitPlayer(target, HitTypeHead, target->GetEye(), self);
                    }

                    // Go back to the original position
//                    self->SetPosition(originalPosition);
                }

                return OK;
            } catch (OutOfRange) {
                return FAILURE;
            }

        }

        // Teleplayer implementation

        String TeleplayerCommand::getCommandName() const {
            return "teleplayer";
        }

        CommandReturnState TeleplayerCommand::operator()(StringVector& args, ClientPlayer* clientPlayer) const {
            if (args.size() >= 1) {

                String const targetName = args.at(0);

                std::cout << "Trying to teleport to the first player matching " << targetName << std::endl;

                Player* targetPlayer = FindPlayerByName(targetName, clientPlayer->GetPlayer()->GetWorld());

                std::cout << "Found player at address " << targetPlayer << std::endl;

                if (targetPlayer) {
                    clientPlayer->GetPlayer()->SetPosition(targetPlayer->GetPosition());

                    return OK;
                }
            }

            return FAILURE;
        }

        // Telecapture implementation

        String TelecaptureCommand::getCommandName() const {
            return "telecapture";
        }

        CommandReturnState TelecaptureCommand::operator()(StringVector&, ClientPlayer* clientPlayer) const {
            using CTFMode = spades::client::CTFGameMode;
            using CTFTeam = CTFMode::Team;

            // Try to find the location of the intel in the world
            Player* localPlayer = clientPlayer->GetPlayer();
            World* world = localPlayer->GetWorld();
            if (world->GetMode()->ModeType() == spades::client::IGameMode::Mode::m_CTF) {
                CTFTeam const team = ((CTFMode*) world->GetMode())->GetTeam(1 - localPlayer->GetTeamId());
                CTFTeam const localTeam = ((CTFMode*) world->GetMode())->GetTeam(localPlayer->GetTeamId());

                // Only TP to the intel if nobody has it
                unless (localTeam.hasIntel) {
                    localPlayer->SetPosition(team.flagPos);
                }

                return OK;
            } else {
                return FAILURE;
            }
        }

        // Telehom implementation

        String TelehomeCommand::getCommandName() const {
            return "telehome";
        }

        CommandReturnState TelehomeCommand::operator()(StringVector&, ClientPlayer* clientPlayer) const {
            // Current only CTF is supported

            using CTFMode = spades::client::CTFGameMode;
            using CTFTeam = CTFMode::Team;

            Player* localPlayer = clientPlayer->GetPlayer();
            World* world = localPlayer->GetWorld();

            if (world->GetMode()->ModeType() == spades::client::IGameMode::Mode::m_CTF) {
                std::cout << "Preparing to teleport home (mode is CTF)" << std::endl;

                CTFTeam const playerTeam = ((CTFMode*) world->GetMode())->GetTeam(localPlayer->GetTeamId());

                std::cout << "Player team id is " << localPlayer->GetTeamId() << " at address " << (&playerTeam) << std::endl;

                localPlayer->SetPosition(playerTeam.basePos);

                std::cout << "Position set" << std::endl;

                return OK;
            } else {
                return FAILURE;
            }
        }

        // "set" command
        
        String SetCommand::getCommandName() const {
            return "set";
        }

        CommandReturnState SetCommand::operator()(StringVector& params, ClientPlayer* clientPlayer) const { 
            using Settings      = spades::Settings;
            using ItemHandle    = Settings::ItemHandle;

            if (params.size() > 1) {
                Settings* settings = Settings::GetInstance();

                String const settingName    = params.at(0);
                String const settingValue   = params.at(1);
            
                ItemHandle fuckYouYVTYouStupidBitch(settingName);
                fuckYouYVTYouStupidBitch = settingValue;

                return OK;
            } else {
                return FAILURE;
            }
        }
    }

    
}
