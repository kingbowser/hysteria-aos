#pragma once

#include "Command.h"
#include "../../Client/ClientPlayer.h"

#include <string>
#include <map>
#include <exception>

namespace Hysteria {

    using CommandMap    = std::map<String, Command*>;
    using String        = std::string;
    using Exception     = std::exception;

    using ClientPlayer  = spades::client::ClientPlayer;

    class CommandNotFoundException: public Exception {

    private:
        String const commandName;
        String const commandLine;

    public:
        CommandNotFoundException(String const name, String const line)
                : commandName(name),
                  commandLine(line)
        {}

        virtual char const* what() const throw() override {
            return ("The command `" + this->commandName + "` is not registered").c_str();
        }

        String const getCommandName() const {
            return commandName;
        }

        String const getCommandLine() const {
            return commandLine;
        }

    };

    class CommandProcessor {

    private:
        CommandMap commands;

    public:
        void addCommand(Command*);

        Command* getCommand(String const) const;

        void removeCommand(String const);

        /**
         * Calls a command by tokenizing the provided string
         * and looking up the command by the first token, and calling it with the rest
         */
        CommandReturnState dispatchCommand(String const, ClientPlayer* player) const;

    };
}

