/*
 *    
 * Hysteria for AoS is licensed under the GPL v3
 *
 * Hysteria     Copyright (C) King Bowser <king@bowser.pw>
 * OpenSpades   Copyright (C) yvt
 * 
 * I know very well that a part of the AoS community, and especially the majority
 * of AoS hackers are immature little shits that can't even form a sentence.
 * Now, I want you to know -- if you intend to add more hacks or rebrand this or 
 * whatever, that even though you could make your changes and never release the source,
 * doing that would make you an extremely terrible person, especially if you distribute
 * a compiled version, but not the source code.
 *
 * Speaking of which, you should know that you would be violating not only Hysteria's
 * license, but also OpenSpades in doing so. Bearing this in mind, you should also know
 * that it is still a violation of copyright for you to take credit for nearly all
 * open source software, and I will take any binary-only release of this to be a 
 * de-facto violation of not only my, but YVT's copyright, and will not hesitate
 * to carpet bomb every single file host with cease and desist notices.
 *
 * In addition to cease and desist notices, you may also receive the following
 * at your *personal* residence:
 *
 * * Emergency Plumbers
 * * Pizza
 * * Mormons
 * * Jehova's Witnesses
 * * Enourmous boxes of pamphlets
 * * Bibles
 * * Junk Mail
 * * Gay/Lesbian/Straight (opposite your orientation) fetish clothing catalogues
 * * Whatever else I can find
 *
 * This is primarily an incentive to discourage those that do wish to release
 * a binary-only copy from even taking the smallest bit of credit for it,
 * because people that do that are worse than Hitler.
 */
#pragma once

#include <Core/Settings.h>
#include <Client/PhysicsConstants.h>

#include <Hysteria/commands/CommandProcessor.h>

#define unless(predicate)           if (!(predicate))
#define until(predicate)            while (!(predicate))

#define repeat(times, var)          for(unsigned int var = 0; var < times; ++var)


namespace Hysteria {

    using Setting    = spades::Settings::ItemHandle;

    // Scope
    Setting extern const AimDelta;
    Setting extern const AimMovementTrim;
    Setting extern const AimMouseTrim;

    // Combat logic mods
    Setting extern const HeadShot;
    Setting extern const HBScale;
    Setting extern const GrenadeSpeedTrim;

    // Weapon mods
    Setting extern const NoRecoil;
    Setting extern const NoToolShake;
    Setting extern const NoGrenadeShake;
    Setting extern const AutoReload;
    Setting extern const FriendlyFire;
    Setting extern const WeaponsIgnoreMap;
    Setting extern const WeaponDistKill;
    Setting extern const WeaponDistBreak;

    // Wepon shot interval settings
    Setting extern const UniformCool;
    Setting extern const UniformCoolTrim;

    // Weapon spread settings
    Setting extern const UseUniformSpread;
    Setting extern const UniformSpread;
    Setting extern const RifleSpread;
    Setting extern const SMGSpread;
    Setting extern const SGSpread;

    // Weapon delay settings
    Setting extern const UseUniformDelay;
    Setting extern const UniformDelay;
    Setting extern const RifleDelay;
    Setting extern const SMGDelay;
    Setting extern const SGDelay;
    Setting extern const GrenadeDelay;

    Setting extern const GrenadeHoldTime;

    // View / World Mods
    Setting extern const RenderHitboxes;
    Setting extern const HitboxAlpha;
    Setting extern const MapCullDisable;
    Setting extern const MapCullDist;
    Setting extern const SphereCullCoef;

    // Player movement mods
    Setting extern const NoBobbing;
    Setting extern const NoBreathing;
    Setting extern const NoAirborneSlowdown;
    Setting extern const NoWadeSlowdown;
    Setting extern const RunAndGun;
    Setting extern const LockingSneak;
    Setting extern const NoSneakSlowdown;
    Setting extern const LockingCrouch;
    Setting extern const NoCrouchSlowdown;
    Setting extern const BaseMovementTrim;
    Setting extern const LockingSprint;
    Setting extern const SprintSpeedTrim;
    Setting extern const JumpVelocity;
    Setting extern const ZVelocityLimit;
    Setting extern const HorizontalNoclip;

    // Build hacks
    Setting extern const BlockPlaceLimit;
    Setting extern const IgnoreHeightLimit;
    Setting extern const SpadeDelay;
    Setting extern const SpadeDigDelay;
    Setting extern const BlockPlaceDelay;

    // Network tweaks
    Setting extern const MovementUpdateRate;

    /**
     * Utility functions for settings
     */

    static inline float GetWeaponSpread(WeaponType weaponType) {
        if (UseUniformSpread) {
            return UniformSpread;
        } else {
            switch (weaponType) {
                case RIFLE_WEAPON:
                    return RifleSpread;
                case SMG_WEAPON:
                    return SMGSpread;
                case SHOTGUN_WEAPON:
                    return SGSpread;
                default:
                    return UniformSpread;
            }
        }
    }

    static inline float GetWeaponDelay(WeaponType weaponType) {
        if (UseUniformDelay) {
            return UniformDelay;
        } else {
            switch (weaponType) {
                case RIFLE_WEAPON:
                    return RifleDelay;
                case SMG_WEAPON:
                    return SMGDelay;
                case SHOTGUN_WEAPON:
                    return SGDelay;
                default:
                    return UniformDelay;
            }
        }
    }

    /**
     * Commands
     */

    CommandProcessor extern commandProcessor;

    /**
     * Other stuff;
     */

    void InitHysteria();
}
