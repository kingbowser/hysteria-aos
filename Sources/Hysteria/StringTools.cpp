#include "StringTools.h"

#include <algorithm>
#include <functional>
#include <sstream>

namespace Hysteria {

    // Type imports

    using String        = std::string;
    using StringVector  = std::vector<String>;
    using StringStream  = std::stringstream;

    // Provided functions

    StringVector SplitString(String const target, char const delimiter) {
        StringVector   tokens;
        StringStream   tokenStream(target);

        {
            String token;

            while (std::getline(tokenStream, token, delimiter)) {
                if (token.length() > 0) {
                    tokens.push_back(token);
                }
            }
        }

        return tokens;
    }

    String StringToLowerCase(String const input) {
        String output;
        output.resize(input.size());

        ::std::transform(input.begin(), input.end(),
                         output.begin(), ::tolower);

        return output;
    }

}
