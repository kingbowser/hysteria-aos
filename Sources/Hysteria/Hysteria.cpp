#include <string>

#include <Hysteria/Hysteria.h>
#include <Hysteria/commands/impl/DefaultCommands.h>

namespace Hysteria {

    using Setting = spades::Settings::ItemHandle;

    // Scope
    Setting const AimDelta          ("cheat_aimDelta",                      "2.8",  "Scope Zoom Distance");
    Setting const AimMovementTrim   ("cheat_scopeMoveCoeffecient",          "1",    "Movement speed trim when aiming");
    Setting const AimMouseTrim      ("cheat_scopeMouseScalar",              "1",    "Adjusts the aim scale when modifying the mouse input");

    // Combat logic mods
    Setting const HeadShot          ("cheat_headShot",                      "1",    "Always get a headshot");

    /*
     * PySpades uses a tolerance coeffecient of five when determining whether or not you are looking at a player
     * Due to the absolutely attrocious nature of PySpades (what a little bastard child of bad C and C++ Simulator, I mean Python)
     */
    Setting const HBScale           ("cheat_hitboxScale",                   "1.5",  "Player hitbox size coeffecient");
    Setting const GrenadeSpeedTrim  ("cheat_grenadeVelocityTrim",           "1.0",  "Modifies the velocity of thrown grenades");

    // Weapon mods
    Setting const NoRecoil          ("cheat_noWeaponRecoil",                "1",    "Disable recoil");
    Setting const NoToolShake       ("cheat_noToolShake",                   "1",    "Disable view shaking when firing a weapon");
    Setting const NoGrenadeShake    ("cheat_noExplosionShake",              "1",    "Disable view shaking when a grenade detonates");
    Setting const AutoReload        ("cheat_autoReload",                    "1",    "Automatically reload the weapon when suitable");
    Setting const FriendlyFire      ("cheat_friendlyFire",                  "0",    "Allow killing of team members");
    Setting const WeaponsIgnoreMap  ("cheat_weaponsIgnoreMap",              "0",    "");
    Setting const WeaponDistKill    ("cheat_maxWeaponPlrDist",              "32",   "Maximum distance at which a bullet hit is registered");
    Setting const WeaponDistBreak   ("cheat_maxWeaponBlockDist",            "32",   "Maximum distance at which a block hit is registered");

    // Wepon shot interval settings
    Setting const UseUniformDelay   ("cheat_uniformWeaponDelayEnabled",     "0",    "If enabled, all weapons share a common cooldown period");
    Setting const UniformDelay      ("cheat_uniformWeaponDelay",            "0.3",  "Common weapon cooldown period");

    /*
     * The following delay values came from PySpades, which is hilariously permissive
     * I highly doubt that these values could be achieved by latency unless PySpades
     * has absolutely trash networking code (which would be in line with the rest of the damn thing)
     *
     * Weapon       OpenSpades  PySpades
     * Rifle    -   0.5         0.2
     * SMG      -   0.1         0.05
     * Shotgun  -   1.0         0.3 (!!!!!)
     */
    Setting const RifleDelay        ("cheat_rifleDelay",                    "0.2",  "Rifle shot interval");
    Setting const SMGDelay          ("cheat_smgDelay",                      "0.05", "SMG shot delay");
    Setting const SGDelay           ("cheat_shotgunDelay",                  "0.3",  "Shotgun delay");
    Setting const GrenadeDelay      ("cheat_grenadeDelay",                  "0.5",  "");

    Setting const GrenadeHoldTime   ("cheat_grenadeHoldTime",               "2.9",  "");

    // Weapon spread settings
    Setting const UseUniformSpread  ("cheat_uniformBulletSpreadEnabled",    "0",        "If enabled, all weapons share a common spread factor");
    Setting const UniformSpread     ("cheat_uniformBulletSpread",           "0",        "Common weapon spread factor");
    Setting const RifleSpread       ("cheat_rifleSpread",                   "0.006",    "Rifle spread factor");
    Setting const SMGSpread         ("cheat_smgSpread",                     "0.0125",   "SMG Spread factor");
    Setting const SGSpread          ("cheat_shotgunSpread",                 "0.024",    "Shotgun spread factor");

    // View / World Mods
    Setting const RenderHitboxes    ("cheat_playerHitboxes",                "1",    "");
    Setting const HitboxAlpha       ("cheat_playerHitboxesAlpha",           "0.75", "");
    Setting const MapCullDisable    ("cheat_noPlayerCulling",               "0",    "Disable distance culling of players");
    Setting const MapCullDist       ("cheat_playerCullDistance",            "128",  "Player Culling Distance");
    Setting const SphereCullCoef    ("cheat_sphereCullCoeffecient",         "1.5",  "Radius coeffecient for sphere culling");

    // Player movement mods
    Setting const NoBobbing         ("cheat_noViewBobbing",                 "0",    "Disable view bobbing animation");
    Setting const NoBreathing       ("cheat_noBreathPulse",                 "0",    "Disable breathing animation");
    Setting const NoAirborneSlowdown("cheat_noAirborneSlowdown",            "0",    "Disable airborne slownedown");
    Setting const NoWadeSlowdown    ("cheat_noWadeSlowdown",                "1",    "Disable water resistance");
    Setting const RunAndGun         ("cheat_runAndGun",                     "1",    "Allow running and shooting");
    Setting const LockingSneak      ("cheat_lockSneak",                     "0",    "Make sneak locking rather than transient");
    Setting const NoSneakSlowdown   ("cheat_noSneakSlowdown",               "0",    "Disable sneak slowdown");
    Setting const LockingCrouch     ("cheat_lockCrouch",                    "0",    "Make crouch locking rather than transient");
    Setting const NoCrouchSlowdown  ("cheat_noCrouchSlowdown",              "1",    "Disable crouch slowdown");
    Setting const BaseMovementTrim  ("cheat_moveCoeffecient",               "1",    "Modifier for the base movement speed");
    Setting const LockingSprint     ("cheat_lockingSprint",                 "0",    "Make sprint toggle");
    Setting const SprintSpeedTrim   ("cheat_sprintCoeffecient",             "1.3",  "Modifier for sprint speed");
    Setting const JumpVelocity      ("cheat_jumpVelocity",                  "0.36", "Modifier for jump height");
    Setting const HorizontalNoclip  ("cheat_horizontalNoclip",              "0",    "");

    // Build hacks
    Setting const BlockPlaceLimit   ("cheat_blockPlaceLimit",               "12",   "Distance limit for block placement");
    Setting const IgnoreHeightLimit ("cheat_blockIgnoreHeightLimit",        "0",    "");
    Setting const BlockPlaceDelay   ("cheat_blockPlaceDelay",               "0.5",  "");
    Setting const SpadeDelay        ("cheat_spadeDelay",                    "0.2",  "");
    Setting const SpadeDigDelay     ("cheat_spadeDigDelay",                 "1.0",  "");

    // Fall speed limit
    Setting const ZVelocityLimit    ("cheat_zVelocityLimit",                "0.20", "");

    Setting const MovementUpdateRate("cheat_movementUpdateRate",            "1.0",  "");

    /**
     * Command processor
     */
    CommandProcessor commandProcessor;

    void InitCommandProcessor() {
        commandProcessor.addCommand(new Commands::TeleportCommand());
        commandProcessor.addCommand(new Commands::TelekillCommand());
        commandProcessor.addCommand(new Commands::TeleplayerCommand());
        commandProcessor.addCommand(new Commands::TelecaptureCommand());
        commandProcessor.addCommand(new Commands::TelehomeCommand());
        commandProcessor.addCommand(new Commands::SetCommand());
    }

    void InitHysteria() {
        InitCommandProcessor();
    }

}
