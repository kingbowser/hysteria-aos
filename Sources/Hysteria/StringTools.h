#pragma once

#include <string>
#include <vector>

namespace Hysteria {

    // Type imports

    using String        = std::string;
    using StringVector  = std::vector<String>;


    // Provided functions

    StringVector SplitString(String const, char const = ' ');

    String StringToLowerCase(String const);

}
