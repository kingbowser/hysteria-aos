#pragma once

#include "../Core/Math.h"
#include "../Client/Player.h"

namespace Hysteria {
    using Player = spades::client::Player;

    static inline void ScaleHitboxes(Player::HitBoxes& hitBoxes, float scale) {
        using OBB3    = spades::OBB3;
        using Matrix4 = spades::Matrix4;

        Matrix4 const scaleFactor = Matrix4::Scale(scale);

        // Head and torso
        hitBoxes.head  = hitBoxes.head  * scaleFactor;  
        hitBoxes.torso = hitBoxes.torso * scaleFactor;

        // Limbs 1-4
        for (OBB3& limb : hitBoxes.limbs) {
            limb = limb * scaleFactor;
        }
    }

}
