/*
 Copyright (c) 2013 yvt
 based on code of pysnip (c) Mathias Kaerlev 2011-2012.
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "Client.h"

#include <Core/Settings.h>
#include <Core/Strings.h>

#include "IAudioChunk.h"
#include "IAudioDevice.h"

#include "ClientUI.h"
#include "PaletteView.h"
#include "LimboView.h"
#include "MapView.h"
#include "Corpse.h"

#include "World.h"
#include "Weapon.h"

#include "NetClient.h"

#include "../Hysteria/Hysteria.h"


SPADES_SETTING(cg_mouseSensitivity,      "1");
SPADES_SETTING(cg_zoomedMouseSensScale,  "0.6");
SPADES_SETTING(cg_mouseExpPower,         "1");
SPADES_SETTING(cg_invertMouseY,          "0");
SPADES_SETTING(cg_holdAimDownSight,      "0");
SPADES_SETTING(cg_keyAttack,             "LeftMouseButton");
SPADES_SETTING(cg_keyAltAttack,          "RightMouseButton");
SPADES_SETTING(cg_keyToolSpade,          "1");
SPADES_SETTING(cg_keyToolBlock,          "2");
SPADES_SETTING(cg_keyToolWeapon,         "3");
SPADES_SETTING(cg_keyToolGrenade,        "4");
SPADES_SETTING(cg_keyReloadWeapon,       "r");
SPADES_SETTING(cg_keyFlashlight,         "f");
SPADES_SETTING(cg_keyLastTool,           "");
SPADES_SETTING(cg_keyMoveLeft,           "a");
SPADES_SETTING(cg_keyMoveRight,          "d");
SPADES_SETTING(cg_keyMoveForward,        "w");
SPADES_SETTING(cg_keyMoveBackward,       "s");
SPADES_SETTING(cg_keyJump,               "Space");
SPADES_SETTING(cg_keyCrouch,             "Control");
SPADES_SETTING(cg_keySprint,             "Shift");
SPADES_SETTING(cg_keySneak,              "v");
SPADES_SETTING(cg_keyCaptureColor,       "e");
SPADES_SETTING(cg_keyGlobalChat,         "t");
SPADES_SETTING(cg_keyTeamChat,           "y");
SPADES_SETTING(cg_keyConsole,            "`");
SPADES_SETTING(cg_keyChangeMapScale,     "m");
SPADES_SETTING(cg_keyToggleMapZoom,      "n");
SPADES_SETTING(cg_keyScoreboard,         "Tab");
SPADES_SETTING(cg_keyLimbo,              "l");
SPADES_SETTING(cg_keyScreenshot,         "0");
SPADES_SETTING(cg_keySceneshot,          "9");
SPADES_SETTING(cg_keySaveMap,            "8");
SPADES_SETTING(cg_switchToolByWheel,     "1");
SPADES_SETTING(cg_debugCorpse,           "0");
SPADES_SETTING(cg_alerts,                "1");


namespace spades {
	namespace client {
		
		bool Client::WantsToBeClosed() {
			return readyToClose;
		}
		
		bool FirstPersonSpectate = false;
		
		void Client::Closing() {
			SPADES_MARK_FUNCTION();
		}

		bool Client::NeedsAbsoluteMouseCoordinate() {
			SPADES_MARK_FUNCTION();

			if (scriptedUI->NeedsInput()) {
				return true;
			} else if (not world) {
				return true;
			} else if (IsLimboViewActive()) {
				return true;
			} else {
			    return false;
            }
		}
		
		void Client::MouseEvent(float x, float y) {
			SPADES_MARK_FUNCTION();
			
			if (scriptedUI->NeedsInput()) {
				scriptedUI->MouseEvent(x, y);
				return;
			} else if (IsLimboViewActive()) {
				limbo->MouseEvent(x, y);
				return;
			} else if (IsFollowing()) {
				
                SPAssert(world != nullptr);
				
				x = -x;

				unless (cg_invertMouseY) {
					y = -y;
                }
				
				followYaw   -= x * 0.003F;
				followPitch -= y * 0.003F;

				if (followPitch < -M_PI * 0.45F) {
                    followPitch = -float(M_PI) * 0.45F;
                }

				if (followPitch > M_PI * 0.45F) {
                    followPitch = float(M_PI) * 0.45F;
                }

				followYaw = fmodf(followYaw, float(M_PI) * 2.0F);

			} else if (world && world->GetLocalPlayer()) {
				Player* p          = world->GetLocalPlayer();
				float aimDownState = GetAimDownState();

				if (p->IsAlive()) {

                    {
                        float const aimMouseScale = GetAimDownZoomScale() * float(Hysteria::AimMouseTrim);

                        x /= aimMouseScale;
                        y /= aimMouseScale;
                    }

                    {                    
                        float rad = powf(x, 2) + powf(y, 2);

                        if (rad > 0.0F) {

                            {
                                float const expPower = float(cg_mouseExpPower);
                                if (expPower < 0.001F || isnan(expPower)) {
                                    SPLog("Invalid cg_mouseExpPower value, resetting to 1.0");
                                    cg_mouseExpPower = 1.0F;
                                }
                            }

                            
                            rad = ({
                                    float const factor = powf(renderer->ScreenWidth() * 0.1F, 2);
                                    powf(rad / factor, float(cg_mouseExpPower) * 0.5F - 0.5F);
                            });

                            SPAssert(not isnan(rad));
                            
                            x *= rad;
                            y *= rad;

                        }
                    }
					
					if (aimDownState > 0.0F) {
						float const scale = powf(float(cg_zoomedMouseSensScale), aimDownState);
						x *= scale;
						y *= scale;
					}
				
                    {
                        float sensitivity = float(cg_mouseSensitivity);    
                        x *= sensitivity;
                        y *= sensitivity;
                    }
					
					if (cg_invertMouseY) {
						y = -y;
                    }
					
					p->Turn(x * 0.003F, y * 0.003F);

				}
			}
		}
		
		void Client::WheelEvent(float x, float y) {
			SPADES_MARK_FUNCTION();
			
			if (scriptedUI->NeedsInput()) {
				scriptedUI->WheelEvent(x, y);
				return;
			}
			
			if (y > 0.5F) {
				KeyEvent("WheelDown", true);
				KeyEvent("WheelDown", false);
			} else if (y < -0.5F) {
				KeyEvent("WheelUp", true);
				KeyEvent("WheelUp", false);
			}
		}
		
		void Client::TextInputEvent(const std::string &ch) {
			SPADES_MARK_FUNCTION();
			
			if (scriptedUI->NeedsInput() && !scriptedUI->isIgnored(ch)) {
				scriptedUI->TextInputEvent(ch);
				return;
			}
			
			// we don't get "/" here anymore
		}
		
		void Client::TextEditingEvent(const std::string &ch,
									  int start, int len) {
			SPADES_MARK_FUNCTION();
			
			if (scriptedUI->NeedsInput() && !scriptedUI->isIgnored(ch)) {
				scriptedUI->TextEditingEvent(ch, start, len);
				return;
			}
		}
		
		bool Client::AcceptsTextInput() {
			SPADES_MARK_FUNCTION();
			
			if (scriptedUI->NeedsInput()) {
				return scriptedUI->AcceptsTextInput();
			}
			return false;
		}
		
		AABB2 Client::GetTextInputRect() {
			SPADES_MARK_FUNCTION();
			if (scriptedUI->NeedsInput()) {
				return scriptedUI->GetTextInputRect();
			}
			return AABB2();
		}
		
		static bool CheckKey(const std::string& cfg,
							 const std::string& input) {
			if (cfg.empty())
				return false;

			static const std::string space1("space");
			static const std::string space2("spacebar");
			static const std::string space3("spacekey");

			if (EqualsIgnoringCase(cfg, space1) ||
				 EqualsIgnoringCase(cfg, space2) ||
				 EqualsIgnoringCase(cfg, space3)) {

					if (input == " ")
						return true;
					}
					else
					{
						if (EqualsIgnoringCase(cfg, input))
								return true;
					}
			return false;
		}
		
		void Client::KeyEvent(const std::string& name, bool down) {
			SPADES_MARK_FUNCTION();
			
			if (scriptedUI->NeedsInput()) {
				if (!scriptedUI->isIgnored(name)) {
					scriptedUI->KeyEvent(name, down);
				} else {
					if (!down) {
						scriptedUI->setIgnored("");
					}
				}
				return;
			}
			
			if (name == "Escape") {
				if (down) {
					if (inGameLimbo) {
						inGameLimbo = false;
					} else {
						if (GetWorld() == nullptr) {
							// no world = loading now.
							// in this case, abort download, and quit the game immediately.
							readyToClose = true;
						}
						else {
							scriptedUI->EnterClientMenu();
						}
					}
				}
			} else if (world) {
				if (IsLimboViewActive()) {
					if (down) {
						limbo->KeyEvent(name);
					}
					return;
				}
				if (IsFollowing()) {
					if (CheckKey(cg_keyAttack, name)) {
						if (down) {
							if (world->GetLocalPlayer()->GetTeamId() >= 2 ||
							   time > lastAliveTime + 1.3F)
								FollowNextPlayer(false);
						}
						return;
					} else if (CheckKey(cg_keyAltAttack, name)) {
						if (down) {
							if (world->GetLocalPlayer()->GetTeamId() >= 2) {
								// spectating
								followingPlayerId = world->GetLocalPlayerIndex();
							} else if (time > lastAliveTime + 1.3F) {
								// dead
								FollowNextPlayer(true);
							}
						}
						return;
					}
				}
				if (world->GetLocalPlayer()) {
					Player *p = world->GetLocalPlayer();
					
					if (p->IsAlive() && p->GetTool() == Player::ToolBlock && down) {
						if (paletteView->KeyInput(name)) {
							return;
						}
					}
					
					if (cg_debugCorpse) {
						if (name == "p" && down) {
							Corpse *corp;
							Player *victim = world->GetLocalPlayer();
							corp = new Corpse(renderer, map, victim);
							corp->AddImpulse(victim->GetFront() * 32.0F);
							corpses.emplace_back(corp);
							
							if (corpses.size() > corpseHardLimit) {
								corpses.pop_front();
							} else if (corpses.size() > corpseSoftLimit) {
								RemoveInvisibleCorpses();
							}
						}
					}
					if (CheckKey(cg_keyMoveLeft, name)) {
						playerInput.moveLeft = down;
						keypadInput.left = down;
						if (down) playerInput.moveRight = false;
						else playerInput.moveRight = keypadInput.right;
					} else if (CheckKey(cg_keyMoveRight, name)) {
						playerInput.moveRight = down;
						keypadInput.right = down;
						if (down) playerInput.moveLeft = false;
						else playerInput.moveLeft = keypadInput.left;
					} else if (CheckKey(cg_keyMoveForward, name)) {
						playerInput.moveForward = down;
						keypadInput.forward = down;
						if (down) playerInput.moveBackward = false;
						else playerInput.moveBackward = keypadInput.backward;
					} else if (CheckKey(cg_keyMoveBackward, name)) {
						playerInput.moveBackward = down;
						keypadInput.backward = down;
						if (down) playerInput.moveForward = false;
						else playerInput.moveForward = keypadInput.forward;
					} else if (CheckKey(cg_keyCrouch, name)) {
                        if (Hysteria::LockingCrouch) {
                            playerInput.crouch = !playerInput.crouch;
                        } else {
    						playerInput.crouch = down;
                        }
					} else if (CheckKey(cg_keySprint, name)) {
                        if (Hysteria::LockingSprint) {
    						playerInput.sprint = !playerInput.sprint;
                        } else {
                            playerInput.sprint = down;
                        }

						if (playerInput.sprint && not(Hysteria::RunAndGun)) {
							if (world->GetLocalPlayer()->IsToolWeapon()) {
								weapInput.secondary = false;
							}
						}

					} else if (CheckKey(cg_keySneak, name)) {
                        if (Hysteria::LockingSneak) {
    						playerInput.sneak = !playerInput.sneak;
                        } else {
                            playerInput.sneak = down;
                        }
					} else if (CheckKey(cg_keyJump, name)) {
						if (down) {
							FirstPersonSpectate = !FirstPersonSpectate;
						}
						playerInput.jump = down;
					} else if (CheckKey(cg_keyAttack, name)) {
						weapInput.primary = down;
					} else if (CheckKey(cg_keyAltAttack, name)) {
						auto lastVal = weapInput.secondary;
						if (world->GetLocalPlayer()->IsToolWeapon() && down) {
                            weapInput.secondary = !weapInput.secondary;
						} else {
                            weapInput.secondary = down;
						}
						if (world->GetLocalPlayer()->IsToolWeapon() && weapInput.secondary && !lastVal &&
						   world->GetLocalPlayer()->IsReadyToUseTool()) {
							AudioParam params;
							params.volume = 0.08F;
							Handle<IAudioChunk> chunk = audioDevice->RegisterSound("Sounds/Weapons/AimDownSightLocal.wav");
							audioDevice->PlayLocal(chunk, MakeVector3(0.4F, -0.3F, 0.5F), params);
						}
					} else if (CheckKey(cg_keyReloadWeapon, name) && down) {
						Weapon *w = world->GetLocalPlayer()->GetWeapon();
						if (w->GetAmmo() < w->GetClipSize() &&
						   w->GetStock() > 0 &&
						   (!world->GetLocalPlayer()->IsAwaitingReloadCompletion()) &&
						   (!w->IsReloading()) &&
						   world->GetLocalPlayer()->GetTool() == Player::ToolWeapon) {
							if (world->GetLocalPlayer()->IsToolWeapon()) {
                                if (weapInput.secondary) {
                                    // if we send WeaponInput after sending Reload,
                                    // server might cancel the reload.
                                    // https://github.com/infogulch/pyspades/blob/895879ed14ddee47bb278a77be86d62c7580f8b7/pyspades/server.py#343
                                    hasDelayedReload = true;
                                    weapInput.secondary = false;
                                    return;
                                }
							}
							world->GetLocalPlayer()->Reload();
							net->SendReload();
						}
					} else if (CheckKey(cg_keyToolSpade, name) && down) {
						if (world->GetLocalPlayer()->GetTeamId() < 2 &&
						   world->GetLocalPlayer()->IsAlive() &&
						   world->GetLocalPlayer()->IsToolSelectable(Player::ToolSpade)) {
							SetSelectedTool(Player::ToolSpade);
						}
					} else if (CheckKey(cg_keyToolBlock, name) && down) {
						if (world->GetLocalPlayer()->GetTeamId() < 2 &&
						   world->GetLocalPlayer()->IsAlive()
						   ) {
							if (world->GetLocalPlayer()->IsToolSelectable(Player::ToolBlock)) {
								SetSelectedTool(Player::ToolBlock);
							} else {
								if (cg_alerts)
									ShowAlert(_Tr("Client", "Out of Blocks"), AlertType::Error);
								else
									PlayAlertSound();
							}
						}
					} else if (CheckKey(cg_keyToolWeapon, name) && down) {
						if (world->GetLocalPlayer()->GetTeamId() < 2 &&
						   world->GetLocalPlayer()->IsAlive()) {
							if (world->GetLocalPlayer()->IsToolSelectable(Player::ToolWeapon)) {
								SetSelectedTool(Player::ToolWeapon);
							} else {
								if (cg_alerts)
									ShowAlert(_Tr("Client", "Out of Ammo"), AlertType::Error);
								else
									PlayAlertSound();
							}
						}
					} else if (CheckKey(cg_keyToolGrenade, name) && down) {
						if (world->GetLocalPlayer()->GetTeamId() < 2 &&
						   world->GetLocalPlayer()->IsAlive()) {
							if (world->GetLocalPlayer()->IsToolSelectable(Player::ToolGrenade)) {
								SetSelectedTool(Player::ToolGrenade);
							} else {
								if (cg_alerts)
									ShowAlert(_Tr("Client", "Out of Grenades"), AlertType::Error);
								else
									PlayAlertSound();
							}
						}
					} else if (CheckKey(cg_keyLastTool, name) && down) {
						if (hasLastTool &&
						   world->GetLocalPlayer()->GetTeamId() < 2 &&
						   world->GetLocalPlayer()->IsAlive() &&
						   world->GetLocalPlayer()->IsToolSelectable(lastTool)) {
							hasLastTool = false;
							SetSelectedTool(lastTool);
						}
					} else if (CheckKey(cg_keyGlobalChat, name) && down) {
						// global chat
						scriptedUI->EnterGlobalChatWindow();
						scriptedUI->setIgnored(name);
					} else if (CheckKey(cg_keyConsole, name) && down) {
                        scriptedUI->EnterConsoleWindow();
                        scriptedUI->setIgnored(name);
                    } else if (CheckKey(cg_keyTeamChat, name) && down) {
						// team chat
						scriptedUI->EnterTeamChatWindow();
						scriptedUI->setIgnored(name);
					} else if (name == "/" && down) {
						// command
						scriptedUI->EnterCommandWindow();
						scriptedUI->setIgnored(name);
					} else if (CheckKey(cg_keyCaptureColor, name) && down) {
						CaptureColor();
					} else if (CheckKey(cg_keyChangeMapScale, name) && down) {
						mapView->SwitchScale();
						Handle<IAudioChunk> chunk = audioDevice->RegisterSound("Sounds/Misc/SwitchMapZoom.wav");
						audioDevice->PlayLocal(chunk, AudioParam());
					} else if (CheckKey(cg_keyToggleMapZoom, name) && down) {
						if (largeMapView->ToggleZoom()) {
							Handle<IAudioChunk> chunk = audioDevice->RegisterSound("Sounds/Misc/OpenMap.wav");
							audioDevice->PlayLocal(chunk, AudioParam());
						} else {
							Handle<IAudioChunk> chunk = audioDevice->RegisterSound("Sounds/Misc/CloseMap.wav");
							audioDevice->PlayLocal(chunk, AudioParam());
						}
					} else if (CheckKey(cg_keyScoreboard, name)) {
						scoreboardVisible = down;
					} else if (CheckKey(cg_keyLimbo, name) && down) {
						limbo->SetSelectedTeam(world->GetLocalPlayer()->GetTeamId());
						limbo->SetSelectedWeapon(world->GetLocalPlayer()->GetWeapon()->GetWeaponType());
						inGameLimbo = true;
					} else if (CheckKey(cg_keySceneshot, name) && down) {
						TakeScreenShot(true);
					} else if (CheckKey(cg_keyScreenshot, name) && down) {
						TakeScreenShot(false);
					} else if (CheckKey(cg_keySaveMap, name) && down) {
						TakeMapShot();
					} else if (CheckKey(cg_keyFlashlight, name) && down) {
						flashlightOn = !flashlightOn;
						flashlightOnTime = time;
						Handle<IAudioChunk> chunk = audioDevice->RegisterSound("Sounds/Player/Flashlight.wav");
						audioDevice->PlayLocal(chunk, AudioParam());
					} else if (cg_switchToolByWheel && down) {
						bool rev = (int)cg_switchToolByWheel > 0;
						if (name == (rev ? "WheelDown":"WheelUp")) {
							if (world->GetLocalPlayer()->GetTeamId() < 2 &&
							   world->GetLocalPlayer()->IsAlive()) {
								Player::ToolType t = world->GetLocalPlayer()->GetTool();
								do {
									switch (t) {
										case Player::ToolSpade:
											t = Player::ToolGrenade;
											break;
										case Player::ToolBlock:
											t = Player::ToolSpade;
											break;
										case Player::ToolWeapon:
											t = Player::ToolBlock;
											break;
										case Player::ToolGrenade:
											t = Player::ToolWeapon;
											break;
									}
								} while (!world->GetLocalPlayer()->IsToolSelectable(t));
								SetSelectedTool(t);
							}
						} else if (name == (rev ? "WheelUp":"WheelDown")) {
							if (world->GetLocalPlayer()->GetTeamId() < 2 &&
							   world->GetLocalPlayer()->IsAlive()) {
								Player::ToolType t = world->GetLocalPlayer()->GetTool();
								do {
									switch (t) {
										case Player::ToolSpade:
											t = Player::ToolBlock;
											break;
										case Player::ToolBlock:
											t = Player::ToolWeapon;
											break;
										case Player::ToolWeapon:
											t = Player::ToolGrenade;
											break;
										case Player::ToolGrenade:
											t = Player::ToolSpade;
											break;
									}
								} while (!world->GetLocalPlayer()->IsToolSelectable(t));
								SetSelectedTool(t);
							}
						}
					}
				} else {
					// limbo
				}
			}
		}
		

		
	}
}
