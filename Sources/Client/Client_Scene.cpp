/*
 Copyright (c) 2013 yvt
 based on code of pysnip (c) Mathias Kaerlev 2011-2012.
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "Client.h"

#include <Core/ConcurrentDispatch.h>
#include <Core/Settings.h>
#include <Core/Strings.h>

#include "World.h"
#include "Weapon.h"
#include "GameMap.h"
#include "Corpse.h"
#include "Grenade.h"
#include "IGameMode.h"
#include "TCGameMode.h"
#include "CTFGameMode.h"

#include "ClientPlayer.h"

#include "NetClient.h"

#include "../Hysteria/Hysteria.h"

SPADES_SETTING(cg_fov, "68");
SPADES_SETTING(cg_thirdperson, "0");

static float nextRandom() {
	return (float)rand() / (float)RAND_MAX;
}

namespace spades {
	namespace client {
		
		
#pragma mark - Drawing
	
        /*
         * Whether or not the camera origin should reside behind the player.
         * This is true if the client player is dead, or TPV mode is on (?)
         */    
		bool Client::ShouldRenderInThirdPersonView() {
            if (world && world->GetLocalPlayer() && !world->GetLocalPlayer()->IsAlive()) {
                return true;
            } else if ((int) cg_thirdperson != 0 && world->GetNumPlayers() <= 1) {
                return true;
            } else {
                return false;
            }
		}
		
		float Client::GetLocalFireVibration() {
            float localFireVibration = 0.0F;
            localFireVibration       = time - localFireVibrationTime;
            localFireVibration       = 1.0F - localFireVibration / 0.1F;

            if(localFireVibration < 0.0F) {
                localFireVibration = 0.0F;
            }

            return localFireVibration;
		}
		
		float Client::GetAimDownZoomScale(){
			if (  world == nullptr 
               || world->GetLocalPlayer() == nullptr 
               || world->GetLocalPlayer()->IsToolWeapon() == false 
               || world->GetLocalPlayer()->IsAlive() == false) {
				return 1.0F;
            } else {
                return 1.0F + powf(GetAimDownState(), 5.0F) * float(Hysteria::AimDelta);
            }
		}
	
        /*
         * XXX ACHTUNG! Ahead lies a god method.
         *
         * Configure the SceneDefinition for the deferred renderer.
         * This includes parameters such as camera blur..
         */    
		SceneDefinition Client::CreateSceneDefinition() {
			SPADES_MARK_FUNCTION();
			
			SceneDefinition def;

            {
                def.time           = time * 1000;
                def.denyCameraBlur = true;
            }
			
			if (world) {
        
                // Fog Setup
                renderer->SetFogColor(FloatingPointRGB(world->GetFogColor()));
				
				Player *player = world->GetLocalPlayer();
				
				def.blurVignette = 0.0F;
			
                // Display another player's view (used when waiting for a respawn)
                // XXX fugly flow control!    
				if (IsFollowing()) {
					int limit = 100;
					// if current following player has left,
					// or removed,
					// choose next player.
					while(!world->GetPlayer(followingPlayerId) ||
						  world->GetPlayer(followingPlayerId)->GetFront().GetPoweredLength() < 0.01F){
						FollowNextPlayer(false);
						if((limit--) <= 0){
							break;
						}
					}
					player = world->GetPlayer(followingPlayerId);
				}

                if (player) {
					
					float roll     = 0.0F;
					float scale    = 1.0F;
					float vibPitch = 0.0F;
					float vibYaw   = 0.0F;

					if (ShouldRenderInThirdPersonView() || (IsFollowing() && player != world->GetLocalPlayer())) {
						Vector3 center;
                        { // Initialize `center`
                            if ((!player->IsAlive()) && lastMyCorpse && player == world->GetLocalPlayer()) {
                                center = lastMyCorpse->GetCenter();
                            } else {
                                center = player->GetEye();
                            }
                        }
						Vector3 up(0,0,-1);
						

                        // If true, traverse Z until a corresponding point is found
                        // XXX fugly flow control!
						if (map->IsSolidWrapped((int) floorf(center.x), (int) floorf(center.y), (int) floorf(center.z))) {
							float z = center.z;
							while (z > center.z - 5.0F) {
								if (!map->IsSolidWrapped((int) floorf(center.x), (int) floorf(center.y), (int) floorf(z))) {
									center.z = z;
									break;
								} else {
									z -= 1.0F;
								}
							}
						}
						
						float distance = 5.0F;
                        { // Conditional initializer for `distance`
                            if (player == world->GetLocalPlayer() && world->GetLocalPlayer()->GetTeamId() < 2 && !world->GetLocalPlayer()->IsAlive()) {
                                // deathcam.
                                float elapsedTime = time - lastAliveTime;
                                distance -= 3.0F * expf(-elapsedTime * 1.0F);
                            }
                        }
						
						Vector3 eye = center;
                        { // Initialize `eye`
                            eye.x += cosf(followYaw) * cosf(followPitch) * distance;
                            eye.y += sinf(followYaw) * cosf(followPitch) * distance;
                            eye.z -= sinf(followPitch) * distance;
                        }
						
                        // settings for making limbo stuff
                        // eye      = (center + (playerFront * 3.0F) + (up * - 0.1F) + (player->GetRight() *2.0F)) * 0.6F;
						
						// try ray casting
                        {
                            GameMap::RayCastResult result;
                            result = map->CastRay2(center, (eye - center).Normalize(), 256);

                            if (result.hit) {
                                float dist    = (result.hitPos - center).GetLength() - 0.3F /* Near clip plane */;
                                float curDist = (eye - center).GetLength();
                                if (curDist > dist) {
                                    float diff = curDist - dist;
                                    eye += (center - eye).Normalize() * diff;
                                }
                            }
                        }
					
					
                        // Spectator Mode    
                        if (FirstPersonSpectate) {
                            def.viewOrigin  = player->GetEye();
                            def.viewAxis[0] = player->GetRight();
                            def.viewAxis[1] = player->GetUp();
                            def.viewAxis[2] = player->GetFront();
                        } else {
                            Vector3 front = (center - eye).Normalize();
                            def.viewOrigin  = eye;
                            def.viewAxis[0] = -Vector3::Cross(up, front).Normalize();
                            def.viewAxis[1] = -Vector3::Cross(front, def.viewAxis[0]).Normalize();
                            def.viewAxis[2] = front;
                        }
						
						def.fovY = (float) cg_fov * static_cast<float>(M_PI) / 180.0F;
						def.fovX = atanf(tanf(def.fovY * 0.5F) * renderer->ScreenWidth() / renderer->ScreenHeight()) * 2.0F;
						
						// update initial spectate pos
						// this is not used now, but if the local player is
						// is spectating, this is used when he/she's no
						// longer following
						followPos = def.viewOrigin;
						followVel = MakeVector3(0, 0, 0);
						
					} else if (player->GetTeamId() >= 2) {

                        // spectator view (noclip view)
                        {
                            Vector3 const center = followPos;
                            Vector3 const front (
                                      -cosf(followYaw) * cosf(followPitch),  
                                      -sinf(followYaw) * cosf(followPitch),
                                       sinf(followPitch));
                            Vector3 const up(0, 0, -1);
                            
                            
                            def.viewOrigin  = center;
                            def.viewAxis[0] = -Vector3::Cross(up, front).Normalize();
                            def.viewAxis[1] = -Vector3::Cross(front, def.viewAxis[0]).Normalize();
                            def.viewAxis[2] = front;
                        }
						
						def.fovY = (float) cg_fov * static_cast<float>(M_PI) / 180.0F;
						def.fovX = atanf(tanf(def.fovY * 0.5F) * renderer->ScreenWidth() / renderer->ScreenHeight()) * 2.0F;
						
						// for 1st view, camera blur can be used
						def.denyCameraBlur = false;
						
					} else {

						Vector3 front = player->GetFront();
						Vector3 right = player->GetRight();
						Vector3 up    = player->GetUp();
						
                        // Perform fire vibration adjustments
                        unless (Hysteria::NoToolShake) {
                            float localFireVibration = powf(GetLocalFireVibration(), 2);

                            { // Initialize `localFireVibration`
                                // Dampen vibration when digging (why are we vibrating when using a bloody shovel?)
                                if (player->GetTool() == Player::ToolSpade) {
                                    localFireVibration *= 0.4F;
                                }
                            }
                            
                            roll           += (nextRandom() - nextRandom()) * 0.03F * localFireVibration;
                            scale          += nextRandom() * 0.04F * localFireVibration;
                            vibPitch       += localFireVibration * (1.0F - localFireVibration) * 0.01F;
                            vibYaw         += sinf(localFireVibration * (float)M_PI * 2.0F) * 0.001F;
                        }
						
						// sprint bob
						unless (Hysteria::NoBobbing) {
							float sp     = SmoothStep(GetSprintState());
							vibYaw      += sinf(player->GetWalkAnimationProgress() * static_cast<float>(M_PI) * 2.0F) * 0.01F * sp;
							roll        -= sinf(player->GetWalkAnimationProgress() * static_cast<float>(M_PI) * 2.0F) * 0.005F * (sp);
                            float p      = powf(cosf(player->GetWalkAnimationProgress() * static_cast<float>(M_PI) * 2.0F), 4);
							vibPitch    += p * 0.01F * sp;
						}
						
						scale /= GetAimDownZoomScale();
                        
						def.viewOrigin            = player->GetEye();
						def.viewAxis[0]           = right;
						def.viewAxis[1]           = up;
						def.viewAxis[2]           = front;
						def.fovY                  = (float) cg_fov * static_cast<float>(M_PI) / 180.0F;
						def.fovX                  = atanf(tanf(def.fovY * 0.5F) * renderer->ScreenWidth() / renderer->ScreenHeight()) * 2.0F;
						def.denyCameraBlur        = false; // FPV can have camera blur
						def.blurVignette          = 0.0F;
						def.depthOfFieldNearRange = powf(GetAimDownState(), 3) * 13.054F;
					}
				
                    // Grenade vibration applies universally    
					unless (Hysteria::NoGrenadeShake) {
						// add grenade vibration
						float grenVib = grenadeVibration;

						if (grenVib > 0.0F) {
							grenVib  *= 10.0F;
                            grenVib   = grenVib > 1.0F ? 1.0F : grenVib;

							roll     += (nextRandom() - nextRandom()) * 0.2F * grenVib;
							vibPitch += (nextRandom() - nextRandom()) * 0.1F * grenVib;
							vibYaw   += (nextRandom() - nextRandom()) * 0.1F * grenVib;
							scale    -= (nextRandom()-nextRandom()) * 0.1F * grenVib;
						}
					}
					
					// add roll / scale
					{
						Vector3 right   = def.viewAxis[0];
						Vector3 up      = def.viewAxis[1];

						def.viewAxis[0] = right * cosf(roll) - up * sinf(roll);
						def.viewAxis[1] = up * cosf(roll) + right * sinf(roll);
						def.fovX        = atanf(tanf(def.fovX * 0.5F) * scale) * 2.0F;
						def.fovY        = atanf(tanf(def.fovY * 0.5F) * scale) * 2.0F;
					}

					{
						Vector3 u = def.viewAxis[1];
						Vector3 v = def.viewAxis[2];
						
						def.viewAxis[1] = u * cosf(vibPitch) - v * sinf(vibPitch);
						def.viewAxis[2] = v * cosf(vibPitch) + u * sinf(vibPitch);
					}

					{
						Vector3 u = def.viewAxis[0];
						Vector3 v = def.viewAxis[2];
						
						def.viewAxis[0] = u * cosf(vibYaw) - v * sinf(vibYaw);
						def.viewAxis[2] = v * cosf(vibYaw) + u * sinf(vibYaw);
					}

				} else {

					def.viewOrigin  = Vector3( 256, 256,    4);
					def.viewAxis[0] = Vector3(-1,   0,      0);
					def.viewAxis[1] = Vector3( 0,   1,      0);
					def.viewAxis[2] = Vector3( 0,   0,      1);
					def.fovY        = float(cg_fov) * float(M_PI) / 180.0F;
					def.fovX        = atanf(tanf(def.fovY * 0.5F) * renderer->ScreenWidth() / renderer->ScreenHeight()) * 2.0F;
					
				}
				
                def.skipWorld = false;

			} else {

				def.viewOrigin  = Vector3(0, 0,  0);
				def.viewAxis[0] = Vector3(1, 0,  0);
				def.viewAxis[1] = Vector3(0, 0, -1);
				def.viewAxis[2] = Vector3(0, 0,  1);
				
				def.fovY = float(cg_fov) * float(M_PI) / 180.0F;
				def.fovX = atanf(tanf(def.fovY * 0.5F) * renderer->ScreenWidth() / renderer->ScreenHeight()) * 2.0F;
				
                renderer->SetFogColor(MakeVector3(0,0,0));
				
                def.skipWorld = true;

			}
        
            def.zNear = 0.05F;
            def.zFar  = float(Hysteria::MapCullDist);

			SPAssert(not isnan(def.viewOrigin.x));
			SPAssert(not isnan(def.viewOrigin.y));
			SPAssert(not isnan(def.viewOrigin.z));
			
			def.radialBlur = std::min(def.radialBlur, 1.0F);
			
			return def;
		}
		
		void Client::AddGrenadeToScene(spades::client::Grenade *g) {
			SPADES_MARK_FUNCTION();
			
            // work-around for water refraction problem
			if (g->GetPosition().z <= 63.0F) {
                ModelRenderParam param;
                param.matrix = ({
                        Matrix4::Translate(g->GetPosition())
                            * Matrix4::Scale(0.03F);
                });
                        
                
                renderer->RenderModel(renderer->RegisterModel("Models/Weapons/Grenade/Grenade.kv6"), param);
            }
		}

        /*
         * Adds line objects to the deferred renderer outlining a given bounding box with the specified colour
         */        
		void Client::AddDebugObjectToScene(const spades::OBB3 &obb, const Vector4& color) {
			const Matrix4& mat = obb.m;
			Vector3 v[2][2][2];
			v[0][0][0] = (mat * MakeVector3(0,0,0)).GetXYZ();
			v[0][0][1] = (mat * MakeVector3(0,0,1)).GetXYZ();
			v[0][1][0] = (mat * MakeVector3(0,1,0)).GetXYZ();
			v[0][1][1] = (mat * MakeVector3(0,1,1)).GetXYZ();
			v[1][0][0] = (mat * MakeVector3(1,0,0)).GetXYZ();
			v[1][0][1] = (mat * MakeVector3(1,0,1)).GetXYZ();
			v[1][1][0] = (mat * MakeVector3(1,1,0)).GetXYZ();
			v[1][1][1] = (mat * MakeVector3(1,1,1)).GetXYZ();
			
			renderer->AddDebugLine(v[0][0][0], v[1][0][0], color);
			renderer->AddDebugLine(v[0][0][1], v[1][0][1], color);
			renderer->AddDebugLine(v[0][1][0], v[1][1][0], color);
			renderer->AddDebugLine(v[0][1][1], v[1][1][1], color);
			
			renderer->AddDebugLine(v[0][0][0], v[0][1][0], color);
			renderer->AddDebugLine(v[0][0][1], v[0][1][1], color);
			renderer->AddDebugLine(v[1][0][0], v[1][1][0], color);
			renderer->AddDebugLine(v[1][0][1], v[1][1][1], color);
			
			renderer->AddDebugLine(v[0][0][0], v[0][0][1], color);
			renderer->AddDebugLine(v[0][1][0], v[0][1][1], color);
			renderer->AddDebugLine(v[1][0][0], v[1][0][1], color);
			renderer->AddDebugLine(v[1][1][0], v[1][1][1], color);
		}
		
		void Client::DrawCTFObjects() {
			SPADES_MARK_FUNCTION();

			CTFGameMode *mode = static_cast<CTFGameMode *>(world->GetMode());

            // TODO 2 should probably be a constant for scalability
			repeat (2, tId) {
				CTFGameMode::Team& team = mode->GetTeam(tId);
				
				ModelRenderParam param;
                {
                    param.customColor = FloatingPointRGB(world->GetTeam(tId).color);
                }
				
				// draw base
                {
                    param.matrix = ({
                            Matrix4::Translate(team.basePos)
                                * Matrix4::Scale(0.3F);
                    });
                    renderer->RenderModel(renderer->RegisterModel("Models/MapObjects/CheckPoint.kv6"), param);
                }
				
				// draw flag
				unless (mode->GetTeam(1 - tId).hasIntel) {
					param.matrix = ({
                            Matrix4::Translate(team.flagPos)
                                * Matrix4::Scale(0.1F);
                    });

					renderer->RenderModel(renderer->RegisterModel("Models/MapObjects/Intel.kv6"), param);
				}
			}
		}
		
		void Client::DrawTCObjects() {
            using Territory = TCGameMode::Territory;

			SPADES_MARK_FUNCTION();

			TCGameMode* mode = (TCGameMode*) world->GetMode();

            repeat (mode->GetNumTerritories(), tId) {

                Territory* territory = mode->GetTerritory(tId);
                ModelRenderParam renderParams;
                { 
                    // Is the client player spectating?
                    if (territory->ownerTeamId == 2) {
                        renderParams.customColor = Vector3(1, 1, 1);
                    } else {
                        renderParams.customColor = FloatingPointRGB(world->GetTeam(territory->ownerTeamId).color);
                    }
                    
                    renderParams.matrix = ({
                            Matrix4::Translate(territory->pos)
                                * Matrix4::Scale(0.3F);
                    });
                }

                renderer->RenderModel(renderer->RegisterModel("Models/MapObjects/CheckPoint.kv6"), renderParams);
            }
		}
	
        /*
         * Draws configures the deferred renderer to draw the following
         * 
         * - Players
         * - Grenades
         * - Entities
         * - Block Cursor
         * - Player Hot-Track (Hitboxes)
         */
		void Client::DrawScene() {
			SPADES_MARK_FUNCTION();
			
			renderer->StartScene(lastSceneDef);
			
			if (world) {
				Player *p = world->GetLocalPlayer();
			
                // Player Artefacts
                {
                    /*
                     * Add players to scene
                     */

                    repeat (world->GetNumPlayerSlots(), slot) {
                        if (world->GetPlayer(slot)) {
                            SPAssert(clientPlayers[slot]);
                            clientPlayers[slot]->AddToScene();
                        } 
                    }
                    
                    /*
                     * Draw Corpses
                     */

                    for (auto& corpse : corpses) {
                        Vector3 const center = corpse->GetCenter();
                        unless ((center - lastSceneDef.viewOrigin).GetPoweredLength() > powf(150.0F, 2)) {
                            corpse->AddToScene();
                        }
                    }
                }

                // Add grenades
                for (Grenade* grenade : world->GetAllGrenades()) {
                    AddGrenadeToScene(grenade);
                }
		
			    // Game Mode Specific 
                switch (world->GetMode()->ModeType()) {
                    case IGameMode::m_CTF:
                        DrawCTFObjects();
                        break;
                    case IGameMode::m_TC:
                        DrawTCObjects();
                        break;
                    default:
                        // Nothing needed
                        break;
                }
			
                // Entity Render Pass    
                for(auto& ent: localEntities){
                    ent->Render3D();
                }
				
				// Block Cursor
				if (p) {
					if (p->IsReadyToUseTool() && p->GetTool() == Player::ToolBlock && p->IsAlive()) {

						std::vector<IntVector3> blocks;
                        { // Initialize `blocks`
                            if (p->IsBlockCursorDragging()) {
                                blocks = std::move(world->CubeLine(p->GetBlockCursorDragPos(), p->GetBlockCursorPos(), 256));
                            } else {
                                blocks.push_back(p->GetBlockCursorPos());
                            }
                        }
						
						bool active = p->IsBlockCursorActive();
						
						Vector4 const color (
                                /* Red   (X) */ 1.0F,
                                /* Green (Y) */ blocks.size() > p->GetNumBlocks() ? 0.0F : 1.0F,
                                /* Blue  (Z) */ active ? 1.0F : 0.0F,
                                /* Alpha (W) */ active ? 1.0F : 0.5F);           
					    	
					
                        /*
                         * Draw Block Cursor Quadrants
                         */    
						for (IntVector3& v : blocks) {
							if (active) {
								
								renderer->AddDebugLine(MakeVector3(v.x, v.y, v.z),
													   MakeVector3(v.x+1, v.y, v.z),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x, v.y+1, v.z),
													   MakeVector3(v.x+1, v.y+1, v.z),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x, v.y, v.z+1),
													   MakeVector3(v.x+1, v.y, v.z+1),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x, v.y+1, v.z+1),
													   MakeVector3(v.x+1, v.y+1, v.z+1),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x, v.y, v.z),
													   MakeVector3(v.x+1, v.y, v.z),
													   color);
								
								renderer->AddDebugLine(MakeVector3(v.x, v.y, v.z),
													   MakeVector3(v.x, v.y+1, v.z),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x, v.y, v.z+1),
													   MakeVector3(v.x, v.y+1, v.z+1),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x+1, v.y, v.z),
													   MakeVector3(v.x+1, v.y+1, v.z),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x+1, v.y, v.z+1),
													   MakeVector3(v.x+1, v.y+1, v.z+1),
													   color);
								
								renderer->AddDebugLine(MakeVector3(v.x, v.y, v.z),
													   MakeVector3(v.x, v.y, v.z+1),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x, v.y+1, v.z),
													   MakeVector3(v.x, v.y+1, v.z+1),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x+1, v.y, v.z),
													   MakeVector3(v.x+1, v.y, v.z+1),
													   color);
								renderer->AddDebugLine(MakeVector3(v.x+1, v.y+1, v.z),
													   MakeVector3(v.x+1, v.y+1, v.z+1),
													   color);
							} else {
								// not active
								
								const float ln = 0.1F;
								{
									float xx = v.x, yy = v.y, zz = v.z;
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx+ln, yy, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy+ln, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy, zz+ln), color);
								}
								{
									float xx = v.x + 1, yy = v.y, zz = v.z;
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx-ln, yy, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy+ln, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy, zz+ln), color);
								}
								{
									float xx = v.x, yy = v.y + 1, zz = v.z;
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx+ln, yy, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy-ln, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy, zz+ln), color);
								}
								{
									float xx = v.x + 1, yy = v.y + 1, zz = v.z;
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx-ln, yy, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy-ln, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy, zz+ln), color);
								}
								{
									float xx = v.x, yy = v.y, zz = v.z + 1;
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx+ln, yy, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy+ln, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy, zz-ln), color);
								}
								{
									float xx = v.x + 1, yy = v.y, zz = v.z + 1;
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx-ln, yy, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy+ln, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy, zz-ln), color);
								}
								{
									float xx = v.x, yy = v.y + 1, zz = v.z + 1;
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx+ln, yy, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy-ln, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy, zz-ln), color);
								}
								{
									float xx = v.x + 1, yy = v.y + 1, zz = v.z + 1;
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx-ln, yy, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy-ln, zz), color);
									renderer->AddDebugLine(Vector3(xx, yy, zz), Vector3(xx, yy, zz-ln), color);
								}
							}
						} 
						
					} 
				}
			}
		
            /*
             * Flashes
             */
			for(auto& light : flashDlights) {
				renderer->AddLight(light);
            }

			flashDlightsOld.clear();
			flashDlightsOld.swap(flashDlights);
			
            // Hovered Player Hitboxes
			{
                static Vector4 const black(1, 1, 1, 1);

				hitTag_t tag       = hit_None;
				Player *hottracked = HotTrackedPlayer( &tag );

				if (hottracked) {
					Vector4 const color       = FloatingPointRGBA(world->GetTeam(hottracked->GetTeamId()).color);
					Player::HitBoxes const hb = hottracked->GetHitBoxes();

					AddDebugObjectToScene(hb.head,      (tag & hit_Head)    ? black : color);
					AddDebugObjectToScene(hb.torso,     (tag & hit_Torso)   ? black : color);
					AddDebugObjectToScene(hb.limbs[0],  (tag & hit_Legs)    ? black : color);
					AddDebugObjectToScene(hb.limbs[1],  (tag & hit_Legs)    ? black : color);
					AddDebugObjectToScene(hb.limbs[2],  (tag & hit_Arms)    ? black : color);
				}
			}
			
			renderer->EndScene();
		}
	
        /*
         * Returns a configuration vector for the GL Renderer Projection
         */    
		Vector3 Client::Project(spades::Vector3 v){
			v -= lastSceneDef.viewOrigin;
			
			// transform to NDC
			Vector3 v2;
			v2.x = Vector3::Dot(v, lastSceneDef.viewAxis[0]);
			v2.y = Vector3::Dot(v, lastSceneDef.viewAxis[1]);
			v2.z = Vector3::Dot(v, lastSceneDef.viewAxis[2]);
			
			float tanX = tanf(lastSceneDef.fovX * 0.5F);
			float tanY = tanf(lastSceneDef.fovY * 0.5F);
			
			v2.x /= tanX * v2.z;
			v2.y /= tanY * v2.z;
			
			// transform to IRenderer 2D coord
			v2.x = (v2.x + 1.0F) / 2.0F * renderer->ScreenWidth();
			v2.y = (-v2.y + 1.0F) / 2.0F * renderer->ScreenHeight();
			
			return v2;
		}

	}
}
