/*
 Copyright (c) 2013 yvt
 based on code of pysnip (c) Mathias Kaerlev 2011-2012.
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#pragma once

#include "Player.h"
#include "PhysicsConstants.h"

namespace spades {
	namespace client {
		class World;
		class Player;
		
        class Weapon {
            protected:
                World*   world;
                Player*  owner;
                float    time;
                bool     shooting;
                bool     reloading;
                float    nextShotTime;
                float    reloadStartTime;
                float    reloadEndTime;
                int      slowReloadLeftCount;
                bool     lastDryFire;
                int      ammo;
                int      stock;
                
            public:
                static Weapon* CreateWeapon(WeaponType, Player*);
                
                Weapon(World*, Player*);
                virtual ~Weapon();

                virtual std::string GetName()  const        = 0;
                virtual int GetClipSize() const             = 0;
                virtual int GetMaxStock()   const           = 0;
                virtual float GetReloadTime() const         = 0;
                virtual bool IsReloadSlow() const           = 0;
                virtual int GetDamage(HitType, float) const = 0;
                virtual WeaponType GetWeaponType() const    = 0;
                virtual Vector3 GetRecoil() const           = 0;
                virtual int GetPelletSize() const           = 0;
                
                float GetDelay() const;
                float GetSpread() const;

                void Restock();
                void Reset();
                void SetShooting(bool);
                
                bool FrameNext(float);
                
                void Reload(bool manual = true);
                void AbortReload();
                
                // for local player
                void ReloadDone(int, int);
                void ForceReloadDone();
                
                bool  IsReadyToShoot()    const;
                float GetReloadProgress() const;
                float TimeToNextFire()    const;

                // Getters
                
                bool IsShooting() const {
                    return shooting;
                }

                bool IsReloading() const { 
                    return reloading; 
                }

                int GetAmmo() const { 
                    return ammo; 
                }

                int GetStock() const { 
                    return stock;
                }
		};
	}
}
