/*
 Copyright (c) 2013 yvt
 based on code of pysnip (c) Mathias Kaerlev 2011-2012.
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "Weapon.h"
#include "World.h"
#include "IWorldListener.h"
#include "../Core/Exception.h"
#include "../Core/Debug.h"
#include "../Core/Settings.h"

#include "../Hysteria/Hysteria.h"

SPADES_SETTING(cg_protocolVersion, "3");

namespace spades {
	namespace client {
		Weapon::Weapon(World *w, Player *p):
            world(w),
            owner(p),
            time(0),
            shooting(false),
            reloading(false),
            nextShotTime(0.0F),
            reloadEndTime(-100.0F),
            reloadStartTime(-101.0F),
            lastDryFire(false)
		{
			SPADES_MARK_FUNCTION();
		}
		
		Weapon::~Weapon() {
			SPADES_MARK_FUNCTION();
		}
		
		void Weapon::Restock() {
			stock = GetMaxStock();
		}
		
		void Weapon::Reset() {
			SPADES_MARK_FUNCTION();
			shooting     = false;
			reloading    = false;
			ammo         = GetClipSize();
			stock        = GetMaxStock();
			time         = 0.0F;
			nextShotTime = 0.0F;
		}
		
		void Weapon::SetShooting(bool b) {
			shooting = b;
		}
		
		bool Weapon::IsReadyToShoot() const {
			return ammo > 0 && time >= nextShotTime && (not(reloading) || IsReloadSlow());
		}
		
		float Weapon::GetReloadProgress() const {
			return (time - reloadStartTime) / (reloadEndTime - reloadStartTime);
		}
		
		float Weapon::TimeToNextFire() const {
			return nextShotTime - time;
		}
		
		bool Weapon::FrameNext(float dt) {
			SPADES_MARK_FUNCTION();
			
			bool fired   = false;
			bool dryFire = false;

			if (shooting && (!reloading || IsReloadSlow())) {
			
                // abort slow reload
				reloading = false;

				if (time >= nextShotTime && ammo > 0) {
					--ammo;
					fired = true;

					if (world->GetListener()) {
						world->GetListener()->PlayerFiredWeapon(owner);
                    }

					nextShotTime = time + GetDelay();
				} else if (time >= nextShotTime) {
					dryFire = true;
				}
			
            } else if (reloading) {
			
                if (time >= reloadEndTime) {

					// reload done
					reloading = false;
			
                    if (IsReloadSlow()) {

						// for local player, server sends
						// new ammo/stock value
						if (ammo < GetClipSize() && stock > 0 && owner != owner->GetWorld()->GetLocalPlayer()) {
							ammo++;
							stock--;
						}

						--slowReloadLeftCount;

						if (slowReloadLeftCount > 0) {
							Reload(false);
                        } else if (world->GetListener()) {
								world->GetListener()->PlayerReloadedWeapon(owner);
                        }

					} else {
			
                        // for local player, server sends
						// new ammo/stock value
						if (owner != owner->GetWorld()->GetLocalPlayer()) {
							int newStock;
							newStock = std::max(0, stock - GetClipSize() + ammo);
							ammo += stock - newStock;
							stock = newStock;
						}
			
                        if (world->GetListener()) {
							world->GetListener()->PlayerReloadedWeapon(owner);
                        }
					}
					
					
				}

			}

			time += dt;
			
			if (dryFire && !lastDryFire && world->GetListener()) {
                world->GetListener()->PlayerDryFiredWeapon(owner);
			}

			lastDryFire = dryFire;

			return fired;
		}
		
		void Weapon::ReloadDone(int ammo, int stock) {
			SPADES_MARK_FUNCTION_DEBUG();
			this->ammo  = ammo;
			this->stock = stock;
		}
		
		void Weapon::AbortReload() {
			SPADES_MARK_FUNCTION_DEBUG();
			reloading = false;
		}
		
		void Weapon::Reload(bool manual) {
			SPADES_MARK_FUNCTION();
			
            unless (reloading || (ammo >= GetClipSize() /* WTF? */) || stock == 0 || (IsReloadSlow() && ammo > 0 && shooting)) {

                if (manual) {
                    slowReloadLeftCount = stock - std::max(0, stock - GetClipSize() + ammo);
                }

                reloading       = true;
                shooting        = false;
                reloadStartTime = time;
                reloadEndTime   = time + GetReloadTime();
                
                if (world->GetListener()) {
                    world->GetListener()->PlayerReloadingWeapon(owner);
                }

            }
		}
		
		void Weapon::ForceReloadDone() {
			int const newStock = std::max(0, stock - GetClipSize() + ammo);
			
            ammo     += stock - newStock;
			stock     = newStock;
		}

        float Weapon::GetSpread() const {
            return Hysteria::GetWeaponSpread(this->GetWeaponType());
        }

        float Weapon::GetDelay() const  {
            return Hysteria::GetWeaponDelay(this->GetWeaponType());
        }


        /**
         * Standard rifle weapon implementation based off of 0.75
         */    
		class RifleWeapon : public Weapon {
            public:
                RifleWeapon(World*w,Player*p)
                    :Weapon(w,p) {}

                virtual std::string GetName() const { 
                    return "Rifle"; 
                }

                virtual int GetClipSize() const { 
                    return 10; 
                }

                virtual int GetMaxStock() const {
                    return 50; 
                }

                virtual float GetReloadTime() const { 
                    return 2.5F; 
                }

                virtual bool IsReloadSlow() const { 
                    return false; 
                }

                virtual WeaponType GetWeaponType() const { 
                    return RIFLE_WEAPON; 
                }

                virtual int GetDamage(HitType type, float distance) const {
                    switch (type) {
                        case HitTypeTorso: 
                            return 49;
                        case HitTypeHead: 
                            return 100;
                        case HitTypeArms: 
                            return 33;
                        case HitTypeLegs: 
                            return 33;
                        case HitTypeBlock: 
                            return 50;
                        default: 
                            SPAssert(false); 
                            return 0;
                    }
                }

                virtual Vector3 GetRecoil() const {
                    return Vector3(0.0001F, 0.05F, 0.0F);
                }

                virtual int GetPelletSize() const { 
                    return 1; 
                }
	
        };

        /**
         * Rifle implementation reflecting 0.76 modifications
         */        
		class RifleWeapon4 : public RifleWeapon {
            public:
                RifleWeapon4(World*w,Player*p) 
                    : RifleWeapon(w,p) {}
                
                virtual int GetClipSize() const override { 
                    return 8; 
                }

                virtual int GetMaxStock() const override { 
                    return 48; 
                }

                virtual Vector3 GetRecoil () const override {
                    return MakeVector3(0.0001F, 0.075F, 0.0F);
                }
        };
	
        /**
         * Standard SMG implementation from 0.75
         */    
		class SMGWeapon : public Weapon {
            public:
                
                SMGWeapon(World*w,Player*p)
                    : Weapon(w,p) {}
                
                virtual std::string GetName() const { 
                    return "SMG"; 
                }
                
                virtual int GetClipSize() const { 
                    return 30; 
                }
                
                virtual int GetMaxStock() const { 
                    return 120; 
                }
                
                virtual float GetReloadTime() const { 
                    return 2.5F; 
                }
                
                virtual bool IsReloadSlow() const { 
                    return false; 
                }
                
                virtual WeaponType GetWeaponType() const { 
                    return SMG_WEAPON; 
                }
                
                virtual int GetDamage(HitType type, float distance) const {
                    switch (type) {
                        case HitTypeTorso: 
                            return 29;
                        case HitTypeHead: 
                            return 75;
                        case HitTypeArms: 
                            return 18;
                        case HitTypeLegs: 
                            return 18;
                        case HitTypeBlock: 
                            return 35;
                        default: 
                            SPAssert(false); 
                            return 0;
                    }
                }
                
                virtual Vector3 GetRecoil () const {
                    return MakeVector3(0.00005F, 0.0125F, 0.0F);
                }

                virtual int GetPelletSize() const { 
                    return 1; 
                }
		};
		
        /**
         * 0.76 implementation of the SMG
         */
		class SMGWeapon4 : public SMGWeapon {
            public:
                SMGWeapon4(World* w,Player* p)
                    : SMGWeapon(w, p) {}

                virtual int GetMaxStock() const override { 
                    return 150; 
                }
		};

        /**
         * Standard Shotgun implementation from 0.75
         */
		class ShotgunWeapon: public Weapon {
            public:
                ShotgunWeapon(World*w,Player*p)
                    : Weapon(w,p) {}

                virtual std::string GetName() const { 
                    return "Shotgun"; 
                }
                
                virtual int GetClipSize() const { 
                    return 6; 
                }
                
                virtual int GetMaxStock() const { 
                    return 48; 
                }
                
                virtual float GetReloadTime() const { 
                    return 0.5F; 
                }
                
                virtual bool IsReloadSlow() const { 
                    return true; 
                }
                
                virtual WeaponType GetWeaponType() const { 
                    return SHOTGUN_WEAPON; 
                }
                
                virtual int GetDamage(HitType type, float) const {
                    switch (type) {
                        case HitTypeTorso: 
                            return 27;
                        case HitTypeHead: 
                            return 37;
                        case HitTypeArms: 
                            return 16;
                        case HitTypeLegs: 
                            return 16;
                        case HitTypeBlock:
                            return 34;
                        default: 
                            SPAssert(false); 
                            return 0;
                    }
                }
                
                virtual Vector3 GetRecoil () const {
                    return MakeVector3(0.0002F, 0.1F, 0.0F);
                }
                
                virtual int GetPelletSize() const { 
                    return 8; 
                }
		};
		
        /**
         * Shotgun implementation compliant with 0.76
         */	
		class ShotgunWeapon4 final : public ShotgunWeapon {
            public:
                ShotgunWeapon4(World* w, Player* p)
                    : ShotgunWeapon(w, p) {}
                
                virtual int GetClipSize() const override { 
                    return 8; 
                }
                
                virtual int GetMaxStock() const override { 
                    return 48; 
                }
                
                virtual float GetReloadTime() const override { 
                    return 0.4F; 
                }
                
                virtual Vector3 GetRecoil() const override {
                    return Vector3(0.0002F, 0.075F, 0.0F);
                }
		};
		
		Weapon *Weapon::CreateWeapon(WeaponType type, Player *p) {
			SPADES_MARK_FUNCTION();
			
			if (int(cg_protocolVersion) == 4) {
				
                switch (type) {
					case RIFLE_WEAPON:
						return new RifleWeapon4(p->GetWorld(), p);
					case SMG_WEAPON:
						return new SMGWeapon4(p->GetWorld(), p);
					case SHOTGUN_WEAPON:
						return new ShotgunWeapon4(p->GetWorld(), p);
					default:
						SPInvalidEnum("type", type);
				}

			} else {
				
                switch (type) {
					case RIFLE_WEAPON:
						return new RifleWeapon(p->GetWorld(), p);
					case SMG_WEAPON:
						return new SMGWeapon(p->GetWorld(), p);
					case SHOTGUN_WEAPON:
						return new ShotgunWeapon(p->GetWorld(), p);
					default:
						SPInvalidEnum("type", type);
				}

			}
		}
		
	}
}
