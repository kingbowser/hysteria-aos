/*
 Copyright (c) 2013 yvt, 
 based on code of pysnip (c) Mathias Kaerlev 2011-2012.
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "Player.h"
#include "World.h"
#include "GameMap.h"
#include "IWorldListener.h"
#include "Weapon.h"
#include "Grenade.h"
#include "../Core/Settings.h"
#include "HitTestDebugger.h"

#include "../Hysteria/Hysteria.h"
#include "../Hysteria/HitboxUtil.h"

namespace spades {
    namespace client {
        
        Player::Player(World *w, int playerId,
                       WeaponType wType, int teamId,
                       Vector3 position,
                       IntVector3 color):
        world(w) {
            SPADES_MARK_FUNCTION();
            
            lastClimbTime          = -100;
            lastJumpTime           = -100;
            lastJump               = false;
            tool                   = ToolWeapon;
            airborne               = false;
            wade                   = false;
            this->position         = position;
            velocity               = MakeVector3(0,0,0);
            orientation            = MakeVector3(1,0,0);
            eye                    = MakeVector3(0,0,0);
            moveDistance           = 0.0F;
            moveSteps              = 0;
            
            this->playerId         = playerId;
            this->weapon           = Weapon::CreateWeapon(wType, this);
            this->teamId           = teamId;
            this->color            = color;
            
            health                 = 100;
            grenades               = 3;
            blockStocks            = 50;
            blockColor             = IntVector3::Make(111, 111, 111);
        
            nextSpadeTime          = 0.0F;
            nextDigTime            = 0.0F;
            nextGrenadeTime        = 0.0F;
            nextBlockTime          = 0.0F;
            firstDig               = false;
            lastReloadingTime      = 0.0F;
            
            pendingPlaceBlock      = false;
            
            blockCursorActive      = false;
            blockCursorDragging    = false;
            
            holdingGrenade         = false;
            reloadingServerSide    = false;
            canPending             = false;

            this->weapon->Reset();

            if (teamId) { // quick hack for correct spawn orientation 
                orientation        = MakeVector3(-1,0,0);
            }
        }
        
        Player::~Player() {
            SPADES_MARK_FUNCTION();
            delete weapon;
        }
        
        bool Player::IsLocalPlayer() {
            if (!world)
                return false;
            return world->GetLocalPlayer() == this;
        }
        
        void Player::SetInput(PlayerInput newInput) {
            SPADES_MARK_FUNCTION();
            
            if (!IsAlive())
                return;
            
            if (newInput.crouch != input.crouch && !airborne) {
                if (newInput.crouch)
                    position.z += 0.9F;
                else
                    position.z -= 0.9F;
            }
            input = newInput;
        }
        
        void Player::SetWeaponInput(WeaponInput newInput) {
            SPADES_MARK_FUNCTION();
            auto *listener = GetWorld()->GetListener();
            
            if (!IsAlive())
                return;
            
            unless (Hysteria::RunAndGun) {
                if (input.sprint) {
                    newInput.primary   = false;
                    newInput.secondary = false;
                }
            }

            if (tool == ToolSpade) {
                if (newInput.secondary)
                    newInput.primary = false;
                if (newInput.secondary != weapInput.secondary) {
                    if (newInput.secondary) {
                        nextDigTime = world->GetTime() + 1.0F;
                        firstDig = true;
                    }
                }
            } else if (tool == ToolGrenade) {
                if (world->GetTime() < nextGrenadeTime) {
                    newInput.primary = false;
                }
                if (grenades == 0) {
                    newInput.primary = false;
                }
                if (weapInput.primary && holdingGrenade &&
                   GetGrenadeCookTime() < 0.15F) {
                    // pin is not pulled yet
                    newInput.primary = true;
                }
                if (newInput.primary != weapInput.primary) {
                    if (!newInput.primary) {
                        if (holdingGrenade) {
                            nextGrenadeTime = world->GetTime() + 0.5F;
                            ThrowGrenade();
                        }
                    } else {
                        holdingGrenade = true;
                        grenadeTime = world->GetTime();
                        if (listener &&
                           this == world->GetLocalPlayer())
                            // playing other's grenade sound
                            // is cheating
                            listener->LocalPlayerPulledGrenadePin();
                    }
                }
            } else if (tool == ToolBlock) {
                // work-around for bug that placing block
                // occasionally becomes impossible
                if (nextBlockTime > world->GetTime() +
                   std::max(GetToolPrimaryDelay(),
                            GetToolSecondaryDelay())) {
                    nextBlockTime = world->GetTime() +
                    std::max(GetToolPrimaryDelay(),
                             GetToolSecondaryDelay());
                }
                
                if (world->GetTime() < nextBlockTime) {
                    newInput.primary = false;
                    newInput.secondary = false;
                }
                if (newInput.secondary)
                    newInput.primary = false;
                if (newInput.secondary != weapInput.secondary) {
                    if (newInput.secondary) {
                        if (IsBlockCursorActive()) {
                            blockCursorDragging = true;
                            blockCursorDragPos = blockCursorPos;
                        } else {
                            // cannot build; invalid position.
                            if (listener &&
                               this == world->GetLocalPlayer()) {
                                listener->
                                LocalPlayerBuildError(BuildFailureReason::InvalidPosition);
                            }
                        }
                    } else {
                        if (IsBlockCursorDragging()) {
                            if (IsBlockCursorActive()) {
                                std::vector<IntVector3> blocks = GetWorld()->CubeLine(blockCursorDragPos,
                                                                                      blockCursorPos, 256);
                                if ((int)blocks.size() <= blockStocks) {
                                    if (listener &&
                                       this == world->GetLocalPlayer())
                                        listener->LocalPlayerCreatedLineBlock(blockCursorDragPos, blockCursorPos);
                                    //blockStocks -= blocks.size(); decrease when created
                                } else {
                                    // cannot build; insufficient blocks.
                                    if (listener &&
                                       this == world->GetLocalPlayer()) {
                                        listener->
                                        LocalPlayerBuildError(BuildFailureReason::InsufficientBlocks);
                                    }
                                }
                                nextBlockTime = world->GetTime() + GetToolSecondaryDelay();
                            } else {
                                // cannot build; invalid position.
                                if (listener &&
                                   this == world->GetLocalPlayer()) {
                                    listener->
                                    LocalPlayerBuildError(BuildFailureReason::InvalidPosition);
                                }
                            }
                        }
                        
                        blockCursorDragging = false;
                        blockCursorActive = false;
                    }
                }
                if (newInput.primary != weapInput.primary ||
                   newInput.primary) {
                    if (newInput.primary) {
                        
                        if (!weapInput.primary)
                            lastSingleBlockBuildSeqDone = false;
                        if (IsBlockCursorActive() && blockStocks > 0) {
                            if (listener &&
                               this == world->GetLocalPlayer())
                                listener->LocalPlayerBlockAction(blockCursorPos, BlockActionCreate);
                            
                            lastSingleBlockBuildSeqDone = true;
                            // blockStocks--; decrease when created
                            
                            nextBlockTime = world->GetTime() + GetToolPrimaryDelay();
                        } else if (blockStocks > 0 && airborne && canPending &&
                                 this == world->GetLocalPlayer()) {
                            pendingPlaceBlock = true;
                            pendingPlaceBlockPos = blockCursorPos;
                        } else if (!IsBlockCursorActive()) {
                            // wait for building becoming possible
                        }
                        
                        blockCursorDragging = false;
                        blockCursorActive = false;
                    } else {
                        if (!lastSingleBlockBuildSeqDone) {
                            // cannot build; invalid position.
                            if (listener &&
                               this == world->GetLocalPlayer()) {
                                listener->
                                LocalPlayerBuildError(BuildFailureReason::InvalidPosition);
                            }
                        }
                    }
                }
            } else if (IsToolWeapon()) {
                weapon->SetShooting(newInput.primary);
            } else {
                SPAssert(false);
            }
            
            weapInput = newInput;
            
            
        }
        
        void Player::Reload() {
            SPADES_MARK_FUNCTION();
            if (health == 0) {
                // dead man cannot reload
                return;
            }
            weapon->Reload();
            if (this == world->GetLocalPlayer() &&
               weapon->IsReloading())
                reloadingServerSide = true;
        }
        
        void Player::ReloadDone(int clip, int stock) {
            reloadingServerSide = false;
            weapon->ReloadDone(clip, stock);
        }
        
        void Player::Restock() {
            SPADES_MARK_FUNCTION();
            if (health == 0) {
                // dead man cannot restock
                return;
            }
            
            weapon->Restock();
            grenades = 3;
            blockStocks = 50;
            health = 100;
            
            if (world->GetListener())
                world->GetListener()->PlayerRestocked(this);
        }
        
        void Player::GotBlock() {
            if (blockStocks < 50)
                blockStocks++;
        }
        
        void Player::SetTool(spades::client::Player::ToolType t) {
            SPADES_MARK_FUNCTION();
            
            if (t == tool)
                return;
            tool = t;
            holdingGrenade = false;
            blockCursorActive = false;
            blockCursorDragging = false;
            
            reloadingServerSide = false;
            
            WeaponInput inp;
            SetWeaponInput(inp);
            
            weapon->AbortReload();
            
            if (world->GetListener())
                world->GetListener()->PlayerChangedTool(this);
        }
        
        void Player::SetHeldBlockColor(spades::IntVector3 col) {
            blockColor = col;
        }
        
        void Player::SetPosition(const spades::Vector3 &v) {
            SPADES_MARK_FUNCTION();
            
            position = v;
            eye = v;
            
        }
        
        void Player::SetVelocity(const spades::Vector3 &v) {
            SPADES_MARK_FUNCTION();
            
            velocity = v;
        }
        
        void Player::SetOrientation(const spades::Vector3 &v) {
            SPADES_MARK_FUNCTION();
            
            orientation = v;
        }
        
        void Player::Turn(float longitude, float latitude) {
            SPADES_MARK_FUNCTION();
            
            Vector3 o = GetFront();
            float lng = atan2f(o.y, o.x);
            float lat = atan2f(o.z, sqrtf(o.x*o.x+o.y*o.y));
            
            lng += longitude;
            lat += latitude;
            
            if (lat < -static_cast<float>(M_PI) * 0.49F)
                lat = -static_cast<float>(M_PI) * 0.49F;
            if (lat > static_cast<float>(M_PI) * 0.49F)
                lat = static_cast<float>(M_PI) * 0.49F;
            
            o.x = cosf(lng) * cosf(lat);
            o.y = sinf(lng) * cosf(lat);
            o.z = sinf(lat);
            SetOrientation(o);
        }
        
        void Player::SetHP(int hp,
                           HurtType type,
                           spades::Vector3 p) {
            health = hp;
            if (this == world->GetLocalPlayer()) {
                if (world->GetListener())
                    world->GetListener()->LocalPlayerHurt(type,
                                                          p.x != 0.0F ||
                                                          p.y != 0.0F ||
                                                          p.z != 0.0F,
                                                          p);
            }
        }
        
        void Player::Update(float dt) {
            SPADES_MARK_FUNCTION();
            auto* listener = world->GetListener();
            
            MovePlayer(dt);
            
            if (!IsAlive()) {
                // do death cleanup
                blockCursorDragging = false;
            }
            
            if (tool == ToolSpade) {
                if (weapInput.primary) {
                    if (world->GetTime() > nextSpadeTime) {
                        UseSpade();
                        nextSpadeTime = world->GetTime() + GetToolPrimaryDelay();
                    }
                } else if (weapInput.secondary) {
                    if (world->GetTime() > nextDigTime) {
                        DigWithSpade();
                        nextDigTime = world->GetTime() + GetToolSecondaryDelay();
                        firstDig = false;
                    }
                }
            } else if (tool == ToolBlock && IsLocalPlayer()) {
                auto* map = GetWorld()->GetMap();
                GameMap::RayCastResult result = map->CastRay2(GetEye(), GetFront(), int(Hysteria::BlockPlaceLimit));

                canPending = false;

                if (blockCursorDragging) {
                    // check the starting point is not floating
                    auto start = blockCursorDragPos;
                    if (map->IsSolidWrapped(start.x-1, start.y, start.z) ||
                       map->IsSolidWrapped(start.x, start.y-1, start.z) ||
                       map->IsSolidWrapped(start.x, start.y, start.z-1) ||
                       map->IsSolidWrapped(start.x+1, start.y, start.z) ||
                       map->IsSolidWrapped(start.x, start.y+1, start.z) ||
                       map->IsSolidWrapped(start.x, start.y, start.z+1)) {
                        // still okay
                    } else {
                        // cannot build; floating
                        if (listener &&
                           this == world->GetLocalPlayer()) {
                            listener->
                            LocalPlayerBuildError(BuildFailureReason::InvalidPosition);
                        }
                        blockCursorDragging = false;
                    }
                }
                
                if (result.hit // The ray caster hit something
                    && (((result.hitBlock + result.normal).z < 62) || bool(Hysteria::IgnoreHeightLimit)) // Ceiling limit
                    && not(OverlapsWithOneBlock(result.hitBlock + result.normal)) // Block overlap check
                    && BoxDistanceToBlock(result.hitBlock + result.normal) < int(Hysteria::BlockPlaceLimit) // Placement limit check
                    && not(pendingPlaceBlock))
                {
                    
                    // Building is possible, and there's no delayed block placement.
                    blockCursorActive = true;
                    blockCursorPos = result.hitBlock + result.normal;
                    
                } else if (pendingPlaceBlock) {
                    
                    // Delayed Block Placement: When player attempts to place a block while jumping and
                    // placing block is currently impossible, building will be delayed until it becomes
                    // possible, as long as player is airborne.
                    if (not airborne || blockStocks <= 0) {
                        // player is no longer airborne, or doesn't have a block to place.
                        pendingPlaceBlock = false;
                        lastSingleBlockBuildSeqDone = true;
                        if (blockStocks > 0) {
                            // cannot build; invalid position.
                        }
                    } else if (not(OverlapsWithOneBlock(pendingPlaceBlockPos))) {
                        // now building became possible.
                        SPAssert(this == world->GetLocalPlayer());
                        
                        if (GetWorld()->GetListener()) {
                            GetWorld()->GetListener()->LocalPlayerBlockAction(pendingPlaceBlockPos, BlockActionCreate);
                        }
                        
                        pendingPlaceBlock = false;
                        lastSingleBlockBuildSeqDone = true;
                        // blockStocks--; decrease when created
                        
                        nextBlockTime = world->GetTime() + GetToolPrimaryDelay();
                    }
                    
                } else {
                    // Delayed Block Placement can be activated only when the only reason making placement
                    // impossible is that block to be placed overlaps with the player's hitbox.
                    canPending = result.hit
                                 && ((result.hitBlock + result.normal).z < 62 || bool(Hysteria::IgnoreHeightLimit))
                                 && BoxDistanceToBlock(result.hitBlock + result.normal) < int(Hysteria::BlockPlaceLimit);
                    
                    blockCursorActive = false;

                    {
                        int dist = 11;

                        while ((dist >= 1) && BoxDistanceToBlock(result.hitBlock + result.normal) > 3.0F) {
                            --dist;
                            result = GetWorld()->GetMap()->CastRay2(GetEye(), GetFront(), dist);
                        }

                        while ((dist < 12) && BoxDistanceToBlock(result.hitBlock + result.normal) < 3.0F) {
                            ++dist;
                            result = GetWorld()->GetMap()->CastRay2(GetEye(), GetFront(), dist);
                        }
                    }
                    
                    blockCursorPos = result.hitBlock + result.normal;
                }
                
            } else if (tool == ToolGrenade) {
                if (holdingGrenade) {
                    if (world->GetTime() - grenadeTime > float(Hysteria::GrenadeHoldTime)) {
                        ThrowGrenade();
                    }
                }
            }
        
            if (tool != ToolWeapon)
                weapon->SetShooting(false);
            if (weapon->FrameNext(dt)) {
                FireWeapon();
            }
            
            if (weapon->IsReloading()) {
                lastReloadingTime = world->GetTime();
            } else if (reloadingServerSide) {
                // for some reason a server didn't return
                // WeaponReload packet.
                if (world->GetTime() + lastReloadingTime + 0.8F) {
                    reloadingServerSide = false;
                    weapon->ForceReloadDone();
                }
            }
        }
        
        bool Player::RayCastApprox(spades::Vector3 start, spades::Vector3 dir) {
            Vector3 diff = position - start;
            
            // |P-A| * cos(theta)
            float c = Vector3::Dot(diff, dir);
            
            // |P-A|^2
            float sq = diff.GetPoweredLength();
            
            // |P-A| * sin(theta)
            float dist = sqrtf(sq - c * c);
            
            return dist < 8.0F;
        }
        
        enum class HitBodyPart {
            None,
            Head,
            Torso,
            Limb1, Limb2,
            Arms
        };
        
        /**
         * This is where the magic happens for weapon discharge and target location!
         */
        void Player::FireWeapon() {
            SPADES_MARK_FUNCTION();
            
            Vector3 muzzle = GetEye() + (GetFront() * 0.01F);
            
            // for hit-test debugging
            std::map<int, HitTestDebugger::PlayerHit> playerHits;
            std::vector<Vector3> bulletVectors;
            
            int pellets  = weapon->GetPelletSize();
            float spread = weapon->GetSpread();
            GameMap *map = world->GetMap();
            
            // pyspades takes destroying more than one block as a
            // speed hack (shotgun does this -- LOL, more proof that pyspades is a steaming pile of garbage!)
            bool blockDestroyed = false;
            
            Vector3 dir2 = GetFront();

            repeat (pellets, _) {
                
                // AoS 0.75's way (dir2 shouldn't be normalized!)
                dir2.x += (GetRandom() - GetRandom()) * spread;
                dir2.y += (GetRandom() - GetRandom()) * spread;
                dir2.z += (GetRandom() - GetRandom()) * spread;

                Vector3 dir = dir2.Normalize();
                
                bulletVectors.push_back(dir);
                
                // first do map raycast
                GameMap::RayCastResult mapResult;
                mapResult = map->CastRay2(muzzle, dir, 500);
                
                Player *hitPlayer       = nullptr;
                float hitPlayerDistance = 0.0F;
                HitBodyPart hitPart     = HitBodyPart::None;
                
                repeat (world->GetNumPlayerSlots(), i) {
                    Player* other = world->GetPlayer(i);

                    // don't shoot ourself
                    if (other == this || other == nullptr) {
                        continue;
                    }

                    // don't shoot dead players, or spectators
                    if (!other->IsAlive() || other->GetTeamId() >= 2) {
                        continue;
                    }

                    // quickly reject players unlikely to be hit
                    unless (other->RayCastApprox(muzzle, dir)) {
                        continue;
                    }
                    
                    HitBoxes hb = other->GetHitBoxes();
                    Hysteria::ScaleHitboxes(hb, float(Hysteria::HBScale));

                    Vector3 hitPos;
                    
                    if (hb.head.RayCast(muzzle, dir, &hitPos)) {
                        float const dist = (hitPos - muzzle).GetLength();

                        if (hitPlayer == nullptr || dist < hitPlayerDistance) {
                            hitPlayer         = other;
                            hitPlayerDistance = dist;
                            hitPart           = HitBodyPart::Head;
                        }
                    } else if (hb.torso.RayCast(muzzle, dir, &hitPos)) {
                        float const dist = (hitPos - muzzle).GetLength();

                        if (hitPlayer == nullptr || dist < hitPlayerDistance) {
                            hitPlayer         = other;
                            hitPlayerDistance = dist;
                            hitPart           = HitBodyPart::Torso;
                        }
                    } else {
                        repeat (3, j) {
                            if (hb.limbs[j].RayCast(muzzle, dir, &hitPos)) {
                                float dist = (hitPos - muzzle).GetLength();
                                if (hitPlayer == nullptr || dist < hitPlayerDistance) {
                                    hitPlayer         = other;
                                    hitPlayerDistance = dist;

                                    switch (j) {
                                        case 0: 
                                            hitPart = HitBodyPart::Limb1; 
                                            break;
                                        case 1: 
                                            hitPart = HitBodyPart::Limb2; 
                                            break;
                                        case 2: 
                                            hitPart = HitBodyPart::Arms;  
                                            break;
                                    }

                                    break;
                                }
                            }
                        }
                    }


                }
                
                Vector3 finalHitPos  = muzzle + dir * 128.0F;
                
                if (not(bool(Hysteria::WeaponsIgnoreMap))  
                    && mapResult.hit && (mapResult.hitPos - muzzle).GetLength() < int(Hysteria::WeaponDistBreak)
                    && (hitPlayer == nullptr || (mapResult.hitPos - muzzle).GetLength() < hitPlayerDistance)) 
                {
                    IntVector3 outBlockCoord = mapResult.hitBlock;
                    // TODO: set correct ray distance
                    // FIXME: why ray casting twice?
                    
                    finalHitPos = mapResult.hitPos;
                    
                    if (outBlockCoord.x >= 0 && outBlockCoord.y >= 0 && outBlockCoord.z >= 0 
                        && outBlockCoord.x < map->Width() && outBlockCoord.y < map->Height() 
                        && outBlockCoord.z < map->Depth())
                    {

                        if (outBlockCoord.z > 61) {
                            // blocks at this level cannot be damaged
                            if (world->GetListener()) {
                                world->GetListener()->BulletHitBlock(mapResult.hitPos,
                                                                     mapResult.hitBlock,
                                                                     mapResult.normal);
                            }
                        } else {
                            int const x = outBlockCoord.x;
                            int const y = outBlockCoord.y;
                            int const z = outBlockCoord.z;

                            SPAssert(map->IsSolid(x, y, z));
                            
                            uint32_t color       = map->GetColor(x, y, z);
                            float const distance = ({
                                    Vector3 const blockF(x + 0.5F, y + 0.5F, z + 0.5F);
                                    (blockF - muzzle).GetLength();
                            });

                            int health           = (color >> 24) - weapon->GetDamage(HitTypeBlock, distance);
                            
                            if (health <= 0 && !blockDestroyed) {
                                health         = 0;
                                blockDestroyed = true;

                                //send destroy cmd
                                if (world->GetListener() &&    world->GetLocalPlayer() == this) {
                                    world->GetListener()->LocalPlayerBlockAction (outBlockCoord, BlockActionTool);
                                }
                            }

                            color = (color & 0xffffff) | (uint32_t(health) << 24);
                            
                            if (map->IsSolid(x, y, z)) {
                                map->Set(x, y, z, true, color);
                            }
                            
                            if (world->GetListener()) {
                                world->GetListener()->BulletHitBlock(mapResult.hitPos,
                                                                     mapResult.hitBlock,
                                                                     mapResult.normal);
                            }
                        }
                    }
                } else if (hitPlayer != nullptr) {
                    if (hitPlayerDistance < int(Hysteria::WeaponDistKill)) {
                        
                        finalHitPos = muzzle + dir * hitPlayerDistance;
                        
                        HitType hitType;

                        if (Hysteria::HeadShot) {
                            hitType = HitTypeHead;
                        } else {
                            switch(hitPart) {
                                case HitBodyPart::Head:
                                    playerHits[hitPlayer->GetId()].numHeadHits++;
                                    hitType = HitTypeHead;
                                    break;
                                case HitBodyPart::Torso:
                                    playerHits[hitPlayer->GetId()].numTorsoHits++;
                                    hitType = HitTypeTorso;
                                    break;
                                case HitBodyPart::Limb1:
                                    playerHits[hitPlayer->GetId()].numLimbHits[0]++;
                                    hitType = HitTypeLegs;
                                    break;
                                case HitBodyPart::Limb2:
                                    playerHits[hitPlayer->GetId()].numLimbHits[1]++;
                                    hitType = HitTypeLegs;
                                    break;
                                case HitBodyPart::Arms:
                                    playerHits[hitPlayer->GetId()].numLimbHits[2]++;
                                    hitType = HitTypeArms;
                                    break;
                                case HitBodyPart::None:
                                    SPAssert(false);
                                    break;
                            }
                        }

                        if (world->GetListener()) {
                            world->GetListener()->BulletHitPlayer(hitPlayer, hitType, finalHitPos, this);
                        }
                    }
                }
                
                if (world->GetListener() && this != world->GetLocalPlayer()) {
                    world->GetListener()->AddBulletTracer(this, muzzle, finalHitPos);
                }
                
                // one pellet done
            }
            
            // do hit test debugging
            auto *debugger = world->GetHitTestDebugger();
            if (debugger && IsLocalPlayer()) {
                debugger->SaveImage(playerHits, bulletVectors);
            }
            
            reloadingServerSide = false;
        }
        
        void Player::ThrowGrenade() {
            SPADES_MARK_FUNCTION();
            
            unless (holdingGrenade) {
                return;
            }
        

            Vector3 const muzzle = GetEye() + GetFront() * 0.1F;
            float const fuse     = 3.0F - (world->GetTime() - grenadeTime);
            Vector3 const vel    = ({
                    (health > 0) 
                        ? GetFront() 
                            * float(Hysteria::GrenadeSpeedTrim)
                            + GetVelocty()
                        : Vector3(0, 0, 0);
            });
            
            if (this == world->GetLocalPlayer()) {
                Grenade *gren = new Grenade(world, muzzle, vel, fuse);
                world->AddGrenade(gren);
                if (world->GetListener())
                    world->GetListener()->PlayerThrownGrenade(this, gren);
            } else {
                // grenade packet will be sent by server
                if (world->GetListener())
                    world->GetListener()->PlayerThrownGrenade(this, nullptr);
            }
            
            holdingGrenade = false;
            --grenades;
        }
        
        void Player::DigWithSpade() {
            SPADES_MARK_FUNCTION();
            
            IntVector3 outBlockCoord;
            GameMap *map = world->GetMap();
            Vector3 muzzle = GetEye(), dir = GetFront();
            
            // TODO: set correct ray distance
            // first do map raycast
            GameMap::RayCastResult mapResult;
            mapResult = map->CastRay2(muzzle,
                                      dir,
                                      256);
        
            outBlockCoord = mapResult.hitBlock;
            
            // TODO: set correct ray distance
            if (mapResult.hit && BoxDistanceToBlock(mapResult.hitBlock + mapResult.normal) < 3.0F &&
               outBlockCoord.x >= 0 && outBlockCoord.y >= 0 && outBlockCoord.z >= 0 &&
               outBlockCoord.x < map->Width() && outBlockCoord.y < map->Height() &&
               outBlockCoord.z < map->Depth()) {
                if (outBlockCoord.z < 62) {
                    SPAssert(map->IsSolid(outBlockCoord.x, outBlockCoord.y, outBlockCoord.z));
                    
                    // send destroy command only for local cmd
                    if (this == world->GetLocalPlayer()) {
                    
                        if (world->GetListener())
                            world->GetListener()->LocalPlayerBlockAction
                            (outBlockCoord, BlockActionDig);
                    
                        
                    }
                    
                    if (world->GetListener())
                        world->GetListener()->PlayerHitBlockWithSpade(this,
                                                                      mapResult.hitPos,
                                                                      mapResult.hitBlock,
                                                                      mapResult.normal);
                }
            } else {
                if (world->GetListener())
                    world->GetListener()->PlayerMissedSpade(this);
            }
        }
        
        void Player::UseSpade() {
            SPADES_MARK_FUNCTION();
            
            bool missed = true;
            
            Vector3 muzzle = GetEye(), dir = GetFront();
            
            IntVector3 outBlockCoord;
            GameMap *map = world->GetMap();
            // TODO: set correct ray distance
            // first do map raycast
            GameMap::RayCastResult mapResult;
            mapResult = map->CastRay2(muzzle,
                                      dir,
                                      256);
            
            Player *hitPlayer = nullptr;
            int hitFlag = 0;
            
            repeat (world->GetNumPlayerSlots(), i) {
                Player* other = world->GetPlayer(i);

                if (other == this || other == nullptr) {
                    continue;
                } if (!other->IsAlive() || other->GetTeamId() >= 2) {
                    continue;
                } if (!other->RayCastApprox(muzzle, dir)) {
                    continue;
                } else if ((eye - other->GetEye()).GetChebyshevLength() >= MELEE_DISTANCE_F) {
                    continue;
                }
                
                Vector3 view = ({
                        Vector3 const diff = other->GetEye() - eye;
                        Vector3 (
                            Vector3::Dot(diff, GetRight()),
                            Vector3::Dot(diff, GetUp()),
                            Vector3::Dot(diff, GetFront())
                        );
                });
                
                unless (view.z >= 0.0F) {
                    view.x /= view.z;
                    view.y /= view.z;
                    view.z  = 0.0F;
                    
                    if (view.GetChebyshevLength() < 5.0F) {
                        hitPlayer = other;
                        hitFlag = 1;
                        break;
                    }
                }
            }
            
            outBlockCoord = mapResult.hitBlock;
            if (mapResult.hit && BoxDistanceToBlock(mapResult.hitBlock + mapResult.normal) < 3.0F && (hitPlayer == nullptr) &&
                outBlockCoord.x >= 0 && outBlockCoord.y >= 0 && outBlockCoord.z >= 0 &&
                outBlockCoord.x < map->Width() && outBlockCoord.y < map->Height() && outBlockCoord.z < map->Depth()) {

                if (outBlockCoord.z < 62) {
                    int x  = outBlockCoord.x;
                    int y  = outBlockCoord.y;
                    int z  = outBlockCoord.z;
                    missed = false;

                    SPAssert(map->IsSolid(x, y, z));
                    
                    uint32_t color = map->GetColor(x, y, z);
                    int health = color >> 24;
                    health -= 55;
                    if (health <= 0) {
                        health = 0;
                        // send destroy command only for local cmd
                        if (this == world->GetLocalPlayer()) {
                            if (world->GetListener())
                                world->GetListener()->LocalPlayerBlockAction(outBlockCoord, BlockActionTool);
                        }
                    }
                    color = (color & 0xffffff) | ((uint32_t)health << 24);
                    if (map->IsSolid(x, y, z))
                        map->Set(x, y, z, true, color);
                    
                    if (world->GetListener())
                        world->GetListener()->PlayerHitBlockWithSpade(this,
                                                                      mapResult.hitPos,
                                                                      mapResult.hitBlock,
                                                                      mapResult.normal);
                }
            } else if (hitPlayer != nullptr) {
                
                if (world->GetListener()) {
                    if (hitFlag)
                        world->GetListener()->BulletHitPlayer(hitPlayer,
                                                              HitTypeMelee,
                                                              hitPlayer->GetEye(),
                                                              this);
                }
                
            }
            
            if (missed) {
                if (world->GetListener())
                    world->GetListener()->PlayerMissedSpade(this);
            }
        }
        
        Vector3 Player::GetFront() {
            SPADES_MARK_FUNCTION_DEBUG();
            return orientation;
        }
        
        Vector3 Player::GetFront2D() {
            SPADES_MARK_FUNCTION_DEBUG();
            return MakeVector3(orientation.x,
                               orientation.y,
                               0.0F).Normalize();
        }
        
        Vector3 Player::GetRight() {
            SPADES_MARK_FUNCTION_DEBUG();
            return -Vector3::Cross(MakeVector3(0,0,-1),
                                  GetFront2D()).Normalize();
        }
        
        Vector3 Player::GetLeft() {
            SPADES_MARK_FUNCTION_DEBUG();
            return -GetRight();
        }
        
        Vector3 Player::GetUp() {
            SPADES_MARK_FUNCTION_DEBUG();
            return Vector3::Cross(GetRight(), GetFront())
            .Normalize();
        }
        
        bool Player::GetWade() {
            SPADES_MARK_FUNCTION_DEBUG();
            return GetOrigin().z > 62.0F;
        }
        
        Vector3 Player::GetOrigin() {
            SPADES_MARK_FUNCTION_DEBUG();
            Vector3 v = eye;
            v.z += (input.crouch ? 0.45F : 0.9F);
            v.z += 0.3F;
            return v;
        }
        
        void Player::BoxClipMove(float fsynctics) {
            // This is the exact constant PySpades checks against to determine whether or not to deal fall damage
            // If your Z-Axis velocity exceeds this value **>>>>> ___ON THE SERVER___ <<<<<**, you will be dealt fall damage
            static float const FALL_SLOW  = 0.24F;
            static float const LOOK_AHEAD = 0.45F;

            SPADES_MARK_FUNCTION();
            
            float const fsyncticsAdjusted = fsynctics * 32.0F;
            
            float offset    = 0.9F;
            float maxZWedge = 1.35F;
            {
                if (input.crouch) {
                    offset    = 0.45F;
                    maxZWedge = 0.9F;
                }
            }

            
            // Next X Coordinate
            float nx = fsyncticsAdjusted * velocity.x + position.x;

            // Next Y Coordinate
            float ny = fsyncticsAdjusted * velocity.y + position.y;

            // Next Z Coordinate
            float nz = position.z + offset;
            
            GameMap *map = world->GetMap();
            bool climb   = false;
        
            {
                float zWedge = maxZWedge;

                float xLookAhead = LOOK_AHEAD;
                {
                    if (velocity.x < 0.0F) {
                        xLookAhead = -xLookAhead;
                    }
                }

                while (zWedge >= -1.36F
                       && not(map->ClipBox(nx + xLookAhead, position.y - LOOK_AHEAD, nz + zWedge))
                       && not(map->ClipBox(nx + xLookAhead, position.y + LOOK_AHEAD, nz + zWedge)))
                {
                    zWedge -= 0.9F;
                }

                if (zWedge < -1.36F || bool(Hysteria::HorizontalNoclip)) {
                    
                    position.x = nx;

                } else if (orientation.z < 0.5F) {
                
                    {
                        float zWedge = 0.35F;
                    
                        while (zWedge >= -2.36F 
                               && not(map->ClipBox(nx + xLookAhead, position.y - LOOK_AHEAD, nz + zWedge))
                               && not(map->ClipBox(nx + xLookAhead, position.y + LOOK_AHEAD, nz + zWedge)))
                        {
                            zWedge -= 0.9F;
                        }

                        if (zWedge < -2.36F) {
                            position.x = nx;
                            climb      = true;
                        } else {
                            velocity.x = 0.0F;
                        }
                    }

                } else {

                    velocity.x = 0.0F;

                }
            
            }

            {
                float zWedge = maxZWedge;

                float yLookAhead = LOOK_AHEAD;
                {
                    if (velocity.y < 0.0F) {
                        yLookAhead = -yLookAhead;
                    }
                }
                
                while (zWedge >= -1.36F 
                       && not(map->ClipBox(position.x - LOOK_AHEAD, ny + yLookAhead, nz + zWedge)) 
                       && not(map->ClipBox(position.x + LOOK_AHEAD, ny + yLookAhead, nz + zWedge))) 
                {
                    zWedge -= 0.9F;
                }

                if (zWedge < -1.36F || bool(Hysteria::HorizontalNoclip)) {

                    position.y = ny;

                } else if (orientation.z < 0.5F && not(climb)) {
                    {
                        float zWedge = 0.35F;

                        while (zWedge >= -2.36F
                               && not(map->ClipBox(position.x - LOOK_AHEAD, ny + yLookAhead, nz + zWedge))
                               && not(map->ClipBox(position.x + LOOK_AHEAD, ny + yLookAhead, nz + zWedge)))
                        {
                            zWedge -= 0.9F;
                        }

                        if (zWedge < -2.36F) {
                            position.y = ny;
                            climb      = true;
                        } else {
                            velocity.y = 0.0F;
                        }
                    }
                } else if (not(climb)) {
                    velocity.y = 0.0F;
                }
            }
            
            {
                float zWedge = maxZWedge;

                if (climb) {
                    velocity.x      *= 0.5F;
                    velocity.y      *= 0.5F;
                    nz              -= 1.0F;

                    lastClimbTime    = world->GetTime();
                    zWedge           = -1.35F;
                } else {

                    // Hard limit Z velocity to FALL_SLOW
                    if (velocity.z > float(Hysteria::ZVelocityLimit)) {
                        velocity.z = Hysteria::ZVelocityLimit;
                    }

                    if (velocity.z < 0.0F) {
                        zWedge = -zWedge;
                    }

                    nz += fsyncticsAdjusted * velocity.z;
                
                }
                
                airborne = true;

                if  (  map->ClipBox(position.x - LOOK_AHEAD, position.y - LOOK_AHEAD, nz + zWedge)
                    || map->ClipBox(position.x - LOOK_AHEAD, position.y + LOOK_AHEAD, nz + zWedge)
                    || map->ClipBox(position.x + LOOK_AHEAD, position.y - LOOK_AHEAD, nz + zWedge)
                    || map->ClipBox(position.x + LOOK_AHEAD, position.y + LOOK_AHEAD, nz + zWedge) ) 
                {
                    
                    if (velocity.z >= 0.0F) {

                        // Hardcoded water depth is 61 blocks from top of the map
                        wade     = position.z > 61.0F;
                        airborne = false;

                    }

                    velocity.z = 0.0F;

                } else {
                    position.z = nz - offset;
                }
            }
            
            RepositionPlayer(position);
        }
        
        bool Player::IsOnGroundOrWade() {
            return ((velocity.z >= 0.0F && velocity.z < 0.017F) && !airborne);
        }
        
        void Player::ForceJump() {
            velocity.z = -0.36F;
            lastJump = true;
            if (world->GetListener() && world->GetTime() > lastJumpTime + 0.1F) {
                world->GetListener()->PlayerJumped(this);
                lastJumpTime = world->GetTime();
            }
        }
        
        void Player::MovePlayer(float fsynctics) {
            bool const isLocal = (this == world->GetLocalPlayer());

            if (input.jump && not(lastJump) && IsOnGroundOrWade()) {
                
                velocity.z = -float(Hysteria::JumpVelocity);
                
                lastJump = true;

                if (world->GetListener() && world->GetTime() > lastJumpTime + 0.1F) {
                    world->GetListener()->PlayerJumped(this);
                    lastJumpTime = world->GetTime();
                }

            } else if (!input.jump) {
                lastJump = false;
            }
            
            float f = fsynctics * ( isLocal ? float(Hysteria::BaseMovementTrim) : 1.0F);
            {
                if (airborne && not(Hysteria::NoAirborneSlowdown)) {
                    f *= 0.1F;
                } else if (input.crouch && not(Hysteria::NoCrouchSlowdown)) {
                    f *= 0.3F;
                } else if ((weapInput.secondary && IsToolWeapon())) {
                    f *= float(Hysteria::AimMovementTrim);
                } else if (input.sneak && not(Hysteria::NoSneakSlowdown)) {
                    f *= 0.5F;
                } else if (input.sprint) {
                    f *= isLocal ? float(Hysteria::SprintSpeedTrim) : 1.3F;
                }
            }

            if ((input.moveForward || input.moveBackward) && (input.moveRight || input.moveLeft)) {
                f /= sqrtf(2.0F);
            }
            
            // looking up or down should alter speed
            float const maxVertLookSlowdown   = 0.9F;
            float const vertLookSlowdownStart = 0.65F; // about 40 degrees
            float slowdownByVertLook =
                std::max(std::abs(GetFront().z) - vertLookSlowdownStart, 0.0F) /
                (1.0F - vertLookSlowdownStart) * maxVertLookSlowdown;

            Vector3 front = GetFront2D() * (1.0F - slowdownByVertLook);
            Vector3 left = GetLeft();

            if (input.moveForward) {
                velocity.x += front.x * f;
                velocity.y += front.y * f;
            } else if (input.moveBackward) {
                velocity.x -= front.x * f;
                velocity.y -= front.y * f;
            }
            if (input.moveLeft) {
                velocity.x += left.x * f;
                velocity.y += left.y * f;
            } else if (input.moveRight) {
                velocity.x -= left.x * f;
                velocity.y -= left.y * f;
            }
            
            // this is a linear approximation that's
            // done in pysnip
            // accurate computation is not difficult
            f = fsynctics + 1.0F;
            velocity.z += fsynctics;
            velocity.z /= f; // air friction
            
            if (wade && not(Hysteria::NoWadeSlowdown)) {
                f = fsynctics * 6.0F + 1.0F;
            } else if (not(airborne) || not(Hysteria::NoAirborneSlowdown)) {
                f = fsynctics * 4.0F + 1.0F;
            }
            
            velocity.x /= f;
            velocity.y /= f;
            
            float f2 = velocity.z;
            BoxClipMove(fsynctics);
            
            // hit ground
            if (velocity.z == 0.0F && (f2 > FALL_SLOW_DOWN)) {
                velocity.x *= 0.5F;
                velocity.y *= 0.5F;
                
                if (f2 > FALL_DAMAGE_VELOCITY) {
                    f2 -= FALL_DAMAGE_VELOCITY;
                    if (world->GetListener()) {
                        world->GetListener()->PlayerLanded(this, true);
                    }
                } else {
                    if (world->GetListener()) {
                        world->GetListener()->PlayerLanded(this, false);
                    }
                }
            }
            
            if  (  velocity.z >= 0.0F 
                && velocity.z < 0.017F 
                && not(input.sneak)
                && not(input.crouch)
                && not(weapInput.secondary && IsToolWeapon())) 
            {
                // count move distance
                f          = fsynctics * 32.0F;
                float dx   = f * velocity.x;
                float dy   = f * velocity.y;
                float dist = sqrtf(dx*dx+dy*dy);
                moveDistance += dist * 0.3F;
                
                bool madeFootstep = false;
                while (moveDistance > 1.0F) {
                    moveSteps++;
                    moveDistance -= 1.0F;
                    
                    if (world->GetListener() && !madeFootstep) {
                        world->GetListener()->PlayerMadeFootstep(this);
                        madeFootstep = true;
                    }
                }
            }
        }
        
        bool Player::TryUncrouch(bool move) {
            SPADES_MARK_FUNCTION();
            
            float x1 = position.x + 0.45F;
            float x2 = position.x - 0.45F;
            float y1 = position.y + 0.45F;
            float y2 = position.y - 0.45F;
            float z1 = position.z + 2.25F;
            float z2 = position.z - 1.35F;
            
            GameMap *map = world->GetMap();
            
            // lower feet
            if (airborne &&
               !(map->ClipBox(x1, y1, z1) ||
                 map->ClipBox(x2, y1, z1) ||
                 map->ClipBox(x1, y2, z1) ||
                 map->ClipBox(x2, y2, z1)))
                return true;
            else if (!(map->ClipBox(x1, y1, z2) ||
                      map->ClipBox(x2, y1, z2) ||
                      map->ClipBox(x1, y2, z2) ||
                      map->ClipBox(x2, y2, z2))) {
                if (move) {
                    position.z -= 0.9F;
                    eye.z -= 0.9F;
                }
                return true;
            }
            return false;
        }
        
        void Player::RepositionPlayer(const spades::Vector3 & pos2) {
            SPADES_MARK_FUNCTION();
            
            eye = position;
            eye = pos2;
            float f = lastClimbTime - world->GetTime();
            if (f > -0.25F) {
                eye.z += (f + 0.25F) / 0.25F;
            }
        }
        
        float Player::GetToolPrimaryDelay() {
            SPADES_MARK_FUNCTION_DEBUG();
            
            switch(tool) {
                case ToolWeapon:
                    return weapon->GetDelay();
                case ToolBlock:
                    return float(Hysteria::BlockPlaceDelay);
                case ToolSpade:
                    return float(Hysteria::SpadeDelay);
                case ToolGrenade:
                    return float(Hysteria::GrenadeDelay);
                default:
                    SPInvalidEnum("tool", tool);
            }
        }
        
        float Player::GetToolSecondaryDelay() {
            SPADES_MARK_FUNCTION_DEBUG();
            
            switch(tool) {
                case ToolBlock:
                    return GetToolPrimaryDelay();
                case ToolSpade:
                    return float(Hysteria::SpadeDigDelay);
                default:
                    SPInvalidEnum("tool", tool);
            }
        }
        
        float Player::GetSpadeAnimationProgress() {
            SPADES_MARK_FUNCTION_DEBUG();
            
            SPAssert(tool == ToolSpade);
            SPAssert(weapInput.primary);
            return 1.0F - (nextSpadeTime - world->GetTime())
            / GetToolPrimaryDelay();
        }
        
        float Player::GetDigAnimationProgress() {
            SPADES_MARK_FUNCTION_DEBUG();
            
            SPAssert(tool == ToolSpade);
            SPAssert(weapInput.secondary);
            return 1.0F - (nextDigTime - world->GetTime())
            / GetToolSecondaryDelay();
        }
        
        float Player::GetTimeToNextGrenade() {
            return nextGrenadeTime - world->GetTime();
        }
        
        void Player::KilledBy(KillType type,
                            Player *killer,
                              int respawnTime) {
            SPADES_MARK_FUNCTION();
            health = 0;
            weapon->SetShooting(false);
            
            // if local player is killed while cooking grenade,
            // drop the live grenade.
            if (this == world->GetLocalPlayer() &&
               tool == ToolGrenade &&
               holdingGrenade) {
                ThrowGrenade();
            }
            if (world->GetListener())
                world->GetListener()->PlayerKilledPlayer(killer, this, type);
            
            input = PlayerInput();
            weapInput = WeaponInput();
            this->respawnTime = world->GetTime() + respawnTime;
        }
        
        bool Player::IsAlive() {
            return health > 0;
        }
        
        std::string Player::GetName() {
            return world->GetPlayerPersistent(GetId()).name;
        }
        
        float Player::GetWalkAnimationProgress() {
            return moveDistance * 0.5F +
            (float)(moveSteps & 1) * 0.5F;
        }
        
        Player::HitBoxes Player::GetHitBoxes()
        {
            SPADES_MARK_FUNCTION_DEBUG();
            Player::HitBoxes hb;
            
            Vector3 front = GetFront();
            
            float yaw = atan2(front.y, front.x) + static_cast<float>(M_PI) * 0.5F;
            float pitch = -atan2(front.z, sqrt(front.x * front.x + front.y * front.y));
            
            // lower axis
            Matrix4 lower = Matrix4::Translate(GetOrigin());
            lower = lower * Matrix4::Rotate(MakeVector3(0,0,1), yaw);
            
            Matrix4 torso;
            
            if (input.crouch) {
                lower = lower * Matrix4::Translate(0, 0, -0.4F);
                // lower
                hb.limbs[0] = AABB3(-0.4F, -0.15F, 0.5F, 0.3F, 0.3F, 0.5F);
                hb.limbs[0] = lower * hb.limbs[0];
                
                hb.limbs[1] = AABB3(0.1F, -0.15F, 0.5F, 0.3F, 0.3F, 0.5F);
                hb.limbs[1] = lower * hb.limbs[1];
                
                torso = lower * Matrix4::Translate(0, 0, -0.3F);
                
                // torso
                hb.torso = AABB3(-0.4F, -0.15F, 0.1F, 0.8F, 0.8F, 0.6F);
                hb.torso = torso * hb.torso;
                
                hb.limbs[2] = AABB3(-0.6F, -0.15F, 0.1F, 1.2F, 0.3F, 0.6F);
                hb.limbs[2] = torso * hb.limbs[2];
                
                // head
                hb.head = AABB3(-0.3F, -0.3F, -0.45F, 0.6F, 0.6F, 0.6F);
                hb.head = Matrix4::Translate(0, 0, -0.15F) * hb.head;
                hb.head = Matrix4::Rotate(MakeVector3(1,0,0), pitch) * hb.head;
                hb.head = Matrix4::Translate(0, 0, 0.15F) * hb.head;
                hb.head = torso * hb.head;
            } else {
                // lower
                hb.limbs[0] = AABB3(-0.4F, -0.15F, 0.0F, 0.3F, 0.3F, 1.0F);
                hb.limbs[0] = lower * hb.limbs[0];
                
                hb.limbs[1] = AABB3(0.1F, -0.15F, 0.0F, 0.3F, 0.3F, 1.0F);
                hb.limbs[1] = lower * hb.limbs[1];
                
                torso = lower * Matrix4::Translate(0, 0, -1.1F);
                
                // torso
                hb.torso = AABB3(-0.4F, -0.15F, 0.1F, 0.8F, 0.3F, 0.9F);
                hb.torso = torso * hb.torso;
                
                hb.limbs[2] = AABB3(-0.6F, -0.15F, 0.1F, 1.2F, 0.3F, 0.9F);
                hb.limbs[2] = torso * hb.limbs[2];
                
                // head
                hb.head = AABB3(-0.3F, -0.3F, -0.5F, 0.6F, 0.6F, 0.6F);
                hb.head = Matrix4::Translate(0, 0, -0.1F) * hb.head;
                hb.head = Matrix4::Rotate(MakeVector3(1,0,0), pitch) * hb.head;
                hb.head = Matrix4::Translate(0, 0, 0.1F) * hb.head;
                hb.head = torso * hb.head;
            }
            
            return hb;
        }
        IntVector3 Player::GetColor() {
            return world->GetTeam(teamId).color;
        }
        
        bool Player::IsCookingGrenade() {
            return tool == ToolGrenade && holdingGrenade;
        }
        float Player::GetGrenadeCookTime() {
            return world->GetTime() - grenadeTime;
        }
        
        void Player::SetWeaponType(WeaponType weap) {
            SPADES_MARK_FUNCTION_DEBUG();
            if (this->weapon->GetWeaponType() == weap)
                return;
            delete this->weapon;
            this->weapon = Weapon::CreateWeapon(weap, this);
        }
        
        void Player::SetTeam(int tId) {
            teamId = tId;
        }
        
        bool Player::IsReadyToUseTool() {
            SPADES_MARK_FUNCTION_DEBUG();
            switch(tool) {
                case ToolBlock:
                    return world->GetTime() > nextBlockTime &&
                    blockStocks > 0;
                case ToolGrenade:
                    return world->GetTime() > nextGrenadeTime &&
                    grenades > 0;
                case ToolSpade:
                    return true;
                case ToolWeapon:
                    return weapon->IsReadyToShoot();
            }
        }
        
        bool Player::IsToolSelectable(ToolType type) {
            SPADES_MARK_FUNCTION_DEBUG();
            switch(type) {
                case ToolSpade:
                    return true;
                case ToolBlock:
                    return blockStocks > 0;
                case ToolWeapon:
                    return weapon->GetAmmo() > 0 ||
                    weapon->GetStock() > 0;
                case ToolGrenade:
                    return grenades > 0;
                default:
                    SPAssert(false);
            }
        }
        
        bool Player::OverlapsWith(const spades::AABB3 &aabb) {
            SPADES_MARK_FUNCTION_DEBUG();
            float offset, m;
            if (input.crouch) {
                offset = 0.45F;
                m = 0.9F;
            } else {
                offset = 0.9F;
                m = 1.35F;
            }
            m -= 0.5F;
            AABB3 playerBox(eye.x - 0.45F,
                            eye.y - 0.45F,
                            eye.z,
                            0.9F, 0.9F, offset + m);
            return aabb && playerBox;
        }
        
        bool Player::OverlapsWithOneBlock(spades::IntVector3 vec) {
            SPADES_MARK_FUNCTION_DEBUG();
            return OverlapsWith(AABB3(vec.x, vec.y, vec.z,
                                      1, 1, 1));
        }
        
#pragma mark - Block Construction
        bool Player::IsBlockCursorActive() {
            return tool == ToolBlock && blockCursorActive;
        }
        bool Player::IsBlockCursorDragging() {
            return tool == ToolBlock && blockCursorDragging;
        }
        float Player::BoxDistanceToBlock(spades::IntVector3 v) {
            Vector3 e = {(float)v.x, (float)v.y, (float)v.z};
            e += 0.5F;
            
            return (e-eye).GetChebyshevLength();
        }
    }
}
