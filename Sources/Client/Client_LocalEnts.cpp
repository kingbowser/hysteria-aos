/*
 Copyright (c) 2013 yvt
 based on code of pysnip (c) Mathias Kaerlev 2011-2012.
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */

#include "Client.h"
#include <cstdlib>

#include <Core/ConcurrentDispatch.h>
#include <Core/Settings.h>
#include <Core/Strings.h>

#include "IAudioChunk.h"
#include "IAudioDevice.h"

#include "ClientUI.h"
#include "PaletteView.h"
#include "LimboView.h"
#include "MapView.h"
#include "Corpse.h"
#include "ClientPlayer.h"
#include "ILocalEntity.h"
#include "ChatWindow.h"
#include "CenterMessageView.h"
#include "Tracer.h"
#include "FallingBlock.h"
#include "HurtRingView.h"
#include "ParticleSpriteEntity.h"
#include "SmokeSpriteEntity.h"

#include "World.h"
#include "Weapon.h"
#include "GameMap.h"
#include "Grenade.h"

#include "NetClient.h"

SPADES_SETTING(cg_blood, "1");
SPADES_SETTING(cg_reduceSmoke, "0");
SPADES_SETTING(cg_waterImpact, "1");

namespace spades {
	namespace client {
		
		
#pragma mark - Local Entities / Effects
		
		
		void Client::RemoveAllCorpses(){
			SPADES_MARK_FUNCTION();
			
			corpses.clear();
			lastMyCorpse = nullptr;
		}
		
		
		void Client::RemoveAllLocalEntities(){
			SPADES_MARK_FUNCTION();
			
			localEntities.clear();
		}
		
		void Client::RemoveInvisibleCorpses(){
			SPADES_MARK_FUNCTION();
			
			decltype(corpses)::iterator it;
			std::vector<decltype(it)> its;
			int cnt = (int)corpses.size() - corpseSoftLimit;
			for(it = corpses.begin(); it != corpses.end(); it++){
				if(cnt <= 0)
					break;
				auto& c = *it;
				if(!c->IsVisibleFrom(lastSceneDef.viewOrigin)){
					if(c.get() == lastMyCorpse)
						lastMyCorpse = nullptr;
					its.push_back(it);
				}
				cnt--;
			}
			
			for(size_t i = 0; i < its.size(); i++)
				corpses.erase(its[i]);
			
		}
		
		
		Player *Client::HotTrackedPlayer( hitTag_t* hitFlag ){
			if(!world)
				return nullptr;
			Player *p = world->GetLocalPlayer();
			if(!p || !p->IsAlive())
				return nullptr;
			if(ShouldRenderInThirdPersonView())
				return nullptr;
			Vector3 origin = p->GetEye();
			Vector3 dir = p->GetFront();
			World::WeaponRayCastResult result = world->WeaponRayCast(origin, dir, p);
			
			if (result.hit && result.player) {
			
                if( hitFlag ) {
                    *hitFlag = result.hitFlag;
                }

                return result.player;
            } else {
                return nullptr;
            }
		}
		
		bool Client::IsMuted() {
			// prevent to play loud sound at connection
			// caused by saved packets
			return time < worldSetTime + 0.05F;
		}
		
		void Client::Bleed(spades::Vector3 v){
			SPADES_MARK_FUNCTION();
			
			if(!cg_blood)
				return;
			
			// distance cull
			if((v - lastSceneDef.viewOrigin).GetPoweredLength() >
			   150.0F * 150.0F)
				return;
			
			//Handle<IImage> img = renderer->RegisterImage("Textures/SoftBall.tga");
			Handle<IImage> img = renderer->RegisterImage("Gfx/White.tga");
			Vector4 color = {0.5F, 0.02F, 0.04F, 1.0F};
			for(int i = 0; i < 10; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				ent->SetTrajectory(v,
								   MakeVector3(GetRandom()-GetRandom(),
											   GetRandom()-GetRandom(),
											   GetRandom()-GetRandom()) * 10.0F,
								   1.0F, 0.7F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.1F + GetRandom()*GetRandom()*0.2F);
				ent->SetLifeTime(3.0F, 0.0F, 1.0F);
				localEntities.emplace_back(ent);
			}
			
			color = MakeVector4(0.7F, 0.35F, 0.37F, 0.6F);
			for(int i = 0; i < 2; i++){
				ParticleSpriteEntity *ent =
				new SmokeSpriteEntity(this, color, 100.0F,
									  SmokeSpriteEntity::Type::Explosion);
				ent->SetTrajectory(v,
								   MakeVector3(GetRandom()-GetRandom(),
											   GetRandom()-GetRandom(),
											   GetRandom()-GetRandom()) * 0.7F,
								   0.8F, 0.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.5F + GetRandom()*GetRandom()*0.2F,
							   2.0F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime(0.20F + GetRandom() * 0.2F, 0.06F, 0.20F);
				localEntities.emplace_back(ent);
			}
			
			if(cg_reduceSmoke)
				return;
			color.w *= 0.1F;
			for(int i = 0; i < 1; i++){
				ParticleSpriteEntity *ent =
				new SmokeSpriteEntity(this, color, 40.0F,
									  SmokeSpriteEntity::Type::Steady);
				ent->SetTrajectory(v,
								   MakeVector3(GetRandom()-GetRandom(),
											   GetRandom()-GetRandom(),
											   GetRandom()-GetRandom()) * 0.7F,
								   0.8F, 0.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.7F + GetRandom()*GetRandom()*0.2F,
							   2.0F, 0.1F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime(0.80F + GetRandom() * 0.4F, 0.06F, 1.0F);
				localEntities.emplace_back(ent);
			}
		}
		
		void Client::EmitBlockFragments(Vector3 origin,
										IntVector3 c){
			SPADES_MARK_FUNCTION();
			
			// distance cull
			float distPowered = (origin - lastSceneDef.viewOrigin).GetPoweredLength();
			if(distPowered >
			   150.0F * 150.0F)
				return;
			
			Handle<IImage> img = renderer->RegisterImage("Gfx/White.tga");
			Vector4 color = {c.x / 255.0F,
				c.y / 255.0F, c.z / 255.0F, 1.0F};
			for(int i = 0; i < 7; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				ent->SetTrajectory(origin,
								   MakeVector3(GetRandom()-GetRandom(),
											   GetRandom()-GetRandom(),
											   GetRandom()-GetRandom()) * 7.0F,
								   1.0F, 0.9F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.2F + GetRandom()*GetRandom()*0.1F);
				ent->SetLifeTime(2.0F, 0.0F, 1.0F);
				if(distPowered < 16.0F * 16.0F)
					ent->SetBlockHitAction(ParticleSpriteEntity::BounceWeak);
				localEntities.emplace_back(ent);
			}
			
			if(distPowered <
			   32.0F * 32.0F){
				for(int i = 0; i < 16; i++){
					ParticleSpriteEntity *ent =
					new ParticleSpriteEntity(this, img, color);
					ent->SetTrajectory(origin,
									   MakeVector3(GetRandom()-GetRandom(),
												   GetRandom()-GetRandom(),
												   GetRandom()-GetRandom()) * 12.0F,
									   1.0F, 0.9F);
					ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
					ent->SetRadius(0.1F + GetRandom()*GetRandom()*0.14F);
					ent->SetLifeTime(2.0F, 0.0F, 1.0F);
					if(distPowered < 16.0F * 16.0F)
						ent->SetBlockHitAction(ParticleSpriteEntity::BounceWeak);
					localEntities.emplace_back(ent);
				}
			}
			
			color += (MakeVector4(1, 1, 1, 1) - color) * 0.2F;
			color.w *= 0.2F;
			for(int i = 0; i < 2; i++){
				ParticleSpriteEntity *ent =
				new SmokeSpriteEntity(this, color, 100.0F);
				ent->SetTrajectory(origin,
								   MakeVector3(GetRandom()-GetRandom(),
											   GetRandom()-GetRandom(),
											   GetRandom()-GetRandom()) * 0.7F,
								   1.0F, 0.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.6F + GetRandom()*GetRandom()*0.2F,
							   0.8F);
				ent->SetLifeTime(0.3F + GetRandom() * 0.3F, 0.06F, 0.4F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				localEntities.emplace_back(ent);
			}
			
		}
		
		void Client::EmitBlockDestroyFragments(IntVector3 blk,
											   IntVector3 c){
			SPADES_MARK_FUNCTION();
			
			Vector3 origin = {blk.x + 0.5F, blk.y + 0.5F, blk.z + 0.5F};
			
			// distance cull
			if((origin - lastSceneDef.viewOrigin).GetPoweredLength() >
			   150.0F * 150.0F)
				return;
			
			Handle<IImage> img = renderer->RegisterImage("Gfx/White.tga");
			Vector4 color = {c.x / 255.0F,
				c.y / 255.0F, c.z / 255.0F, 1.0F};
			for(int i = 0; i < 8; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				ent->SetTrajectory(origin,
								   MakeVector3(GetRandom()-GetRandom(),
											   GetRandom()-GetRandom(),
											   GetRandom()-GetRandom()) * 7.0F,
								   1.0F, 1.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.3F + GetRandom()*GetRandom()*0.2F);
				ent->SetLifeTime(2.0F, 0.0F, 1.0F);
				ent->SetBlockHitAction(ParticleSpriteEntity::BounceWeak);
				localEntities.emplace_back(ent);
			}
		}
		
		void Client::MuzzleFire(spades::Vector3 origin,
								spades::Vector3 dir,
								bool local) {
			DynamicLightParam l;
			l.origin = origin;
			l.radius = 5.0F;
			l.type = DynamicLightTypePoint;
			l.color = MakeVector3(3.0F, 1.6F, 0.5F);
			flashDlights.push_back(l);
			
			Vector4 color;
			Vector3 velBias = {0, 0, -0.5F};
			color = MakeVector4( 0.8F, 0.8F, 0.8F, 0.3F);
			
			// rapid smoke
			for(int i = 0; i < 2; i++){
				ParticleSpriteEntity *ent =
				new SmokeSpriteEntity(this, color, 120.0F,
									  SmokeSpriteEntity::Type::Explosion);
				ent->SetTrajectory(origin,
								   (MakeVector3(GetRandom()-GetRandom(),
												GetRandom()-GetRandom(),
												GetRandom()-GetRandom())+velBias*0.5F) * 0.3F,
								   1.0F, 0.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.4F,
							   3.0F, 0.0000005F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime(0.2F + GetRandom()*0.1F, 0.0F, 0.30F);
				localEntities.emplace_back(ent);
			}
		}
		
		void Client::GrenadeExplosion(spades::Vector3 origin){
			float dist = (origin - lastSceneDef.viewOrigin).GetLength();
			if(dist > 170.0F)
				return;
			grenadeVibration += 2.0F / (dist + 5.0F);
			if(grenadeVibration > 1.0F)
				grenadeVibration = 1.0F;
			
			DynamicLightParam l;
			l.origin = origin;
			l.radius = 16.0F;
			l.type = DynamicLightTypePoint;
			l.color = MakeVector3(3.0F, 1.6F, 0.5F);
			l.useLensFlare = true;
			flashDlights.push_back(l);
			
			Vector3 velBias = {0,0,0};
			if(!map->ClipBox(origin.x, origin.y, origin.z)){
				if(map->ClipBox(origin.x + 1.0F, origin.y, origin.z)){
					velBias.x -= 1.0F;
				}
				if(map->ClipBox(origin.x - 1.0F, origin.y, origin.z)){
					velBias.x += 1.0F;
				}
				if(map->ClipBox(origin.x, origin.y + 1.0F, origin.z)){
					velBias.y -= 1.0F;
				}
				if(map->ClipBox(origin.x, origin.y - 1.0F, origin.z)){
					velBias.y += 1.0F;
				}
				if(map->ClipBox(origin.x, origin.y , origin.z + 1.0F)){
					velBias.z -= 1.0F;
				}
				if(map->ClipBox(origin.x, origin.y , origin.z - 1.0F)){
					velBias.z += 1.0F;
				}
			}
			
			Vector4 color;
			color = MakeVector4( 0.6F, 0.6F, 0.6F, 1.0F);
			// rapid smoke
			for(int i = 0; i < 4; i++){
				ParticleSpriteEntity *ent =
				new SmokeSpriteEntity(this, color, 60.0F,
									  SmokeSpriteEntity::Type::Explosion);
				ent->SetTrajectory(origin,
								   (MakeVector3(GetRandom()-GetRandom(),
												GetRandom()-GetRandom(),
												GetRandom()-GetRandom())+velBias*0.5F) * 2.0F,
								   1.0F, 0.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.6F + GetRandom()*GetRandom()*0.4F,
							   2.0F, 0.2F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime(1.8F + GetRandom()*0.1F, 0.0F, 0.20F);
				localEntities.emplace_back(ent);
			}
			
			// slow smoke
			color.w = 0.25F;
			for(int i = 0; i < 8; i++){
				ParticleSpriteEntity *ent =
				new SmokeSpriteEntity(this, color, 20.0F);
				ent->SetTrajectory(origin,
								   (MakeVector3(GetRandom()-GetRandom(),
												GetRandom()-GetRandom(),
												(GetRandom()-GetRandom()) * 0.2F)) * 2.0F,
								   1.0F, 0.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(1.5F + GetRandom()*GetRandom()*0.8F,
							   0.2F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				if(cg_reduceSmoke)
					ent->SetLifeTime(1.0F + GetRandom() * 2.0F, 0.1F, 8.0F);
				else
					ent->SetLifeTime(2.0F + GetRandom() * 5.0F, 0.1F, 8.0F);
				localEntities.emplace_back(ent);
			}
			
			// fragments
			Handle<IImage> img = renderer->RegisterImage("Gfx/White.tga");
			color = MakeVector4(0.01, 0.03, 0, 1.0F);
			for(int i = 0; i < 42; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				Vector3 dir = MakeVector3(GetRandom()-GetRandom(),
										  GetRandom()-GetRandom(),
										  GetRandom()-GetRandom());
				dir += velBias * 0.5F;
				float radius = 0.1F + GetRandom()*GetRandom()*0.2F;
				ent->SetTrajectory(origin + dir * 0.2F,
								   dir * 20.0F,
								   0.1F + radius * 3.0F, 1.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(radius);
				ent->SetLifeTime(3.5F + GetRandom() * 2.0F, 0.0F, 1.0F);
				ent->SetBlockHitAction(ParticleSpriteEntity::BounceWeak);
				localEntities.emplace_back(ent);
			}
			
			// fire smoke
			color= MakeVector4(1.0F, 0.7F, 0.4F, 0.2F) * 5.0F;
			for(int i = 0; i < 4; i++){
				ParticleSpriteEntity *ent =
				new SmokeSpriteEntity(this, color, 120.0F,
									  SmokeSpriteEntity::Type::Explosion);
				ent->SetTrajectory(origin,
								   (MakeVector3(GetRandom()-GetRandom(),
												GetRandom()-GetRandom(),
												GetRandom()-GetRandom())+velBias) * 6.0F,
								   1.0F, 0.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.3F + GetRandom()*GetRandom()*0.4F,
							   3.0F, 0.1F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime(0.18F + GetRandom()*0.03F, 0.0F, 0.10F);
				//ent->SetAdditive(true);
				localEntities.emplace_back(ent);
			}
		}
		
		void Client::GrenadeExplosionUnderwater(spades::Vector3 origin){
			float dist = (origin - lastSceneDef.viewOrigin).GetLength();
			if(dist > 170.0F)
				return;
			grenadeVibration += 1.5F / (dist + 5.0F);
			if(grenadeVibration > 1.0F)
				grenadeVibration = 1.0F;
			
			Vector3 velBias = {0,0,0};
			
			Vector4 color;
			color = MakeVector4( 0.95F, 0.95F, 0.95F, 0.6F);
			// water1
			Handle<IImage> img = renderer->RegisterImage("Textures/WaterExpl.png");
			if(cg_reduceSmoke) color.w = 0.3F;
			for(int i = 0; i < 7; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				ent->SetTrajectory(origin,
								   (MakeVector3(GetRandom()-GetRandom(),
												GetRandom()-GetRandom(),
												-GetRandom()*7.0F)) * 2.5F,
								   0.3F, 0.6F);
				ent->SetRotation(0.0F);
				ent->SetRadius(1.5F + GetRandom()*GetRandom()*0.4F,
							   1.3F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime(3.0F + GetRandom()*0.3F, 0.0F, 0.60F);
				localEntities.emplace_back(ent);
			}
			
			// water2
			img = renderer->RegisterImage("Textures/Fluid.png");
			color.w = 0.9F;
			if(cg_reduceSmoke) color.w = 0.4F;
			for(int i = 0; i < 16; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				ent->SetTrajectory(origin,
								   (MakeVector3(GetRandom()-GetRandom(),
												GetRandom()-GetRandom(),
												-GetRandom()*10.0F)) * 3.5F,
								   1.0F, 1.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.9F + GetRandom()*GetRandom()*0.4F,
							   0.7F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime(3.0F + GetRandom()*0.3F, 0.7F, 0.60F);
				localEntities.emplace_back(ent);
			}
			
			// slow smoke
			color.w = 0.4F;
			if(cg_reduceSmoke) color.w = 0.2F;
			for(int i = 0; i < 8; i++){
				ParticleSpriteEntity *ent =
				new SmokeSpriteEntity(this, color, 20.0F);
				ent->SetTrajectory(origin,
								   (MakeVector3(GetRandom()-GetRandom(),
												GetRandom()-GetRandom(),
												(GetRandom()-GetRandom()) * 0.2F)) * 2.0F,
								   1.0F, 0.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(1.4F + GetRandom()*GetRandom()*0.8F,
							   0.2F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime((cg_reduceSmoke ? 3.0F : 6.0F) + GetRandom() * 5.0F, 0.1F, 8.0F);
				localEntities.emplace_back(ent);
			}
			
			// fragments
			img = renderer->RegisterImage("Gfx/White.tga");
			color = MakeVector4(1,1,1, 0.7F);
			for(int i = 0; i < 42; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				Vector3 dir = MakeVector3(GetRandom()-GetRandom(),
										  GetRandom()-GetRandom(),
										  -GetRandom() * 3.0F);
				dir += velBias * 0.5F;
				float radius = 0.1F + GetRandom()*GetRandom()*0.2F;
				ent->SetTrajectory(origin + dir * 0.2F +
								   MakeVector3(0, 0, -1.2F),
								   dir * 13.0F,
								   0.1F + radius * 3.0F, 1.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(radius);
				ent->SetLifeTime(3.5F + GetRandom() * 2.0F, 0.0F, 1.0F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Delete);
				localEntities.emplace_back(ent);
			}
			
			
			// TODO: wave?
		}
		
		
		void Client::BulletHitWaterSurface(spades::Vector3 origin){
			float dist = (origin - lastSceneDef.viewOrigin).GetLength();
			if(dist > 150.0F)
				return;
			if(!cg_waterImpact)
				return;
			
			Vector4 color;
			color = MakeVector4( 0.95F, 0.95F, 0.95F, 0.3F);
			// water1
			Handle<IImage> img = renderer->RegisterImage("Textures/WaterExpl.png");
			if(cg_reduceSmoke) color.w = 0.2F;
			for(int i = 0; i < 2; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				ent->SetTrajectory(origin,
								   (MakeVector3(GetRandom()-GetRandom(),
												GetRandom()-GetRandom(),
												-GetRandom()*7.0F)) * 1.0F,
								   0.3F, 0.6F);
				ent->SetRotation(0.0F);
				ent->SetRadius(0.6F + GetRandom()*GetRandom()*0.4F,
							   0.7F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime(3.0F + GetRandom()*0.3F, 0.1F, 0.60F);
				localEntities.emplace_back(ent);
			}
			
			// water2
			img = renderer->RegisterImage("Textures/Fluid.png");
			color.w = 0.9F;
			if(cg_reduceSmoke) color.w = 0.4F;
			for(int i = 0; i < 6; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				ent->SetTrajectory(origin,
								   (MakeVector3(GetRandom()-GetRandom(),
												GetRandom()-GetRandom(),
												-GetRandom()*10.0F)) * 2.0F,
								   1.0F, 1.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(0.6F + GetRandom()*GetRandom()*0.6F,
							   0.6F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Ignore);
				ent->SetLifeTime(3.0F + GetRandom()*0.3F, GetRandom() * 0.3F, 0.60F);
				localEntities.emplace_back(ent);
			}
			
			
			// fragments
			img = renderer->RegisterImage("Gfx/White.tga");
			color = MakeVector4(1,1,1, 0.7F);
			for(int i = 0; i < 10; i++){
				ParticleSpriteEntity *ent =
				new ParticleSpriteEntity(this, img, color);
				Vector3 dir = MakeVector3(GetRandom()-GetRandom(),
										  GetRandom()-GetRandom(),
										  -GetRandom() * 3.0F);
				float radius = 0.03F + GetRandom()*GetRandom()*0.05F;
				ent->SetTrajectory(origin + dir * 0.2F +
								   MakeVector3(0, 0, -1.2F),
								   dir * 5.0F,
								   0.1F + radius * 3.0F, 1.0F);
				ent->SetRotation(GetRandom() * (float)M_PI * 2.0F);
				ent->SetRadius(radius);
				ent->SetLifeTime(3.5F + GetRandom() * 2.0F, 0.0F, 1.0F);
				ent->SetBlockHitAction(ParticleSpriteEntity::Delete);
				localEntities.emplace_back(ent);
			}
			
			
			// TODO: wave?
		}

	}
}
