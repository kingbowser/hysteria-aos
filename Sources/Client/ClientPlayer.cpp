/*
 Copyright (c) 2013 yvt
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ClientPlayer.h"
#include <ScriptBindings/ScriptFunction.h>
#include "IImage.h"
#include "IModel.h"
#include "IRenderer.h"
#include "Client.h"
#include "World.h"
#include <Core/Settings.h>
#include "CTFGameMode.h"
#include "Weapon.h"
#include "GunCasing.h"
#include <ScriptBindings/IToolSkin.h>
#include <ScriptBindings/IViewToolSkin.h>
#include <ScriptBindings/IThirdPersonToolSkin.h>
#include <ScriptBindings/ISpadeSkin.h>
#include <ScriptBindings/IBlockSkin.h>
#include <ScriptBindings/IGrenadeSkin.h>
#include <ScriptBindings/IWeaponSkin.h>
#include "IAudioDevice.h"
#include "IAudioChunk.h"

// ------------------------------------

#include "../Hysteria/Hysteria.h"

// ------------------------------------

SPADES_SETTING(cg_ragdoll,      "");
SPADES_SETTING(cg_ejectBrass,   "");



namespace spades {
	namespace client {
		
		class SandboxedRenderer : public IRenderer {
			Handle<IRenderer> base;
			AABB3 clipBox;
			bool allowDepthHack;
			
			void OnProhibitedAction() {
			}
			
			bool CheckVisibility(const AABB3 &box) {
				if (!clipBox.Contains(box) ||
					!std::isfinite(box.min.x) || !std::isfinite(box.min.y) || !std::isfinite(box.min.z) ||
					!std::isfinite(box.max.x) || !std::isfinite(box.max.y) || !std::isfinite(box.max.z)) {
					OnProhibitedAction();
					return false;
				}
				return true;
			}
		protected:
			~SandboxedRenderer(){}
		public:
			
			SandboxedRenderer(IRenderer *base) :
			base(base) {}
			
			void SetClipBox(const AABB3 &b){ clipBox = b; }
			void SetAllowDepthHack(bool h) { allowDepthHack = h; }
			
			void Init() { OnProhibitedAction(); }
			void Shutdown() { OnProhibitedAction(); }
			
			IImage *RegisterImage(const char *filename)
			{ return base->RegisterImage(filename); }
			IModel *RegisterModel(const char *filename)
			{ return base->RegisterModel(filename); }
			
			IImage *CreateImage(Bitmap *bmp)
			{ return base->CreateImage(bmp); }
			IModel *CreateModel(VoxelModel *m)
			{ return base->CreateModel(m); }
			
			void SetGameMap(GameMap *)
			{ OnProhibitedAction(); }
			
			void SetFogDistance(float)
			{ OnProhibitedAction(); }
			void SetFogColor(Vector3)
			{ OnProhibitedAction(); }
			
			void StartScene(const SceneDefinition&)
			{ OnProhibitedAction(); }
			
			void AddLight(const client::DynamicLightParam& light) {
				Vector3 rad(light.radius, light.radius, light.radius);
				if (CheckVisibility(AABB3(light.origin - rad, light.origin + rad))) {
					base->AddLight(light);
				}
			}
			
			void RenderModel(IModel *model, const ModelRenderParam& p) {
				if (!model) {
					SPInvalidArgument("model");
					return;
				}
				if (p.depthHack && !allowDepthHack) {
					OnProhibitedAction();
					return;
				}
				auto bounds = (p.matrix * OBB3(model->GetBoundingBox())).GetBoundingAABB();
				if (CheckVisibility(bounds)) {
					base->RenderModel(model, p);
				}
			}
			void AddDebugLine(Vector3 a, Vector3 b, Vector4 color) {
				OnProhibitedAction();
			}
			
			void AddSprite(IImage *image, Vector3 center, float radius, float rotation) {
				Vector3 rad(radius * 1.5F, radius * 1.5F, radius * 1.5F);
				if (CheckVisibility(AABB3(center - rad, center + rad))) {
					base->AddSprite(image, center, radius, rotation);
				}
			}
			void AddLongSprite(IImage *image, Vector3 p1, Vector3 p2, float radius)
			{
				Vector3 rad(radius * 1.5F, radius * 1.5F, radius * 1.5F);
				AABB3 bounds1(p1 - rad, p1 + rad);
				AABB3 bounds2(p2 - rad, p2 + rad);
				bounds1 += bounds2;
				if (CheckVisibility(bounds1)) {
					base->AddLongSprite(image, p1, p2, radius);
				}
			}
			
			void EndScene() { OnProhibitedAction(); }
			
			void MultiplyScreenColor(Vector3) { OnProhibitedAction(); }
			
			/** Sets color for image drawing. Deprecated because
			 * some methods treats this as an alpha premultiplied, while
			 * others treats this as an alpha non-premultiplied.
			 * @deprecated */
			void SetColor(Vector4 col) {
				base->SetColor(col);
			}
			
			/** Sets color for image drawing. Always alpha premultiplied. */
			void SetColorAlphaPremultiplied(Vector4 col) {
				base->SetColorAlphaPremultiplied(col);
			}
			
			void DrawImage(IImage *img, const Vector2& outTopLeft)
			{
				if (allowDepthHack)
					base->DrawImage(img, outTopLeft);
				else
					OnProhibitedAction();
			}
			void DrawImage(IImage *img, const AABB2& outRect)
			{
				if (allowDepthHack)
					base->DrawImage(img, outRect);
				else
					OnProhibitedAction();
			}
			void DrawImage(IImage *img, const Vector2& outTopLeft, const AABB2& inRect)
			{
				if (allowDepthHack)
					base->DrawImage(img, outTopLeft, inRect);
				else
					OnProhibitedAction();
			}
			void DrawImage(IImage *img, const AABB2& outRect, const AABB2& inRect)
			{
				if (allowDepthHack)
					base->DrawImage(img, outRect, inRect);
				else
					OnProhibitedAction();
			}
			void DrawImage(IImage *img, const Vector2& outTopLeft, const Vector2& outTopRight, const Vector2& outBottomLeft, const AABB2& inRect)
			{
				if (allowDepthHack)
					base->DrawImage(img, outTopLeft, outTopRight, outBottomLeft, inRect);
				else
					OnProhibitedAction();
			}
			
			void DrawFlatGameMap(const AABB2& outRect, const AABB2& inRect)
			{ OnProhibitedAction(); }
			
			void FrameDone()
			{ OnProhibitedAction(); }
			
			void Flip()
			{ OnProhibitedAction(); }
			
			Bitmap *ReadBitmap()
			{ OnProhibitedAction(); return nullptr; }
			
			float ScreenWidth() { return base->ScreenWidth(); }
			float ScreenHeight() { return base->ScreenHeight(); }
		};
		
		ClientPlayer::ClientPlayer(Player *p,
								   Client *c):
		player(p), client(c){
			SPADES_MARK_FUNCTION();
			
			sprintState = 0.0F;
			aimDownState = 0.0F;
			toolRaiseState = 0.0F;
			currentTool = p->GetTool();
			localFireVibrationTime = -100.0F;
			time = 0.0F;
			viewWeaponOffset = MakeVector3(0, 0, 0);
			
			ScriptContextHandle ctx;
			IRenderer *renderer = client->GetRenderer();
			IAudioDevice *audio = client->GetAudioDevice();
			
			sandboxedRenderer.Set(new SandboxedRenderer(renderer), false);
			renderer = sandboxedRenderer;
			
			static ScriptFunction spadeFactory("ISpadeSkin@ CreateThirdPersonSpadeSkin(Renderer@, AudioDevice@)");
			spadeSkin = initScriptFactory( spadeFactory, renderer, audio );
			
			static ScriptFunction spadeViewFactory("ISpadeSkin@ CreateViewSpadeSkin(Renderer@, AudioDevice@)");
			spadeViewSkin = initScriptFactory( spadeViewFactory, renderer, audio );
			
			static ScriptFunction blockFactory("IBlockSkin@ CreateThirdPersonBlockSkin(Renderer@, AudioDevice@)");
			blockSkin = initScriptFactory( blockFactory, renderer, audio );
			
			static ScriptFunction blockViewFactory("IBlockSkin@ CreateViewBlockSkin(Renderer@, AudioDevice@)");
			blockViewSkin = initScriptFactory( blockViewFactory, renderer, audio );
			
			static ScriptFunction grenadeFactory("IGrenadeSkin@ CreateThirdPersonGrenadeSkin(Renderer@, AudioDevice@)");
			grenadeSkin = initScriptFactory( grenadeFactory, renderer, audio );
			
			static ScriptFunction grenadeViewFactory("IGrenadeSkin@ CreateViewGrenadeSkin(Renderer@, AudioDevice@)");
			grenadeViewSkin = initScriptFactory( grenadeViewFactory, renderer, audio );
			
			static ScriptFunction rifleFactory("IWeaponSkin@ CreateThirdPersonRifleSkin(Renderer@, AudioDevice@)");
			static ScriptFunction smgFactory("IWeaponSkin@ CreateThirdPersonSMGSkin(Renderer@, AudioDevice@)");
			static ScriptFunction shotgunFactory("IWeaponSkin@ CreateThirdPersonShotgunSkin(Renderer@, AudioDevice@)");
			static ScriptFunction rifleViewFactory("IWeaponSkin@ CreateViewRifleSkin(Renderer@, AudioDevice@)");
			static ScriptFunction smgViewFactory("IWeaponSkin@ CreateViewSMGSkin(Renderer@, AudioDevice@)");
			static ScriptFunction shotgunViewFactory("IWeaponSkin@ CreateViewShotgunSkin(Renderer@, AudioDevice@)");
			switch(p->GetWeapon()->GetWeaponType()){
				case RIFLE_WEAPON:
					weaponSkin = initScriptFactory( rifleFactory, renderer, audio );
					weaponViewSkin = initScriptFactory( rifleViewFactory, renderer, audio );
					break;
				case SMG_WEAPON:
					weaponSkin = initScriptFactory( smgFactory, renderer, audio );
					weaponViewSkin = initScriptFactory( smgViewFactory, renderer, audio );
					break;
				case SHOTGUN_WEAPON:
					weaponSkin = initScriptFactory( shotgunFactory, renderer, audio );
					weaponViewSkin = initScriptFactory( shotgunViewFactory, renderer, audio );
					break;
				default:
					SPAssert(false);
			}
			
		}
		ClientPlayer::~ClientPlayer() {
			spadeSkin->Release();
			blockSkin->Release();
			weaponSkin->Release();
			grenadeSkin->Release();
			
			spadeViewSkin->Release();
			blockViewSkin->Release();
			weaponViewSkin->Release();
			grenadeViewSkin->Release();
			
		}

		asIScriptObject* ClientPlayer::initScriptFactory( ScriptFunction& creator, IRenderer* renderer, IAudioDevice* audio )
		{
			ScriptContextHandle ctx = creator.Prepare();
			ctx->SetArgObject(0, reinterpret_cast<void*>(renderer));
			ctx->SetArgObject(1, reinterpret_cast<void*>(audio));
			ctx.ExecuteChecked();
			asIScriptObject* result = reinterpret_cast<asIScriptObject *>(ctx->GetReturnObject());
			result->AddRef();
			return result;
		}

		void ClientPlayer::Invalidate() {
			player = NULL;
		}
		
		bool ClientPlayer::IsChangingTool() {
			return currentTool != player->GetTool() ||
			toolRaiseState < 0.999F;
		}
		
		float ClientPlayer::GetLocalFireVibration() {
			float localFireVibration = 0.0F;
			localFireVibration = time - localFireVibrationTime;
			localFireVibration = 1.0F - localFireVibration / 0.1F;
			if(localFireVibration < 0.0F)
				localFireVibration = 0.0F;
			return localFireVibration;
		}
		
		void ClientPlayer::Update(float dt) {
			time += dt;
			
			PlayerInput actualInput = player->GetInput();
			WeaponInput actualWeapInput = player->GetWeaponInput();
			Vector3 vel = player->GetVelocty();
			vel.z = 0.0F;
			if(actualInput.sprint && player->IsAlive() &&
			   vel.GetLength() > 0.1F){
				sprintState += dt * 4.0F;
				if(sprintState > 1.0F)
					sprintState = 1.0F;
			}else{
				sprintState -= dt * 3.0F;
				if(sprintState < 0.0F)
					sprintState = 0.0F;
			}
			
			if(actualWeapInput.secondary && player->IsToolWeapon() &&
			   player->IsAlive()){
				aimDownState += dt * 6.0F;
				if(aimDownState > 1.0F)
					aimDownState = 1.0F;
			}else{
				aimDownState -= dt * 3.0F;
				if(aimDownState < 0.0F)
					aimDownState = 0.0F;
			}
			
			if(currentTool == player->GetTool()) {
				toolRaiseState += dt * 4.0F;
				if(toolRaiseState > 1.0F)
					toolRaiseState = 1.0F;
				if(toolRaiseState < 0.0F)
					toolRaiseState = 0.0F;
			}else{
				toolRaiseState -= dt * 4.0F;
				if(toolRaiseState < 0.0F){
					toolRaiseState = 0.0F;
					currentTool = player->GetTool();
					
					// play tool change sound
					if(player->IsLocalPlayer()) {
						auto *audioDevice = client->GetAudioDevice();
						Handle<IAudioChunk> c;
						switch(player->GetTool()) {
							case Player::ToolSpade:
								c = audioDevice->RegisterSound("Sounds/Weapons/Spade/RaiseLocal.wav");
								break;
							case Player::ToolBlock:
								c = audioDevice->RegisterSound("Sounds/Weapons/Block/RaiseLocal.wav");
								break;
							case Player::ToolWeapon:
								switch(player->GetWeapon()->GetWeaponType()){
									case RIFLE_WEAPON:
										c = audioDevice->RegisterSound("Sounds/Weapons/Rifle/RaiseLocal.wav");
										break;
									case SMG_WEAPON:
										c = audioDevice->RegisterSound("Sounds/Weapons/SMG/RaiseLocal.wav");
										break;
									case SHOTGUN_WEAPON:
										c = audioDevice->RegisterSound("Sounds/Weapons/Shotgun/RaiseLocal.wav");
										break;
								}
								
								break;
							case Player::ToolGrenade:
								c = audioDevice->RegisterSound("Sounds/Weapons/Grenade/RaiseLocal.wav");
								break;
						}
						audioDevice->PlayLocal(c, MakeVector3(0.4F, -0.3F, 0.5F),
											   AudioParam());
					}
				}else if(toolRaiseState > 1.0F){
					toolRaiseState = 1.0F;
				}
			}
			
			{
				float scale = dt;
				Vector3 vel = player->GetVelocty();
				Vector3 front = player->GetFront();
				Vector3 right = player->GetRight();
				Vector3 up = player->GetUp();
				viewWeaponOffset.x += Vector3::Dot(vel, right) * scale;
				viewWeaponOffset.y -= Vector3::Dot(vel, front) * scale;
				viewWeaponOffset.z += Vector3::Dot(vel, up) * scale;
				if(dt > 0.0F)
					viewWeaponOffset *= powf(0.02F, dt);
				
				if(currentTool == Player::ToolWeapon &&
				   player->GetWeaponInput().secondary) {
					
					if(dt > 0.0F)
						viewWeaponOffset *= powf(0.01F, dt);
					
					const float limitX = 0.003F;
					const float limitY = 0.003F;
					if(viewWeaponOffset.x < -limitX)
						viewWeaponOffset.x = Mix(viewWeaponOffset.x, -limitX, 0.5F);
					if(viewWeaponOffset.x > limitX)
						viewWeaponOffset.x = Mix(viewWeaponOffset.x, limitX, 0.5F);
					if(viewWeaponOffset.z < 0.0F)
						viewWeaponOffset.z = Mix(viewWeaponOffset.z, 0.0F, 0.5F);
					if(viewWeaponOffset.z > limitY)
						viewWeaponOffset.z = Mix(viewWeaponOffset.z, limitY, 0.5F);
				}
			}
			
			// FIXME: should do for non-active skins?
			asIScriptObject *skin;
			if(ShouldRenderInThirdPersonView()){
				if(currentTool == Player::ToolSpade) {
					skin = spadeSkin;
				}else if(currentTool == Player::ToolBlock) {
					skin = blockSkin;
				}else if(currentTool == Player::ToolGrenade) {
					skin = grenadeSkin;
				}else if(currentTool == Player::ToolWeapon) {
					skin = weaponSkin;
				}else{
					SPInvalidEnum("currentTool", currentTool);
				}
			}else{
				if(currentTool == Player::ToolSpade) {
					skin = spadeViewSkin;
				}else if(currentTool == Player::ToolBlock) {
					skin = blockViewSkin;
				}else if(currentTool == Player::ToolGrenade) {
					skin = grenadeViewSkin;
				}else if(currentTool == Player::ToolWeapon) {
					skin = weaponViewSkin;
				}else{
					SPInvalidEnum("currentTool", currentTool);
				}
			}
			{
				ScriptIToolSkin interface(skin);
				interface.Update(dt);
			}
		}
		
		Matrix4 ClientPlayer::GetEyeMatrix() {
			Player *p = player;
			return Matrix4::FromAxis(-p->GetRight(), p->GetFront(), -p->GetUp(), p->GetEye());
		}
		
		void ClientPlayer::SetSkinParameterForTool(Player::ToolType type,
												   asIScriptObject *skin) {
			Player *p = player;
			if(currentTool == Player::ToolSpade) {
				
				ScriptISpadeSkin interface(skin);
				WeaponInput inp = p->GetWeaponInput();
				if(p->GetTool() != Player::ToolSpade){
					interface.SetActionType(SpadeActionTypeIdle);
					interface.SetActionProgress(0.0F);
				}else if(inp.primary) {
					interface.SetActionType(SpadeActionTypeBash);
					interface.SetActionProgress(p->GetSpadeAnimationProgress());
				}else if(inp.secondary) {
					interface.SetActionType(p->IsFirstDig() ?
											SpadeActionTypeDigStart:
											SpadeActionTypeDig);
					interface.SetActionProgress(p->GetDigAnimationProgress());
				}else{
					interface.SetActionType(SpadeActionTypeIdle);
					interface.SetActionProgress(0.0F);
				}
			}else if(currentTool == Player::ToolBlock) {
				
				// TODO: smooth ready state
				ScriptIBlockSkin interface(skin);
				if(p->GetTool() != Player::ToolBlock){
					// FIXME: use block's IsReadyToUseTool
					// for smoother transition
					interface.SetReadyState(0.0F);
				}else if(p->IsReadyToUseTool()) {
					interface.SetReadyState(1.0F);
				}else{
					interface.SetReadyState(0.0F);
				}
				
				interface.SetBlockColor(MakeVector3(p->GetBlockColor()) / 255.0F);
			}else if(currentTool == Player::ToolGrenade) {
				
				ScriptIGrenadeSkin interface(skin);
				interface.SetReadyState(1.0F - p->GetTimeToNextGrenade() / 0.5F);
				
				WeaponInput inp = p->GetWeaponInput();
				if(inp.primary) {
					interface.SetCookTime(p->GetGrenadeCookTime());
				}else{
					interface.SetCookTime(0.0F);
				}
			}else if(currentTool == Player::ToolWeapon) {
				
				Weapon *w = p->GetWeapon();
				ScriptIWeaponSkin interface(skin);
				interface.SetReadyState(1.0F - w->TimeToNextFire() / w->GetDelay());
				interface.SetAimDownSightState(aimDownState);
				interface.SetAmmo(w->GetAmmo());
				interface.SetClipSize(w->GetClipSize());
				interface.SetReloading(w->IsReloading());
				interface.SetReloadProgress(w->GetReloadProgress());
			}else{
				SPInvalidEnum("currentTool", currentTool);
			}
		}
		
		void ClientPlayer::SetCommonSkinParameter(asIScriptObject *skin){
			asIScriptObject *curSkin;
			if(ShouldRenderInThirdPersonView()){
				if(currentTool == Player::ToolSpade) {
					curSkin = spadeSkin;
				}else if(currentTool == Player::ToolBlock) {
					curSkin = blockSkin;
				}else if(currentTool == Player::ToolGrenade) {
					curSkin = grenadeSkin;
				}else if(currentTool == Player::ToolWeapon) {
					curSkin = weaponSkin;
				}else{
					SPInvalidEnum("currentTool", currentTool);
				}
			}else{
				if(currentTool == Player::ToolSpade) {
					curSkin = spadeViewSkin;
				}else if(currentTool == Player::ToolBlock) {
					curSkin = blockViewSkin;
				}else if(currentTool == Player::ToolGrenade) {
					curSkin = grenadeViewSkin;
				}else if(currentTool == Player::ToolWeapon) {
					curSkin = weaponViewSkin;
				}else{
					SPInvalidEnum("currentTool", currentTool);
				}
			}
			
			float sprint = SmoothStep(sprintState);
			float putdown = 1.0F - toolRaiseState;
			putdown *= putdown;
			putdown = std::min(1.0F, putdown * 1.5F);
			{
				ScriptIToolSkin interface(skin);
				interface.SetRaiseState((skin == curSkin)?
										(1.0F - putdown):
										0.0F);
				interface.SetSprintState(sprint);
				interface.SetMuted(client->IsMuted());
			}
		}
	
        /*
         * This is called when adding the client player to the renderer
         */    
		void ClientPlayer::AddToSceneFirstPersonView() {
			Player *p           = player;
			IRenderer *renderer = client->GetRenderer();
			World *world        = client->GetWorld();
			Matrix4 eyeMatrix   = GetEyeMatrix();
			
            // Configure the clipping region for the local player view in case of overdraw
            {
                Vector3 const outset(20.0F, 20.0F, 20.0F);

                sandboxedRenderer->SetClipBox(AABB3(eyeMatrix.GetOrigin() - outset,
                                                    eyeMatrix.GetOrigin() + outset));
                sandboxedRenderer->SetAllowDepthHack(true);
            }
		
            // Draw flashlight luma    
			if (client->flashlightOn) {
				float brightness;
				brightness = client->time - client->flashlightOnTime;
				brightness = 1.0F - expf(-brightness * 5.0F);
				
				// add flash light
				DynamicLightParam light;
				light.origin = (eyeMatrix * MakeVector3(0, -0.05F, -0.1F)).GetXYZ();
				light.color = MakeVector3(1, 0.7F, 0.5F) * 1.5F * brightness;
				light.radius = 40.0F;
				light.type = DynamicLightTypeSpotlight;
				light.spotAngle = 30.0F * M_PI / 180.0F;
				light.spotAxis[0] = p->GetRight();
				light.spotAxis[1] = p->GetUp();
				light.spotAxis[2] = p->GetFront();
				light.image = renderer->RegisterImage("Gfx/Spotlight.tga");
				renderer->AddLight(light);
				
				light.color *= 0.3F;
				light.radius = 10.0F;
				light.type = DynamicLightTypePoint;
				light.image = NULL;
				renderer->AddLight(light);
				
				// add glare
				renderer->SetColorAlphaPremultiplied(MakeVector4(1, 0.7F, 0.5F, 0) * brightness * 0.3F);
				renderer->AddSprite(renderer->RegisterImage("Gfx/Glare.tga"), (eyeMatrix * MakeVector3(0, 0.3F, -0.3F)).GetXYZ(), 0.8F, 0.0F);
			}
			
			// view weapon
			Vector3 viewWeaponOffset = this->viewWeaponOffset;
			
			// bobbing
			unless (Hysteria::NoBobbing) {
				float sp = 1.0F - aimDownState;
				sp *= 0.3F;
				sp *= std::min(1.0F, p->GetVelocty().GetLength() * 5.0F);
				viewWeaponOffset.x += sinf(p->GetWalkAnimationProgress() * M_PI * 2.0F) * 0.01F * sp;
				float vl = cosf(p->GetWalkAnimationProgress() * M_PI * 2.0F);
				vl *= vl;
				viewWeaponOffset.z += vl * 0.012F * sp;
			}
			
			// slow pulse
			unless (Hysteria::NoBreathing) {
				float sp = 1.0F - aimDownState;
				float vl = sinf(world->GetTime() * 1.0F);
				
				viewWeaponOffset.x += vl * 0.001F * sp;
				viewWeaponOffset.y += vl * 0.0007F * sp;
				viewWeaponOffset.z += vl * 0.003F * sp;
			}
			
			Vector3 leftHand;
            Vector3 rightHand;

            { // Initialize rightHand and leftHand, along with setting up some other view parameters
                asIScriptObject *skin;
               
                switch (currentTool) {
                    case Player::ToolSpade:
                        skin = spadeViewSkin;
                        break;
                    case Player::ToolBlock:
                        skin = blockViewSkin;
                        break;
                    case Player::ToolGrenade:
                        skin = grenadeViewSkin;
                        break;
                    case Player::ToolWeapon:
                        skin = weaponViewSkin;
                        break;
                    default:
                        SPInvalidEnum("currentTool", currentTool);
                        break;
                }

                SetSkinParameterForTool(currentTool, skin);
                SetCommonSkinParameter(skin);
                
                // Run FPV Skin Scripts
                
                {
                    ScriptIViewToolSkin interface(skin);
                    interface.SetEyeMatrix(GetEyeMatrix());
                    interface.SetSwing(viewWeaponOffset);
                }

                {
                    ScriptIToolSkin interface(skin);
                    interface.AddToScene();
                }

                {
                    ScriptIViewToolSkin interface(skin);
                    leftHand  = interface.GetLeftHandPosition();
                    rightHand = interface.GetRightHandPosition();
                }
            }
			
			// view hands
			if (leftHand.GetPoweredLength() > 0.001F && rightHand.GetPoweredLength() > 0.001F) {
                static float const armlen = 0.5F;
					
				ModelRenderParam param;
                {
                    param.depthHack   = true;
                    param.customColor = FloatingPointRGB(p->GetColor());
                }
				
				Vector3 const shoulders[] = {
                    Vector3( 0.4F, 0.0F, 0.25F),
					Vector3(-0.4F, 0.0F, 0.25F)
                };

				Vector3 const hands[] = {
                    leftHand, 
                    rightHand
                };

				Vector3 const benddirs[] = {
                    Vector3( 0.5F, 0.2F, 0.0F),
					Vector3(-0.5F, 0.2F, 0.0F)
                };

				repeat (2, i) {
					Vector3 const shoulder = shoulders[i];
					Vector3 const hand     = hands[i];
					Vector3 const benddir  = benddirs[i];
					Vector3 bend           = Vector3::Cross(benddir, hand-shoulder).Normalize();
					
					if (bend.z < 0.0F) {
                        bend.z = -bend.z;
                    }
					
					Vector3 const elbow = ({
                        float const handToShoulder = (hand - shoulder).GetPoweredLength();
                        float const bendlen        = sqrtf(std::max(armlen*armlen - handToShoulder * 0.25F, 0.0F));
                        ((hand + shoulder) * 0.5F) + (bend * bendlen);
                    });
					
					{
                        IModel* arm = renderer->RegisterModel("Models/Player/Arm.kv6");
                        Vector3 axises[3];
						axises[2] = (hand - elbow).Normalize();
						axises[0] = MakeVector3(0, 0, 1);
						axises[1] = Vector3::Cross(axises[2], axises[0]).Normalize();
						axises[0] = Vector3::Cross(axises[1], axises[2]).Normalize();
						
						Matrix4 mat = Matrix4::Scale(0.05F);
						mat = Matrix4::FromAxis(axises[0], axises[1], axises[2], elbow) * mat;
						mat = eyeMatrix * mat;
						
						param.matrix = mat;
						renderer->RenderModel(arm, param);
					}
					
					{
                        IModel* upperArm = renderer->RegisterModel("Models/Player/UpperArm.kv6");
						Vector3 axises[3];
						axises[2] = (elbow - shoulder).Normalize();
						axises[0] = MakeVector3(0, 0, 1);
						axises[1] = Vector3::Cross(axises[2], axises[0]).Normalize();
						axises[0] = Vector3::Cross(axises[1], axises[2]).Normalize();
						
						Matrix4 mat = Matrix4::Scale(0.05F);
						mat = Matrix4::FromAxis(axises[0], axises[1], axises[2], shoulder) * mat;
						mat = eyeMatrix * mat;
						
						param.matrix = mat;
						renderer->RenderModel(upperArm, param);
					}
				}
				
			}
			
			// --- local view ends
		
		}
		
        /*
         * This adds a the player instance to the client's map as if it were being looked at from
         * a third person (the client player)
         */
		void ClientPlayer::AddToSceneThirdPersonView() {
			Player *p           = player;
			IRenderer *renderer = client->GetRenderer();
			World *world        = client->GetWorld();
		
            // XXX return point    
			unless (p->IsAlive()) {
				unless (cg_ragdoll) {
					ModelRenderParam param;
                    {
                        param.matrix      = Matrix4::Translate(p->GetOrigin()+ MakeVector3(0,0,1));
                        param.matrix      = param.matrix * Matrix4::Scale(0.1F);
                        param.customColor = FloatingPointRGB(p->GetColor());
                    }

					renderer->RenderModel(renderer->RegisterModel("Models/Player/Dead.kv6"), param);
				}

				return;
			}
		
            // Set clipping region to prevent overdraw
            {    
                Vector3 const outset(2.0F, 2.0F, 4.0F);
                auto const origin = p->GetOrigin();

                sandboxedRenderer->SetClipBox(AABB3(origin - outset, origin + outset));
                sandboxedRenderer->SetAllowDepthHack(false);
            }
			
			float pitchBias;
			asIScriptObject *skin;

			// ready for tool rendering
            {
                switch (currentTool) {
                    case Player::ToolSpade:
                        skin = spadeSkin;
                        break;
                    case Player::ToolBlock:
                        skin = blockSkin;
                        break;
                    case Player::ToolGrenade:
                        skin = grenadeSkin;
                        break;
                    case Player::ToolWeapon:
                        skin = weaponSkin;
                        break;
                    default:
                        SPInvalidEnum("currentTool", currentTool);
                }
                
                SetSkinParameterForTool(currentTool, skin);
                
                SetCommonSkinParameter(skin);
                
                {
                    ScriptIThirdPersonToolSkin interface(skin);
                    pitchBias = interface.GetPitchBias();
                }
            }
		
            /**
             * This field is the village whore (literally -- it keeps getting its contents fucked with).
             *
             * TODO please refactor this away
             */
			ModelRenderParam param;
            {
                param.customColor = FloatingPointRGB(p->GetColor());
            }

			Vector3 const front  = p->GetFront();
			float const yaw      = atan2(front.y, front.x) + M_PI * 0.5F;
			float const pitch    = -atan2(front.z, sqrt(front.x * front.x + front.y * front.y));
			
			// lower axis
            Matrix4 const lower = ({
                    Matrix4::Translate(p->GetOrigin())
                        * Matrix4::Rotate(Vector3(0, 0, 1), yaw);
            });

			Matrix4 const scaler = ({
                    Matrix4::Scale(0.1F)
                        * Matrix4::Scale(-1, -1, 1);
            });
			
            // Render  lower body -- e.g. parts dependent upon crouch state
            {
                PlayerInput const inp  = p->GetInput();
                float const translateY = inp.crouch ? 0.2F : 0.0F;

                float const legAngle   = sinf(p->GetWalkAnimationProgress() * M_PI * 2.0F) * 0.6F;
                float const velocityX  = Vector3::Dot(p->GetVelocty(), p->GetFront2D()) * 4.0F;
                float const velocityY  = Vector3::Dot(p->GetVelocty(), p->GetRight()) * 3.0F;

                /*
                 * Player armature translation matrices
                 * These are used to shift each individual component of the player model
                 * in to the correct position, depending on whether or not the player is
                 * crouched
                 */

                Matrix4 const leg1 = ({ 
                        float const rotateX = legAngle * velocityX;
                        float const rotateY = legAngle * velocityY;

                        lower 
                            * Matrix4::Translate(-0.25F, translateY, -0.1F)
                            * Matrix4::Rotate(Vector3(1, 0, 0), rotateX)
                            * Matrix4::Rotate(Vector3(0, 1, 0), rotateY);
                });

                Matrix4 const leg2 = ({
                        float const rotateX = -legAngle * velocityX;
                        float const rotateY = -legAngle * velocityY;

                        lower 
                            * Matrix4::Translate(0.25F,  translateY, -0.1F)
                            * Matrix4::Rotate(Vector3(1, 0, 0),  rotateX)
                            * Matrix4::Rotate(Vector3(0, 1, 0),  rotateY);
                });

                Matrix4 const torso = ({
                        // Account for leg height depending upon crouch state
                        float const translateZ = inp.crouch ? -0.55F : -1.0F;

                        lower
                            * Matrix4::Translate(0.0F, 0.0F, translateZ);
                });

                Matrix4 const head = ({
                        torso
                            * Matrix4::Translate(0.0F, 0.0F, 0.0F)
                            * Matrix4::Rotate(Vector3(1, 0, 0), pitch);
                });

                Matrix4 const arms = ({
                        // Shift arms forward when not crouched
                        float const translateZ = inp.crouch ? 0.0F : 0.1F;
                        float const armPitch   = ({ 
                                float _p = pitch;

                                if (inp.sprint) {
                                    _p -= 0.5F;
                                }

                                _p += pitchBias;

                                if (_p < 0.0F) {
                                    _p = std::max(_p, -float(M_PI) * 0.5F) * 0.9F;
                                }

                                _p;
                        }); 

                        torso
                            * Matrix4::Translate(0.0F, 0.0F, translateZ)
                            * Matrix4::Rotate(Vector3(1, 0, 0), armPitch);
                });

                /*
                 * Rendering Logic
                 *
                 * The following logic uses the translation matrices
                 */

                // Legs and Torso
                {
                    IModel* legModel;
                    IModel* torsoModel;
                    {
                        if (inp.crouch) {
                            legModel   = renderer->RegisterModel("Models/Player/LegCrouch.kv6");
                            torsoModel = renderer->RegisterModel("Models/Player/TorsoCrouch.kv6");
                        } else {
                            legModel   = renderer->RegisterModel("Models/Player/Leg.kv6");
                            torsoModel = renderer->RegisterModel("Models/Player/Torso.kv6");
                        }
                    }

                    {
                        param.matrix = leg1 * scaler;
                        renderer->RenderModel(legModel, param);

                        param.matrix = leg2 * scaler;
                        renderer->RenderModel(legModel, param);
                    }

                    {
                        param.matrix = torso * scaler;
                        renderer->RenderModel(torsoModel, param);
                    }
                }

                // Arms
                {
                    param.matrix = arms * scaler;
                    renderer->RenderModel(renderer->RegisterModel("Models/Player/Arms.kv6"), param);
                }               

                // Head
                {
                    param.matrix = head * scaler;
                    renderer->RenderModel(renderer->RegisterModel("Models/Player/Head.kv6"), param);
                }

                // Tool
                {
                    {
                        ScriptIThirdPersonToolSkin interface(skin);
                        interface.SetOriginMatrix(arms);
                    }

                    {
                        ScriptIToolSkin interface(skin);
                        interface.AddToScene();
                    }
                }

                // If we're in a CTF server, draw the intel briefcase (if the player has it)
                {
                    using Team = CTFGameMode::Team;

                    IGameMode* gameMode = world->GetMode();

                    if (gameMode && gameMode->ModeType() == IGameMode::m_CTF) {
                        CTFGameMode* ctfMode = static_cast<CTFGameMode*>(world->GetMode());

                        int const teamId = p->GetTeamId();

                        if (teamId < 3) {
                            Team& team = ctfMode->GetTeam(teamId);
                            
                            if (team.hasIntel && team.carrier == p->GetId()) {
                                param.customColor = FloatingPointRGB(world->GetTeam(1 - teamId).color);
                                
                                Matrix4 const briefcase = ({
                                        torso 
                                            * Matrix4::Translate(0.0F, 0.6F, 0.5F);
                                });        

                                // Actually render the briefcase
                                param.matrix = briefcase * scaler;
                                renderer->RenderModel(renderer->RegisterModel("Models/MapObjects/Intel.kv6"), param);
                            }
                        }
                    }
                }
            }
			
			// third person player rendering, done

		}
		
		void ClientPlayer::AddToScene() {
			SPADES_MARK_FUNCTION();
			
			Player *p = player;
			IRenderer *renderer = client->GetRenderer();
			const SceneDefinition& lastSceneDef = client->GetLastSceneDef();
			
			if(p->GetTeamId() >= 2){
				// spectator, or dummy player
				return;
			}

            // Draw hitboxes -- ghetto ESP
           
            if (Hysteria::RenderHitboxes) {

                if (p != client->GetWorld()->GetLocalPlayer() && p->IsAlive()) {
                    Vector4 teamColor; 
                    {
                        teamColor = FloatingPointRGBA(client->GetWorld()->GetTeam(p->GetTeamId()).color);
                        teamColor.w = float(Hysteria::HitboxAlpha);
                    }
                    
                    {
                        Player::HitBoxes hb = p->GetHitBoxes();

                        client->AddDebugObjectToScene(hb.head,      teamColor);
                        client->AddDebugObjectToScene(hb.torso,     teamColor);
                        client->AddDebugObjectToScene(hb.limbs[0],  teamColor);
                        client->AddDebugObjectToScene(hb.limbs[1],  teamColor);
                        client->AddDebugObjectToScene(hb.limbs[2],  teamColor);
                    }
                }

            }

            unless (Hysteria::MapCullDisable) {
                float distancePowered = (p->GetOrigin() - lastSceneDef.viewOrigin).GetPoweredLength();
                if (distancePowered > powf(float(Hysteria::MapCullDist), 2)) {
                    return;
                }
            }
			
			if(!ShouldRenderInThirdPersonView()){
				AddToSceneFirstPersonView();
			} else {
				AddToSceneThirdPersonView();
			}
		}
		
		void ClientPlayer::Draw2D() {
			if(!ShouldRenderInThirdPersonView()) {
				asIScriptObject *skin;
				
				if(currentTool == Player::ToolSpade) {
					skin = spadeViewSkin;
				}else if(currentTool == Player::ToolBlock) {
					skin = blockViewSkin;
				}else if(currentTool == Player::ToolGrenade) {
					skin = grenadeViewSkin;
				}else if(currentTool == Player::ToolWeapon) {
					skin = weaponViewSkin;
				}else{
					SPInvalidEnum("currentTool", currentTool);
				}
				
				SetSkinParameterForTool(currentTool, skin);
				
				SetCommonSkinParameter(skin);
				
				// common process
				{
					ScriptIViewToolSkin interface(skin);
					interface.SetEyeMatrix(GetEyeMatrix());
					interface.SetSwing(viewWeaponOffset);
					interface.Draw2D();
				}
			}
		}
		
		bool ClientPlayer::ShouldRenderInThirdPersonView() {
			if(player != player->GetWorld()->GetLocalPlayer())
				return true;
			return client->ShouldRenderInThirdPersonView();
		}
		
		void ClientPlayer::FiredWeapon() {
			World *world = player->GetWorld();
			Vector3 muzzle;
			const SceneDefinition& lastSceneDef = client->GetLastSceneDef();
			IRenderer *renderer = client->GetRenderer();
			IAudioDevice *audioDevice = client->GetAudioDevice();
			Player *p = player;
			
			// make dlight
			{
				Vector3 vec;
				Matrix4 eyeMatrix = GetEyeMatrix();
				Matrix4 mat;
				mat = Matrix4::Translate(-0.13F,
										 0.5F,
										 0.2F);
				mat = eyeMatrix * mat;
				
				vec = (mat * MakeVector3(0, 1, 0)).GetXYZ();
				muzzle = vec;
				client->MuzzleFire(vec, player->GetFront(), player == world->GetLocalPlayer());
			}
			
			if(cg_ejectBrass){
				float dist = (player->GetOrigin() - lastSceneDef.viewOrigin).GetPoweredLength();
				if(dist < 130.0F * 130.0F) {
					IModel *model = NULL;
					Handle<IAudioChunk> snd = NULL;
					Handle<IAudioChunk> snd2 = NULL;
					switch(player->GetWeapon()->GetWeaponType()){
						case RIFLE_WEAPON:
							model = renderer->RegisterModel("Models/Weapons/Rifle/Casing.kv6");
							snd = (rand()&0x1000)?
							audioDevice->RegisterSound("Sounds/Weapons/Rifle/ShellDrop1.wav"):
							audioDevice->RegisterSound("Sounds/Weapons/Rifle/ShellDrop2.wav");
							snd2 =
							audioDevice->RegisterSound("Sounds/Weapons/Rifle/ShellWater.wav");
							break;
						case SHOTGUN_WEAPON:
							// FIXME: don't want to show shotgun't casing
							// because it isn't ejected when firing
							model = renderer->RegisterModel("Models/Weapons/Shotgun/Casing.kv6");
							break;
						case SMG_WEAPON:
							model = renderer->RegisterModel("Models/Weapons/SMG/Casing.kv6");
							snd = (rand()&0x1000)?
							audioDevice->RegisterSound("Sounds/Weapons/SMG/ShellDrop1.wav"):
							audioDevice->RegisterSound("Sounds/Weapons/SMG/ShellDrop2.wav");
							snd2 =
							audioDevice->RegisterSound("Sounds/Weapons/SMG/ShellWater.wav");
							break;
					}
					if(model){
						Vector3 origin;
						origin = muzzle - p->GetFront() * 0.5F;
						
						Vector3 vel;
						vel = p->GetFront() * 0.5F + p->GetRight() +
						p->GetUp() * 0.2F;
						switch(p->GetWeapon()->GetWeaponType()){
							case SMG_WEAPON:
								vel -= p->GetFront() * 0.7F;
								break;
							case SHOTGUN_WEAPON:
								vel *= 0.5F;
								break;
							default:
								break;
						}
						
						ILocalEntity *ent;
						ent = new GunCasing(client, model, snd, snd2,
											origin, p->GetFront(),
											vel);
						client->AddLocalEntity(ent);
						
					}
				}
			}
			
			asIScriptObject *skin;
			// FIXME: what if current tool isn't weapon?
			if(ShouldRenderInThirdPersonView()){
				skin = weaponSkin;
			}else{
				skin = weaponViewSkin;
			}
			
			{
				ScriptIWeaponSkin interface(skin);
				interface.WeaponFired();
			}
	
		}
		
		void ClientPlayer::ReloadingWeapon() {
			asIScriptObject *skin;
			// FIXME: what if current tool isn't weapon?
			if(ShouldRenderInThirdPersonView()){
				skin = weaponSkin;
			}else{
				skin = weaponViewSkin;
			}
			
			{
				ScriptIWeaponSkin interface(skin);
				interface.ReloadingWeapon();
			}
		}
		
		void ClientPlayer::ReloadedWeapon() {
			asIScriptObject *skin;
			// FIXME: what if current tool isn't weapon?
			if(ShouldRenderInThirdPersonView()){
				skin = weaponSkin;
			}else{
				skin = weaponViewSkin;
			}

			{
				ScriptIWeaponSkin interface(skin);
				interface.ReloadedWeapon();
			}
		}
	}
}
