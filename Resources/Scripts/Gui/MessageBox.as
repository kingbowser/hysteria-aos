/*
 Copyright (c) 2013 yvt
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */

namespace spades {
	class AlertScreen: spades::ui::UIElement {
		
		float contentsTop, contentsHeight;
		
		spades::ui::EventHandler@ Closed;
		spades::ui::UIElement@ owner;
		
		AlertScreen(spades::ui::UIElement@ owner, string text, float height = 200.0F) {
			super(owner.Manager);
			@this.owner = owner;
			@Font = Manager.RootElement.Font;
			this.Bounds = owner.Bounds;
			
			float contentsWidth = 700.0F;
			float contentsLeft = (Manager.Renderer.ScreenWidth - contentsWidth) * 0.5F;
			contentsHeight = height;
			contentsTop = (Manager.Renderer.ScreenHeight - contentsHeight) * 0.5F;
			{
				spades::ui::Label label(Manager);
				label.BackgroundColor = Vector4(0, 0, 0, 0.4F);
				label.Bounds = Bounds;
				AddChild(label);
			}
			{
				spades::ui::Label label(Manager);
				label.BackgroundColor = Vector4(0, 0, 0, 0.8F);
				label.Bounds = AABB2(0.0F, contentsTop - 13.0F, Size.x, contentsHeight + 27.0F);
				AddChild(label);
			}
			{
				spades::ui::Button button(Manager);
				button.Caption = _Tr("MessageBox", "OK");
				button.Bounds = AABB2(
					contentsLeft + contentsWidth - 150.0F, 
					contentsTop + contentsHeight - 30.0F
					, 150.0F, 30.0F);
				button.Activated = spades::ui::EventHandler(this.OnOkPressed);
				AddChild(button);
			}
			{
				spades::ui::TextViewer viewer(Manager);
				AddChild(viewer);
				viewer.Bounds = AABB2(contentsLeft, contentsTop, contentsWidth, contentsHeight - 40.0F);
				viewer.Text = text;
			}
		}
		
		private void OnClosed() {
			if(Closed !is null) {
				Closed(this);
			}
		}
		
		void Close() {
			owner.Enable = true;
			owner.Parent.RemoveChild(this);
		}
		
		void Run() {
			owner.Enable = false;
			owner.Parent.AddChild(this);
		}
		
		private void OnOkPressed(spades::ui::UIElement@ sender) {
			Close();
		}
		
		void HotKey(string key) {
			if(IsEnabled and (key == "Enter" or key == "Escape")) {
				Close();
			} else {
				UIElement::HotKey(key);
			}
		}
		
		void Render() {
			Vector2 pos = ScreenPosition;
			Vector2 size = Size;
			Renderer@ r = Manager.Renderer;
			Image@ img = r.RegisterImage("Gfx/White.tga");
			
			r.ColorNP = Vector4(1, 1, 1, 0.08F);
			r.DrawImage(img, 
				AABB2(pos.x, pos.y + contentsTop - 15.0F, size.x, 1.0F));
			r.DrawImage(img, 
				AABB2(pos.x, pos.y + contentsTop + contentsHeight + 15.0F, size.x, 1.0F));
			r.ColorNP = Vector4(1, 1, 1, 0.2F);
			r.DrawImage(img, 
				AABB2(pos.x, pos.y + contentsTop - 14.0F, size.x, 1.0F));
			r.DrawImage(img, 
				AABB2(pos.x, pos.y + contentsTop + contentsHeight + 14.0F, size.x, 1.0F));
				
			UIElement::Render();
		}
		
	}
}
