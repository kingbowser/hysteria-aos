/*
 Copyright (c) 2013 yvt
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */

namespace spades {

	class PreferenceViewOptions {
		bool GameActive = false;
	}
	
	class PreferenceView: spades::ui::UIElement {
		private spades::ui::UIElement@ owner;
		
		private PreferenceTab@[] tabs;
		float ContentsLeft, ContentsWidth;
		float ContentsTop, ContentsHeight;
		
		int SelectedTabIndex = 0;
		
		spades::ui::EventHandler@ Closed;
		
		PreferenceView(spades::ui::UIElement@ owner, PreferenceViewOptions@ options) {
			super(owner.Manager);
			@this.owner = owner;
			this.Bounds = owner.Bounds;
			ContentsWidth = 800.0F;
			ContentsLeft = (Manager.Renderer.ScreenWidth - ContentsWidth) * 0.5F;
			ContentsHeight = 550.0F;
			ContentsTop = (Manager.Renderer.ScreenHeight - ContentsHeight) * 0.5F;
			
			{
				spades::ui::Label label(Manager);
				label.BackgroundColor = Vector4(0, 0, 0, 0.4F);
				label.Bounds = Bounds;
				AddChild(label);
			}
			{
				spades::ui::Label label(Manager);
				label.BackgroundColor = Vector4(0, 0, 0, 0.8F);
				label.Bounds = AABB2(0.0F, ContentsTop - 13.0F, Size.x, ContentsHeight + 27.0F);
				AddChild(label);
			}
			
			AddTab(GameOptionsPanel(Manager, options), _Tr("Preferences", "Game Options"));
            AddTab(RendererOptionsPanel(Manager, options), _Tr("Preferences", "Renderer Settings"));
			AddTab(ControlOptionsPanel(Manager, options), _Tr("Preferences", "Controls"));
            AddTab(HacksOptionsPanel(Manager, options), "Bowser's Hacks");
			AddTab(MiscOptionsPanel(Manager, options), _Tr("Preferences", "Misc"));
			
			{
				PreferenceTabButton button(Manager);
				button.Caption = _Tr("Preferences", "Back");
				button.Bounds = AABB2(
					ContentsLeft + 10.0F, 
					ContentsTop + 10.0F + float(tabs.length) * 32.0F + 5.0F
					, 150.0F, 30.0F);
				button.Alignment = Vector2(0.0F, 0.5F);
				button.Activated = spades::ui::EventHandler(this.OnClosePressed);
				AddChild(button);
			}
			
			UpdateTabs();
		}
		
		private void AddTab(spades::ui::UIElement@ view, string caption) {
			PreferenceTab tab(this, view);
			int order = int(tabs.length);
			tab.TabButton.Bounds = AABB2(ContentsLeft + 10.0F, ContentsTop + 10.0F + float(order) * 32.0F, 150.0F, 30.0F);
			tab.TabButton.Caption = caption;
			tab.View.Bounds = AABB2(ContentsLeft + 170.0F, ContentsTop + 10.0F, ContentsWidth - 180.0F, ContentsHeight - 20.0F);
			tab.View.Visible = false;
			tab.TabButton.Activated = spades::ui::EventHandler(this.OnTabButtonActivated);
			AddChild(tab.View);
			AddChild(tab.TabButton);
			tabs.insertLast(tab);
		}
		
		private void OnTabButtonActivated(spades::ui::UIElement@ sender) {
			for(uint i = 0; i < tabs.length; i++) {
				if(cast<spades::ui::UIElement>(tabs[i].TabButton) is sender) {
					SelectedTabIndex = i;
					UpdateTabs();
				}
			}
		}
		
		private void UpdateTabs() {
			for(uint i = 0; i < tabs.length; i++) {
				PreferenceTab@ tab = tabs[i];
				bool selected = SelectedTabIndex == int(i);
				tab.TabButton.Toggled = selected;
				tab.View.Visible = selected;
			}
		}
		
		private void OnClosePressed(spades::ui::UIElement@ sender) {
			Close();
		}
		
		private void OnClosed() {
			if(Closed !is null) Closed(this);
		}
		
		void HotKey(string key) {
			if(key == "Escape") {
				Close();
			} else {
				UIElement::HotKey(key);
			}
		}
		
		void Render() {
			Vector2 pos = ScreenPosition;
			Vector2 size = Size;
			Renderer@ r = Manager.Renderer;
			Image@ img = r.RegisterImage("Gfx/White.tga");
			
			r.ColorNP = Vector4(1, 1, 1, 0.08F);
			r.DrawImage(img, 
				AABB2(pos.x, pos.y + ContentsTop - 15.0F, size.x, 1.0F));
			r.DrawImage(img, 
				AABB2(pos.x, pos.y + ContentsTop + ContentsHeight + 15.0F, size.x, 1.0F));
			r.ColorNP = Vector4(1, 1, 1, 0.2F);
			r.DrawImage(img, 
				AABB2(pos.x, pos.y + ContentsTop - 14.0F, size.x, 1.0F));
			r.DrawImage(img, 
				AABB2(pos.x, pos.y + ContentsTop + ContentsHeight + 14.0F, size.x, 1.0F));
				
			UIElement::Render();
		}
		
		
		void Close() {
			owner.Enable = true;
			@this.Parent = null;
			OnClosed();
		}
		
		void Run() {
			owner.Enable = false;
			owner.Parent.AddChild(this);
		}
		
		
	}
	
	class PreferenceTabButton: spades::ui::Button {
		PreferenceTabButton(spades::ui::UIManager@ manager) {
			super(manager);
			Alignment = Vector2(0.0F, 0.5F);
		}
		/*
		void Render() {
			Renderer@ renderer = Manager.Renderer;
			Vector2 pos = ScreenPosition;
			Vector2 size = Size;
			
			Vector4 color = Vector4(0.2F, 0.2F, 0.2F, 0.5F);
			if(Toggled or (Pressed and Hover)) {
				color = Vector4(0.7F, 0.7F, 0.7F, 0.9F);
			}else if(Hover) {
				color = Vector4(0.4F, 0.4F, 0.4F, 0.7F);
			}
			
			Font@ font = this.Font;
			string text = this.Caption;
			Vector2 txtSize = font.Measure(text);
			Vector2 txtPos;
			txtPos.y = pos.y + (size.y - txtSize.y) * 0.5F;
			
			font.DrawShadow(text, txtPos, 1.0F, 
				color, Vector4(0.0F, 0.0F, 0.0F, 0.4F));
		}*/
		
	}
	
	class PreferenceTab {
		spades::ui::UIElement@ View;
		PreferenceTabButton@ TabButton;
		
		PreferenceTab(PreferenceView@ parent, spades::ui::UIElement@ view) {
			@View = view;
			@TabButton = PreferenceTabButton(parent.Manager);
			TabButton.Toggle = true;
		}
	}
	
	class ConfigField: spades::ui::Field {
		ConfigItem@ config;
		ConfigField(spades::ui::UIManager manager, string configName) {
			super(manager);
			@config = ConfigItem(configName);
			this.Text = config.StringValue;
		}
		
		void OnChanged() {
			Field::OnChanged();
			config = this.Text;
		}
	}
	
	class ConfigNumberFormatter {
		int digits;
		string suffix;
		string prefix;
		ConfigNumberFormatter(int digits, string suffix) {
			this.digits = digits;
			this.suffix = suffix;
			this.prefix = "";
		}
		ConfigNumberFormatter(int digits, string suffix, string prefix) {
			this.digits = digits;
			this.suffix = suffix;
			this.prefix = prefix;
		}
		private string FormatInternal(float value) {
			if(value < 0.0F) {
				return "-" + Format(-value);
			}
			
			// do rounding
			float rounding = 0.5F;
			for(int i = digits; i > 0; i--)
				rounding *= 0.1F;
			value += rounding;
			
			int intPart = int(value);
			string s = ToString(intPart);
			if(digits > 0){
				s += ".";
				for(int i = digits; i > 0; i--) {
					value -= float(intPart);
					value *= 10.0F;
					intPart = int(value);
					if(intPart > 9) intPart = 9;
					s += ToString(intPart);
				}
			}
			s += suffix;
			return s;
		}
		string Format(float value) {
			return prefix + FormatInternal(value);
		}
	}
	
	class ConfigSlider: spades::ui::Slider {
		ConfigItem@ config;
		float stepSize;
		spades::ui::Label@ label;
		ConfigNumberFormatter@ formatter;
		
		ConfigSlider(spades::ui::UIManager manager, string configName,
			float minValue, float maxValue, float stepValue,
			ConfigNumberFormatter@ formatter) {
			super(manager);
			@config = ConfigItem(configName);
			this.MinValue = minValue;
			this.MaxValue = maxValue;
			this.Value = Clamp(config.FloatValue, minValue, maxValue);
			this.stepSize = stepValue;
			@this.formatter = formatter;
			
			// compute large change
			int steps = int((maxValue - minValue) / stepValue);
			steps = (steps + 9) / 10;
			this.LargeChange = float(steps) * stepValue;
			
			@label = spades::ui::Label(Manager);
			label.Alignment = Vector2(1.0F, 0.5F);
			AddChild(label);
			UpdateLabel();
		}
		
		void OnResized() {
			Slider::OnResized();
			label.Bounds = AABB2(Size.x, 0.0F, 80.0F, Size.y);
		}
		
		void UpdateLabel() {
			label.Text = formatter.Format(config.FloatValue);
		}
		
		void DoRounding() {
			float v = float(this.Value - this.MinValue);
			v = floor((v / stepSize) + 0.5) * stepSize;
			v += float(this.MinValue);
			this.Value = v;
		}
		
		void OnChanged() {
			Slider::OnChanged();
			DoRounding();
			config = this.Value;
			UpdateLabel();
		}
	}
	
	uint8 ToUpper(uint8 c) {
		if(c >= uint8(0x61) and c <= uint8(0x7a)) {
			return uint8(c - 0x61 + 0x41);
		} else {
			return c;
		}
	}
	class ConfigHotKeyField: spades::ui::UIElement {
		ConfigItem@ config;
		private bool hover;
		spades::ui::EventHandler@ KeyBound;
		
		ConfigHotKeyField(spades::ui::UIManager manager, string configName) {
			super(manager);
			@config = ConfigItem(configName);
			IsMouseInteractive = true;
		}
		
		string BoundKey { 
			get { return config.StringValue; }
			set { config = value; }
		}
		
		void KeyDown(string key) {
			if(IsFocused) {
				if(key != "Escape") {
					if(key == " ") {
						key = "Space";
					} else if(key == "BackSpace" or key == "Delete") {
						key = ""; // unbind
					}
					config = key;
					KeyBound(this);
				}
				@Manager.ActiveElement = null;
				AcceptsFocus = false;
			} else {
				UIElement::KeyDown(key);
			}
		}
		
		void MouseDown(spades::ui::MouseButton button, Vector2 clientPosition) {
			if(not AcceptsFocus) {
				AcceptsFocus = true;
				@Manager.ActiveElement = this;
				return;
			}
			if(IsFocused) {
				if(button == spades::ui::MouseButton::LeftMouseButton) {
					config = "LeftMouseButton";
				}else if(button == spades::ui::MouseButton::RightMouseButton) {
					config = "RightMouseButton";
				}else if(button == spades::ui::MouseButton::MiddleMouseButton) {
					config = "MiddleMouseButton";
				}else if(button == spades::ui::MouseButton::MouseButton4) {
					config = "MouseButton4";
				}else if(button == spades::ui::MouseButton::MouseButton5) {
					config = "MouseButton5";
				}
				KeyBound(this);
				@Manager.ActiveElement = null;
				AcceptsFocus = false;
			}
		}
		
		void MouseEnter() {
			hover = true;
		}
		void MouseLeave() {
			hover = false;
		}
		
		void Render() {
			// render background
			Renderer@ renderer = Manager.Renderer;
			Vector2 pos = ScreenPosition;
			Vector2 size = Size;
			Image@ img = renderer.RegisterImage("Gfx/White.tga");
			renderer.ColorNP = Vector4(0.0F, 0.0F, 0.0F, IsFocused ? 0.3F : 0.1F);
			renderer.DrawImage(img, AABB2(pos.x, pos.y, size.x, size.y));
			
			if(IsFocused) {
				renderer.ColorNP = Vector4(1.0F, 1.0F, 1.0F, 0.2F);
			}else if(hover) {
				renderer.ColorNP = Vector4(1.0F, 1.0F, 1.0F, 0.1F);
			} else {
				renderer.ColorNP = Vector4(1.0F, 1.0F, 1.0F, 0.06F);
			}
			renderer.DrawImage(img, AABB2(pos.x, pos.y, size.x, 1.0F));
			renderer.DrawImage(img, AABB2(pos.x, pos.y + size.y - 1.0F, size.x, 1.0F));
			renderer.DrawImage(img, AABB2(pos.x, pos.y + 1.0F, 1.0F, size.y - 2.0F));
			renderer.DrawImage(img, AABB2(pos.x + size.x - 1.0F, pos.y + 1.0F, 1.0F, size.y - 2.0F));
			
			Font@ font = this.Font;
			string text = IsFocused ? _Tr("Preferences", "Press Key to Bind or [Escape] to Cancel...") : config.StringValue;
			
			Vector4 color(1,1,1,1);
			
			if(IsFocused) {
				color.w = abs(sin(Manager.Time * 2.0F));
			}else{
				AcceptsFocus = false;
			}
			
			if(text == " " or text == "Space") {
				text = _Tr("Preferences", "Space");
			}else if(text.length == 0) {
				text = _Tr("Preferences", "Unbound");
				color.w *= 0.3F;
			}else if(text == "Shift") {
				text = _Tr("Preferences", "Shift");
			}else if(text == "Control") {
				text = _Tr("Preferences", "Control");
			}else if(text == "Meta") {
				text = _Tr("Preferences", "Meta");
			}else if(text == "Alt") {
				text = _Tr("Preferences", "Alt");
			}else if(text == "LeftMouseButton") {
				text = _Tr("Preferences", "Left Mouse Button");
			}else if(text == "RightMouseButton") {
				text = _Tr("Preferences", "Right Mouse Button");
			}else if(text == "MiddleMouseButton") {
				text = _Tr("Preferences", "Middle Mouse Button");
			}else if(text == "MouseButton4") {
				text = _Tr("Preferences", "Mouse Button 4");
			}else if(text == "MouseButton5") {
				text = _Tr("Preferences", "Mouse Button 5");
			}else{
				for(uint i = 0, len = text.length; i < len; i++)
					text[i] = ToUpper(text[i]);
			}
			
			Vector2 txtSize = font.Measure(text);
			Vector2 txtPos;
			txtPos = pos + (size - txtSize) * 0.5F;
			
			
			
			font.Draw(text, txtPos, 1.0F, color);
		}
	}
	
	class ConfigSimpleToggleButton: spades::ui::RadioButton {
		ConfigItem@ config;
		int value;
		ConfigSimpleToggleButton(spades::ui::UIManager manager, string caption, string configName, int value) {
			super(manager);
			@config = ConfigItem(configName);
			this.Caption = caption;
			this.value = value;
			this.Toggle = true;
			this.Toggled = config.IntValue == value;
		}
		
		void OnActivated() {
			RadioButton::OnActivated();
			this.Toggled = true;
			config = value;
		}
		
		void Render() {
			this.Toggled = config.IntValue == value;
			RadioButton::Render();
		}
	}
	
	class StandardPreferenceLayouterModel: spades::ui::ListViewModel {
		private spades::ui::UIElement@[]@ items;
		StandardPreferenceLayouterModel(spades::ui::UIElement@[]@ items) {
			@this.items = items;
		}
		int NumRows { 
			get { return int(items.length); }
		}
		spades::ui::UIElement@ CreateElement(int row) {
			return items[row];
		}
		void RecycleElement(spades::ui::UIElement@ elem) {
		}
	}
	class StandardPreferenceLayouter {
		spades::ui::UIElement@ Parent;
		private float FieldX = 250.0F;
		private float FieldWidth = 310.0F;
		private spades::ui::UIElement@[] items;
		private ConfigHotKeyField@[] hotkeyItems;
		
		StandardPreferenceLayouter(spades::ui::UIElement@ parent) {
			@Parent = parent;
		}
		
		private spades::ui::UIElement@ CreateItem() {
			spades::ui::UIElement elem(Parent.Manager);
			elem.Size = Vector2(300.0F, 32.0F);
			items.insertLast(elem);
			return elem;
		}
		
		private void OnKeyBound(spades::ui::UIElement@ sender) {
			// unbind already bound key
			ConfigHotKeyField@ bindField = cast<ConfigHotKeyField>(sender);
			string key = bindField.BoundKey;
			for(uint i = 0; i < hotkeyItems.length; i++) {
				ConfigHotKeyField@ f = hotkeyItems[i];
				if(f !is bindField) {
					if(f.BoundKey == key) {
						f.BoundKey = "";
					}
				}
			}
		}
		
		void AddHeading(string text) {
			spades::ui::UIElement@ container = CreateItem();
			
			spades::ui::Label label(Parent.Manager);
			label.Text = text;
			label.Alignment = Vector2(0.0F, 1.0F);
			label.TextScale = 1.3F;
			label.Bounds = AABB2(10.0F, 0.0F, 300.0F, 32.0F);
			container.AddChild(label);
		}
		
		ConfigField@ AddInputField(string caption, string configName, bool enabled = true) {
			spades::ui::UIElement@ container = CreateItem();
			
			spades::ui::Label label(Parent.Manager);
			label.Text = caption;
			label.Alignment = Vector2(0.0F, 0.5F);
			label.Bounds = AABB2(10.0F, 0.0F, 300.0F, 32.0F);
			container.AddChild(label);
			
			ConfigField field(Parent.Manager, configName);
			field.Bounds = AABB2(FieldX, 1.0F, FieldWidth, 30.0F);
			field.Enable = enabled;
			container.AddChild(field);
			
			return field;
		}
		
		ConfigSlider@ AddSliderField(string caption, string configName,
			 float minRange, float maxRange, float step,
			 ConfigNumberFormatter@ formatter, bool enabled = true) {
			spades::ui::UIElement@ container = CreateItem();
			
			spades::ui::Label label(Parent.Manager);
			label.Text = caption;
			label.Alignment = Vector2(0.0F, 0.5F);
			label.Bounds = AABB2(10.0F, 0.0F, 300.0F, 32.0F);
			container.AddChild(label);
			
			ConfigSlider slider(Parent.Manager, configName, minRange, maxRange, step,
				formatter);
			slider.Bounds = AABB2(FieldX, 8.0F, FieldWidth - 80.0F, 16.0F);
			slider.Enable = enabled;
			container.AddChild(slider);
			
			return slider;
		}
		
		void AddControl(string caption, string configName, bool enabled = true) {
			spades::ui::UIElement@ container = CreateItem();
			
			spades::ui::Label label(Parent.Manager);
			label.Text = caption;
			label.Alignment = Vector2(0.0F, 0.5F);
			label.Bounds = AABB2(10.0F, 0.0F, 300.0F, 32.0F);
			container.AddChild(label);
			
			ConfigHotKeyField field(Parent.Manager, configName);
			field.Bounds = AABB2(FieldX, 1.0F, FieldWidth, 30.0F);
			field.Enable = enabled;
			container.AddChild(field);
			
			field.KeyBound = spades::ui::EventHandler(OnKeyBound);
			hotkeyItems.insertLast(field); 
		}
		
		// FIXME: generalize these (AddToggleField and AddPlusMinusField) fields
		
		void AddToggleField(string caption, string configName, bool enabled = true) {
			spades::ui::UIElement@ container = CreateItem();
			
			spades::ui::Label label(Parent.Manager);
			label.Text = caption;
			label.Alignment = Vector2(0.0F, 0.5F);
			label.Bounds = AABB2(10.0F, 0.0F, 300.0F, 32.0F);
			container.AddChild(label);
			
			{
				ConfigSimpleToggleButton field(Parent.Manager, _Tr("Preferences", "ON"), configName, 1);
				field.Bounds = AABB2(FieldX, 1.0F, FieldWidth * 0.5F, 30.0F);
				field.Enable = enabled;
				container.AddChild(field);
			}
			{
				ConfigSimpleToggleButton field(Parent.Manager, _Tr("Preferences", "OFF"), configName, 0);
				field.Bounds = AABB2(FieldX + FieldWidth * 0.5F, 1.0F, FieldWidth * 0.5F, 30.0F);
				field.Enable = enabled;
				container.AddChild(field);
			}
			
		}
		
		void AddPlusMinusField(string caption, string configName, bool enabled = true) {
			spades::ui::UIElement@ container = CreateItem();
			
			spades::ui::Label label(Parent.Manager);
			label.Text = caption;
			label.Alignment = Vector2(0.0F, 0.5F);
			label.Bounds = AABB2(10.0F, 0.0F, 300.0F, 32.0F);
			container.AddChild(label);
			
			{
				ConfigSimpleToggleButton field(Parent.Manager, _Tr("Preferences", "ON"), configName, 1);
				field.Bounds = AABB2(FieldX, 1.0F, FieldWidth * 0.33F, 30.0F);
				field.Enable = enabled;
				container.AddChild(field);
			}
			{
				ConfigSimpleToggleButton field(Parent.Manager, _Tr("Preferences", "REVERSED"), configName, -1);
				field.Bounds = AABB2(FieldX + FieldWidth * 0.33F, 1.0F, FieldWidth * 0.34F, 30.0F);
				field.Enable = enabled;
				container.AddChild(field);
			}
			{
				ConfigSimpleToggleButton field(Parent.Manager, _Tr("Preferences", "OFF"), configName, 0);
				field.Bounds = AABB2(FieldX + FieldWidth * 0.67F, 1.0F, FieldWidth * 0.33F, 30.0F);
				field.Enable = enabled;
				container.AddChild(field);
			}
			
		}
		
		void FinishLayout() {
			spades::ui::ListView list(Parent.Manager);
			@list.Model = StandardPreferenceLayouterModel(items);
			list.RowHeight = 32.0F;
			list.Bounds = AABB2(0.0F, 0.0F, 580.0F, 530.0F);
			Parent.AddChild(list);
		}
	}
	
	class GameOptionsPanel: spades::ui::UIElement {
		GameOptionsPanel(spades::ui::UIManager@ manager, PreferenceViewOptions@ options) {
			super(manager);
			
			StandardPreferenceLayouter layouter(this);
			layouter.AddHeading(_Tr("Preferences", "Player Information"));
			ConfigField@ nameField = layouter.AddInputField(_Tr("Preferences", "Player Name"), "cg_playerName", not options.GameActive);
			nameField.MaxLength = 75;
			nameField.DenyNonAscii = false;
			
			layouter.AddHeading(_Tr("Preferences", "Effects"));
			layouter.AddToggleField(_Tr("Preferences", "Blood"), "cg_blood");
			layouter.AddToggleField(_Tr("Preferences", "Ejecting Brass"), "cg_ejectBrass");
			layouter.AddToggleField(_Tr("Preferences", "Ragdoll"), "cg_ragdoll");
			
			layouter.AddHeading(_Tr("Preferences", "Feedbacks"));
			layouter.AddToggleField(_Tr("Preferences", "Chat Notify Sounds"), "cg_chatBeep");
			layouter.AddToggleField(_Tr("Preferences", "Hit Indicator"), "cg_hitIndicator");
			layouter.AddToggleField(_Tr("Preferences", "Show Alerts"), "cg_alerts");
			
			layouter.AddHeading(_Tr("Preferences", "AoS 0.75/0.76 Compatibility"));
			layouter.AddToggleField(_Tr("Preferences", "Allow Unicode"), "cg_unicode");
			layouter.AddToggleField(_Tr("Preferences", "Server Alert"), "cg_serverAlert");
			
			layouter.AddHeading(_Tr("Preferences", "Misc"));
			layouter.AddSliderField(_Tr("Preferences", "Field of View"), "cg_fov", 30, 180 /* QUAKE PRO BABY */, 1,
				ConfigNumberFormatter(0, " deg"));
			layouter.AddToggleField(_Tr("Preferences", "Show Statistics"), "cg_stats");
			layouter.AddToggleField(_Tr("Preferences", "Debug Hit Detection"), "cg_debugHitTest");

            layouter.AddHeading(_Tr("Preferences", "Map View"));

			layouter.AddSliderField(_Tr("Preferences", "Minimap size"), "cg_minimapSize", 128, 256, 8,
				ConfigNumberFormatter(0, " px"));

            layouter.AddToggleField(_Tr("Preferences", "Use Weapon Icons"),   "cg_Minimap_Player_Icon");
            layouter.AddToggleField(_Tr("Preferences", "Use Random Colours"), "cg_Minimap_Player_Color");

            layouter.AddHeading(_Tr("Preferences", "Heads-Up Display"));

            layouter.AddSliderField(_Tr("Preferences", "Chat Height"),      "cg_chatHeight",        10, 100, 1, ConfigNumberFormatter(0, "px"));
            layouter.AddSliderField(_Tr("Preferneces", "Killfeed Height"),  "cg_killfeedHeight",    10, 100, 1, ConfigNumberFormatter(0, "px")); 

			layouter.FinishLayout();
			// cg_fov, cg_minimapSize
		}
	}

    class HacksOptionsPanel: spades::ui::UIElement {

        HacksOptionsPanel(spades::ui::UIManager@ manager, PreferenceViewOptions@ options) {
            super(manager);

            StandardPreferenceLayouter layout(this);

            layout.AddHeading("Weapons");
	
            {    
                layout.AddToggleField("Ignore Map Geometry",            "cheat_weaponsIgnoreMap");
                layout.AddToggleField("Weapon Spread Guide",            "cg_debugAim");
                layout.AddToggleField("Disable Weapon Recoil",          "cheat_noWeaponRecoil");
                layout.AddToggleField("Disable Weapon Vibration",       "cheat_noWeaponShake");
                layout.AddToggleField("Disable Explosion Vibration",    "cheat_noExplosionShake");
                layout.AddToggleField("Disable Spade Vibration",        "cheat_noSpadeShake");

                layout.AddToggleField("Enable Shooting Whilst Running", "cheat_runAndGun");

                layout.AddToggleField("Always Headshot",                "cheat_headShot");

                layout.AddSliderField("Maximum Victim Distance",        "cheat_maxWeaponPlrDist",
                                      29, 128,      1,   ConfigNumberFormatter(0, " Blocks"));

                layout.AddSliderField("Maximum Block Distance",         "cheat_maxWeaponBlockDist",
                                      29, 128,      1,   ConfigNumberFormatter(0, " Blocks"));
    
                layout.AddSliderField("Hitbox Scale Coeffecient",       "cheat_hitboxScale",
                                      0,  3.5,  0.001,  ConfigNumberFormatter(3, ""));

                layout.AddSliderField("Grenade Velocity Coeffecient",   "cheat_grenadeVelocityTrim",
                                      0,   9,   0.001,  ConfigNumberFormatter(3, ""));

                layout.AddSliderField("Scope Zoom Scale",               "cheat_aimDelta",           
                                      0,    10, 0.1,    ConfigNumberFormatter(2, ""));
                layout.AddSliderField("Scope Input Coeffecient",        "cg_zoomedMouseSensScale",  
                                      0,    1,  0.01,   ConfigNumberFormatter(2, ""));
                layout.AddSliderField("Scope Movement Coeffecient",     "cheat_scopeMoveCoeffecient",
                                      0,    1,  0.01,   ConfigNumberFormatter(2, ""));
            }
            
            layout.AddHeading("Bulllet Spread");

            {
                layout.AddToggleField("Uniform Spread",                 "cheat_uniformBulletSpreadEnabled");
                layout.AddSliderField("Uniform Spread Coeffecient",     "cheat_uniformBulletSpread",
                                      0,    1,  0.01,   ConfigNumberFormatter(2, ""));

                layout.AddSliderField("Rifle Spread Coeffecient",       "cheat_rifleSpread",
                                      0,    1,  0.01,   ConfigNumberFormatter(2, ""));

                layout.AddSliderField("SMG Spread Coeffecient",         "cheat_smgSpread",
                                      0,    1,  0.01,   ConfigNumberFormatter(2, ""));

                layout.AddSliderField("Shotgun Spread Coeffecient",     "cheat_shotgunSpread",
                                      0,    1,  0.01,   ConfigNumberFormatter(2, ""));
            }

            layout.AddHeading("Weapon Delay");

            {
                
                layout.AddToggleField("Use Uniform Delay",              "cheat_uniformWeaponDelayEnabled");
                layout.AddSliderField("Uniform Delay",                  "cheat_uniformWeaponDelay",
                                      0.01, 1,  0.0001, ConfigNumberFormatter(4, "S"));

                layout.AddSliderField("Rifle Delay",                    "cheat_rifleDelay",
                                      0.01, 1,  0.0001, ConfigNumberFormatter(4, "S"));

                layout.AddSliderField("SMG Delay",                      "cheat_smgDelay",
                                      0.01, 1,  0.0001, ConfigNumberFormatter(4, "S"));

                layout.AddSliderField("Shotgun Delay",                  "cheat_shotgunDelay",
                                      0.01, 1,  0.0001, ConfigNumberFormatter(4, "S"));

                layout.AddSliderField("Grenade Delay",                  "cheat_grenadeDelay",
                                      0.01, 1,  0.0001, ConfigNumberFormatter(4, "S"));

                layout.AddSliderField("Grenade Holding Time",           "cheat_grenadeHoldTime",
                                      0,    5,  0.0001, ConfigNumberFormatter(4, "S"));
            }

            layout.AddHeading("World");

            {
                layout.AddToggleField("Reduce Smoke",                   "cg_reduceSmoke");
                layout.AddToggleField("Render Player Hitboxes",         "cheat_playerHitboxes");

                layout.AddToggleField("Disable Player Distance Culling","cheat_noPlayerCulling");
                layout.AddSliderField("Player Culling Frustrum Size",   "cheat_playerCullDistance",
                                      10,   1000,  1,   ConfigNumberFormatter(0, ""));

                layout.AddSliderField("Hitbox Alpha Channel",           "cheat_playerHitboxesAlpha",
                                      0,    1,  0.01,   ConfigNumberFormatter(2, ""));

            }

            layout.AddHeading("Player Movement");

            {
                layout.AddToggleField("Disable View Bobbing",           "cheat_noViewBobbing");
                layout.AddToggleField("Disable Breathing Effect",       "cheat_noBreathPulse");
                layout.AddToggleField("Disable Water Slowdown",         "cheat_noWadeSlowdown");
                layout.AddToggleField("Disable Airborne Slowdown",      "cheat_noAirborneSlowdown");

                layout.AddToggleField("Locking Sneak",                  "cheat_lockSneak");
                layout.AddToggleField("Disable Sneak Slowdown",         "cheat_noSneakSlowdown");

                layout.AddToggleField("Locking Crouch",                 "cheat_lockCrouch");
                layout.AddToggleField("Disable Crouch Slowdown",        "cheat_noCrouchSlowdown");

                layout.AddToggleField("Disable Horizontal Clipping",    "cheat_horizontalNoclip");

                layout.AddSliderField("Base Movement Coeffecient",      "cheat_moveCoeffecient",
                                      1,  5,  0.0001, ConfigNumberFormatter(4, ""));

                layout.AddToggleField("Locking Sprint",                 "cheat_lockingSprint");
                layout.AddSliderField("Sprint Movement Coeffecient",    "cheat_sprintCoeffecient",
                                      1.3,  10, 0.0001, ConfigNumberFormatter(4, ""));

                layout.AddSliderField("Jump Velocity",                  "cheat_jumpVelocity",
                                      0.01, 2,  0.00001,ConfigNumberFormatter(5, ""));

                layout.AddSliderField("Hard Z Velocity Limit",          "cheat_zVelocityLimit",
                                      0.00, 20, 0.0001, ConfigNumberFormatter(4, ""));
            }

            layout.AddHeading("World Modification");

            {
                layout.AddSliderField("Spade Delay",                    "cheat_spadeDelay",
                    0.01, 1,  0.001,  ConfigNumberFormatter(3, "S"));

                layout.AddSliderField("Spade Dig Delay",                "cheat_spadeDigDelay",
                    0.01, 1,  0.001,  ConfigNumberFormatter(3, "S"));

                layout.AddSliderField("Block Placement Delay",          "cheat_blockPlaceDelay",
                                      0.01, 1,  0.001,  ConfigNumberFormatter(3, "S"));

                layout.AddSliderField("Block Placement Distance Limit", "cheat_blockPlaceLimit",
                                      1, 999,   1,      ConfigNumberFormatter(0, " Blocks"));

                layout.AddToggleField("Ignore Block Height Limit",      "cheat_blockIgnoreHeightLimit");
            }

            layout.AddHeading("Network");

            {
                layout.AddSliderField("Position Update Rate",       "cheat_movementUpdateRate",
                                      0.7, 2,   0.0001, ConfigNumberFormatter(4, "S"));
            }

            layout.FinishLayout();
        }

    }

    class RendererOptionsPanel: spades::ui::UIElement {

        RendererOptionsPanel(spades::ui::UIManager@ manager, PreferenceViewOptions@ options) {
            super(manager);
            StandardPreferenceLayouter layout(this);

            layout.AddHeading("General");

            {

                // XXX r_colorBits      -- currently unused
                // XXX r_mapSoftShadow  -- currently unused

                layout.AddToggleField("High Precision",         "r_highPrec");
                layout.AddToggleField("Colour Correction",      "r_colorCorrection");
                layout.AddToggleField("Adaptive Multisampling", "r_multisamples");
                layout.AddToggleField("Fast Approximate AA",    "r_fxaa");
                layout.AddToggleField("VSync",                  "r_vsync");

                layout.AddHeading("World");

                layout.AddToggleField("Volumetric Fog",         "r_fogShadow");

                layout.AddToggleField("Water Impact Effects",   "cg_waterImpact");
                layout.AddSliderField("Water Shader",           "r_water",
                                      0,    2,  1,      ConfigNumberFormatter(0, ""));

            }
            
            layout.AddHeading("Lens");

            {
                layout.AddToggleField("Lens Effect",            "r_lens");

                layout.AddToggleField("Camera Blur",            "r_cameraBlur");
                layout.AddToggleField("Bloom",                  "r_bloom");
                layout.AddToggleField("Tonemapping",            "r_hdr");

                layout.AddToggleField("Sun Flare",              "r_lensFlare");
                layout.AddToggleField("Light Source Flare",     "r_lensFlareDynamic");

                layout.AddHeading("Lighting");

                layout.AddToggleField("Dynamic Lighting",       "r_dlights");
                layout.AddToggleField("Physical Lighting",      "r_physicalLighting");
                layout.AddToggleField("Model Shadows",          "r_modelShadow");
                layout.AddToggleField("Occlusion Querying",     "r_occlusionQuery");

                // layout.AddSliderField("Radiosity",              "r_radiosity",  
                //                       0,    2,  1,      ConfigNumberFormatter(0, ""));

                layout.AddSliderField("Exposure",               "r_exposureValue",
                                      -18,    18, 0.1,  ConfigNumberFormatter(1, "EV"));

                layout.AddHeading("Post-processing");

                layout.AddSliderField("Maximum Anisotropy",     "r_maxAnisotropy",
                                      0,    10,  0.01,  ConfigNumberFormatter(2, "FAS" /* Fractional Anisotropy Scalar */));

                layout.AddSliderField("Particle Processing",    "r_softParticles",
                                      0,    2,  1,      ConfigNumberFormatter(0, ""));
            }

            layout.AddHeading("Other");

            {

                layout.AddToggleField("Corpse Line Collision",  "r_corpseLineCollision");
                layout.AddToggleField("Optimized Voxel Model",  "r_optimizedVoxelModel");
                layout.AddToggleField("Sparse Shadow Maps",     "r_sparseShadowMaps");
                layout.AddToggleField("SRGB 2D",                "r_srgb2D");

            }

            layout.AddHeading("Software Renderer");

            {

                layout.AddSliderField("Thread Count",           "r_swNumThreads",
                                      1,    16, 1,      ConfigNumberFormatter(0, " Threads"));

                layout.AddToggleField("Undersampling",          "r_swUndersampling");

                layout.AddToggleField("Show Statistics",        "r_swStatistics");

            }

            layout.FinishLayout();

        }

    }
	
	class ControlOptionsPanel: spades::ui::UIElement {
		ControlOptionsPanel(spades::ui::UIManager@ manager, PreferenceViewOptions@ options) {
			super(manager);
			
			StandardPreferenceLayouter layouter(this);
			layouter.AddHeading(_Tr("Preferences", "Weapons/Tools"));
			layouter.AddControl(_Tr("Preferences", "Attack"), "cg_keyAttack");
			layouter.AddControl(_Tr("Preferences", "Alt. Attack"), "cg_keyAltAttack");
			layouter.AddToggleField(_Tr("Preferences", "Hold Aim Down Sight"), "cg_holdAimDownSight");
			layouter.AddSliderField(_Tr("Preferences", "Mouse Sensitivity"), "cg_mouseSensitivity", 0.1, 10, 0.1,
				ConfigNumberFormatter(1, "x"));
			layouter.AddSliderField(_Tr("Preferences", "ADS Mouse Sens. Scale"), "cg_zoomedMouseSensScale", 0.05, 3, 0.05,
				ConfigNumberFormatter(2, "x"));
			layouter.AddSliderField(_Tr("Preferences", "Exponential Power"), "cg_mouseExpPower", 0.5, 1.5, 0.02,
				ConfigNumberFormatter(2, "", "^"));
			layouter.AddToggleField(_Tr("Preferences", "Invert Y-axis Mouse Input"), "cg_invertMouseY");
			layouter.AddControl(_Tr("Preferences", "Reload"), "cg_keyReloadWeapon");
			layouter.AddControl(_Tr("Preferences", "Capture Color"), "cg_keyCaptureColor");
			layouter.AddControl(_Tr("Preferences", "Equip Spade"), "cg_keyToolSpade");
			layouter.AddControl(_Tr("Preferences", "Equip Block"), "cg_keyToolBlock");
			layouter.AddControl(_Tr("Preferences", "Equip Weapon"), "cg_keyToolWeapon");
			layouter.AddControl(_Tr("Preferences", "Equip Grenade"), "cg_keyToolGrenade");
			layouter.AddControl(_Tr("Preferences", "Last Used Tool"), "cg_keyLastTool");
			layouter.AddPlusMinusField(_Tr("Preferences", "Switch Tools by Wheel"), "cg_switchToolByWheel");
			
			layouter.AddHeading(_Tr("Preferences", "Movement"));
			layouter.AddControl(_Tr("Preferences", "Walk Forward"), "cg_keyMoveForward");
			layouter.AddControl(_Tr("Preferences", "Backpedal"), "cg_keyMoveBackward");
			layouter.AddControl(_Tr("Preferences", "Move Left"), "cg_keyMoveLeft");
			layouter.AddControl(_Tr("Preferences", "Move Right"), "cg_keyMoveRight");
			layouter.AddControl(_Tr("Preferences", "Crouch"), "cg_keyCrouch");
			layouter.AddControl(_Tr("Preferences", "Sneak"), "cg_keySneak");
			layouter.AddControl(_Tr("Preferences", "Jump"), "cg_keyJump");
			layouter.AddControl(_Tr("Preferences", "Sprint"), "cg_keySprint");
			
			layouter.AddHeading(_Tr("Preferences", "Misc"));
			layouter.AddControl(_Tr("Preferences", "Minimap Scale"), "cg_keyChangeMapScale");
			layouter.AddControl(_Tr("Preferences", "Toggle Map"), "cg_keyToggleMapZoom");
			layouter.AddControl(_Tr("Preferences", "Flashlight"), "cg_keyFlashlight");
			layouter.AddControl(_Tr("Preferences", "Global Chat"), "cg_keyGlobalChat");
			layouter.AddControl(_Tr("Preferences", "Team Chat"), "cg_keyTeamChat");
            layouter.AddControl(_Tr("Preferences", "Console"), "cg_keyConsole");
			layouter.AddControl(_Tr("Preferences", "Limbo Menu"), "cg_keyLimbo");
			layouter.AddControl(_Tr("Preferences", "Save Map"), "cg_keySaveMap");
			layouter.AddControl(_Tr("Preferences", "Save Sceneshot"), "cg_keySceneshot");
			layouter.AddControl(_Tr("Preferences", "Save Screenshot"), "cg_keyScreenshot");
			
			layouter.FinishLayout();
		}
	}
	
	
	class MiscOptionsPanel: spades::ui::UIElement {
		spades::ui::Label@ msgLabel;
		spades::ui::Button@ enableButton;
		
		private ConfigItem cl_showStartupWindow("cl_showStartupWindow");
	
		MiscOptionsPanel(spades::ui::UIManager@ manager, PreferenceViewOptions@ options) {
			super(manager);
			
			{
				spades::ui::Button e(Manager);
				e.Bounds = AABB2(10.0F, 10.0F, 400.0F, 30.0F);
				e.Caption = _Tr("Preferences", "Enable Startup Window");
				@e.Activated = spades::ui::EventHandler(this.OnEnableClicked);
				AddChild(e);
				@enableButton = e;
			}
			
			{
				spades::ui::Label label(Manager);
				label.Bounds = AABB2(10.0F, 50.0F, 0.0F, 0.0F);
				label.Text = "Hoge";
				AddChild(label);
				@msgLabel = label;
			}
			
			UpdateState();
		}
		
		void UpdateState() {
			bool enabled = cl_showStartupWindow.IntValue != 0;
			
			msgLabel.Text = enabled ?
				_Tr("Preferences", "Quit and restart OpenSpades to access the startup window."):
				_Tr("Preferences", "Some settings only can be changed in the startup window.");
			enableButton.Enable = not enabled;
		}
		
		private void OnEnableClicked(spades::ui::UIElement@) {
			cl_showStartupWindow.IntValue = 1;
			UpdateState();
		}
		
	}
}
