/*
 Copyright (c) 2013 yvt
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */
 
 namespace spades {
	class ViewShotgunSkin: 
	IToolSkin, IViewToolSkin, IWeaponSkin,
	BasicViewWeapon {
		
		private AudioDevice@ audioDevice;
		private Model@ gunModel;
		private Model@ pumpModel;
		private Model@ sightModel1;
		private Model@ sightModel2;
		
		private AudioChunk@ fireSound;
		private AudioChunk@ fireFarSound;
		private AudioChunk@ fireStereoSound;
		private AudioChunk@ reloadSound;
		private AudioChunk@ cockSound;
		
		ViewShotgunSkin(Renderer@ r, AudioDevice@ dev){
			super(r);
			@audioDevice = dev;
			@gunModel = renderer.RegisterModel
				("Models/Weapons/Shotgun/WeaponNoPump.kv6");
			@pumpModel = renderer.RegisterModel
				("Models/Weapons/Shotgun/Pump.kv6");
			@sightModel1 = renderer.RegisterModel
				("Models/Weapons/Shotgun/Sight1.kv6");
			@sightModel2 = renderer.RegisterModel
				("Models/Weapons/Shotgun/Sight2.kv6");
				
				
			@fireSound = dev.RegisterSound
				("Sounds/Weapons/Shotgun/FireLocal.wav");
			@fireFarSound = dev.RegisterSound
				("Sounds/Weapons/Shotgun/FireFar.wav");
			@fireStereoSound = dev.RegisterSound
				("Sounds/Weapons/Shotgun/FireStereo.wav");
			@reloadSound = dev.RegisterSound
				("Sounds/Weapons/Shotgun/ReloadLocal.wav");
			@cockSound = dev.RegisterSound
				("Sounds/Weapons/Shotgun/CockLocal.wav");
		}
		
		void Update(float dt) {
			BasicViewWeapon::Update(dt);
		}
		
		void WeaponFired(){
			BasicViewWeapon::WeaponFired();
			
			if(!IsMuted){
				Vector3 origin = Vector3(0.4F, -0.3F, 0.5F);
				AudioParam param;
				param.volume = 8.0F;
				audioDevice.PlayLocal(fireSound, origin, param);
				
				param.volume = 2.0F;
				audioDevice.PlayLocal(fireFarSound, origin, param);
				audioDevice.PlayLocal(fireStereoSound, origin, param);
			}
		}
		
		void ReloadingWeapon() {
			if(!IsMuted){
				Vector3 origin = Vector3(0.4F, -0.3F, 0.5F);
				AudioParam param;
				param.volume = 0.2F;
				audioDevice.PlayLocal(reloadSound, origin, param);
			}
		}
		
		void ReloadedWeapon() {
			if(!IsMuted){
				Vector3 origin = Vector3(0.4F, -0.3F, 0.5F);
				AudioParam param;
				param.volume = 0.2F;
				audioDevice.PlayLocal(cockSound, origin, param);
			}
		}
		
		float GetZPos() {
			return 0.2F - AimDownSightStateSmooth * 0.0535F;
		}
		
		// rotates gun matrix to ensure the sight is in
		// the center of screen (0, ?, 0).
		Matrix4 AdjustToAlignSight(Matrix4 mat, Vector3 sightPos, float fade) {
			Vector3 p = mat * sightPos;
			mat = CreateRotateMatrix(Vector3(0.0F, 0.0F, 1.0F), atan(p.x / p.y) * fade) * mat;
			mat = CreateRotateMatrix(Vector3(-1.0F, 0.0F, 0.0F), atan(p.z / p.y) * fade) * mat;
			return mat;
		}
		
		void Draw2D() {
			if(AimDownSightState > 0.6)
				return;
			BasicViewWeapon::Draw2D();
		}
		
		void AddToScene() {
			Matrix4 mat = CreateScaleMatrix(0.033F);
			mat = GetViewWeaponMatrix() * mat;
			
			bool reloading = IsReloading;
			float reload = ReloadProgress;
			Vector3 leftHand, rightHand;
			
			rightHand = mat * Vector3(0.0F, -8.0F, 2.0F);
			
			Vector3 leftHand2 = mat * Vector3(5.0F, -10.0F, 4.0F);
			Vector3 leftHand3 = mat * Vector3(1.0F, 1.0F, 2.0F);
			
			if(AimDownSightStateSmooth > 0.8F){
				mat = AdjustToAlignSight(mat, Vector3(0.0F, 8.5F, -4.4F), 
					(AimDownSightStateSmooth - 0.8F) / 0.2F);
			}
			
			ModelRenderParam param;
			Matrix4 weapMatrix = eyeMatrix * mat;
			param.matrix = weapMatrix;
			param.depthHack = true;
			renderer.AddModel(gunModel, param);
			
			// draw sights
			Matrix4 sightMat = weapMatrix;
			sightMat *= CreateTranslateMatrix(0.025F, -9.0F, -4.4F);
			sightMat *= CreateScaleMatrix(0.05F);
			param.matrix = sightMat;
			renderer.AddModel(sightModel2, param); // rear
			
			sightMat = weapMatrix;
			sightMat *= CreateTranslateMatrix(0.025F, 8.5F, -4.4F);
			sightMat *= CreateScaleMatrix(0.05F);
			param.matrix = sightMat;
			renderer.AddModel(sightModel1, param); // front pin
			
			// reload action
			reload *= 0.5F;
			leftHand = mat * Vector3(0.0F, 4.0F, 2.0F);
			
			if(reloading) {
				if(reload < 0.2F) {
					float per = reload / 0.2F;
					leftHand = Mix(leftHand, leftHand2,
						SmoothStep(per));
				}else if(reload < 0.35F){
					float per = (reload - 0.2F) / 0.15F;
					leftHand = Mix(leftHand2, leftHand3,
						SmoothStep(per));
				}else if(reload < 0.5F){
					float per = (reload - 0.35F) / 0.15F;
					leftHand = Mix(leftHand3, leftHand,
						SmoothStep(per));
				}
			}
			
			// motion blending parameter
			float cockFade = 1.0F;
			if(reloading){
				if(reload < 0.25F ||
					ammo < (clipSize - 1)) {
					cockFade = 0.0F;	
				}else{
					cockFade = (reload - 0.25F) * 10.0F;
					cockFade = Min(cockFade, 1.0F);
				}
			}
			
			if(cockFade > 0.0F){
				float cock = 0.0F;
				float tim = 1.0F - readyState;
				if(tim < 0.0F){
					// might be right after reloading
					if(ammo >= clipSize && reload > 0.5F && reload < 1.0F){
						tim = reload - 0.5F;
						if(tim < 0.05F){
							cock = 0.0F;
						}else if(tim < 0.12F){
							cock = (tim - 0.05F) / 0.07F;
						}else if(tim < 0.26F){
							cock = 1.0F;
						}else if(tim < 0.36F){
							cock = 1.0F - (tim - 0.26F) / 0.1F;
						}
					}
				}else if(tim < 0.2F){
					cock = 0.0F;
				}else if(tim < 0.3F){
					cock = (tim - 0.2F) / 0.1F;
				}else if(tim < 0.42F){
					cock = 1.0F;
				}else if(tim < 0.52F){
					cock = 1.0F - (tim - 0.42F) / 0.1F;
				}else{
					cock = 0.0F;
				}
				
				cock *= cockFade;
				mat = mat * CreateTranslateMatrix(0.0F, cock * -1.5F, 0.0F);
				
				leftHand = Mix(leftHand,
					mat * Vector3(0.0F, 4.0F, 2.0F), cockFade);
			}
			
			param.matrix = eyeMatrix * mat;
			renderer.AddModel(pumpModel, param);
			
			LeftHandPosition = leftHand;
			RightHandPosition = rightHand;
		}
	}
	
	IWeaponSkin@ CreateViewShotgunSkin(Renderer@ r, AudioDevice@ dev) {
		return ViewShotgunSkin(r, dev);
	}
}
