/*
 Copyright (c) 2013 yvt
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */
 
 namespace spades {
	class ViewRifleSkin: 
	IToolSkin, IViewToolSkin, IWeaponSkin,
	BasicViewWeapon {
		
		private AudioDevice@ audioDevice;
		private Model@ gunModel;
		private Model@ magazineModel;
		
		private AudioChunk@ fireSound;
		private AudioChunk@ fireFarSound;
		private AudioChunk@ fireStereoSound;
		private AudioChunk@ reloadSound;
		private Model@ sightModel1;
		private Model@ sightModel2;
		
		ViewRifleSkin(Renderer@ r, AudioDevice@ dev){
			super(r);
			@audioDevice = dev;
			@gunModel = renderer.RegisterModel
				("Models/Weapons/Rifle/WeaponNoMagazine.kv6");
			@magazineModel = renderer.RegisterModel
				("Models/Weapons/Rifle/Magazine.kv6");
			@sightModel1 = renderer.RegisterModel
				("Models/Weapons/Rifle/Sight1.kv6");
			@sightModel2 = renderer.RegisterModel
				("Models/Weapons/Rifle/Sight2.kv6");
			
			@fireSound = dev.RegisterSound
				("Sounds/Weapons/Rifle/FireLocal.wav");
			@fireFarSound = dev.RegisterSound
				("Sounds/Weapons/Rifle/FireFar.wav");
			@fireStereoSound = dev.RegisterSound
				("Sounds/Weapons/Rifle/FireStereo.wav");
			@reloadSound = dev.RegisterSound
				("Sounds/Weapons/Rifle/ReloadLocal.wav");
		}
		
		void Update(float dt) {
			BasicViewWeapon::Update(dt);
		}
		
		void WeaponFired(){
			BasicViewWeapon::WeaponFired();
			
			if(!IsMuted){
				Vector3 origin = Vector3(0.4F, -0.3F, 0.5F);
				AudioParam param;
				param.volume = 8.0F;
				audioDevice.PlayLocal(fireSound, origin, param);
				
				param.referenceDistance = 4.0F;
				param.volume = 1.0F;
				audioDevice.PlayLocal(fireFarSound, origin, param);
				param.referenceDistance = 1.0F;
				audioDevice.PlayLocal(fireStereoSound, origin, param);
			}
		}
		
		void ReloadingWeapon() {
			if(!IsMuted){
				Vector3 origin = Vector3(0.4F, -0.3F, 0.5F);
				AudioParam param;
				param.volume = 0.2F;
				audioDevice.PlayLocal(reloadSound, origin, param);
			}
		}
		
		float GetZPos() {
			return 0.2F - AimDownSightStateSmooth * 0.0520F;
		}
		
		// rotates gun matrix to ensure the sight is in
		// the center of screen (0, ?, 0).
		Matrix4 AdjustToAlignSight(Matrix4 mat, Vector3 sightPos, float fade) {
			Vector3 p = mat * sightPos;
			mat = CreateRotateMatrix(Vector3(0.0F, 0.0F, 1.0F), atan(p.x / p.y) * fade) * mat;
			mat = CreateRotateMatrix(Vector3(-1.0F, 0.0F, 0.0F), atan(p.z / p.y) * fade) * mat;
			return mat;
		}
		
		void Draw2D() {
			if(AimDownSightState > 0.6)
				return;
			BasicViewWeapon::Draw2D();
		}
		
		void AddToScene() {
			Matrix4 mat = CreateScaleMatrix(0.033F);
			mat = GetViewWeaponMatrix() * mat;
			
			bool reloading = IsReloading;
			float reload = ReloadProgress;
			Vector3 leftHand, rightHand;
			
			leftHand = mat * Vector3(1.0F, 6.0F, 1.0F);
			rightHand = mat * Vector3(0.0F, -8.0F, 2.0F);
			
			Vector3 leftHand2 = mat * Vector3(5.0F, -10.0F, 4.0F);
			Vector3 rightHand3 = mat * Vector3(-2.0F, -7.0F, -4.0F);
			Vector3 rightHand4 = mat * Vector3(-3.0F, -4.0F, -6.0F);
			
			if(AimDownSightStateSmooth > 0.8F){
				mat = AdjustToAlignSight(mat, Vector3(0.0F, 16.0F, -4.6F), (AimDownSightStateSmooth - 0.8F) / 0.2F);
			}
			
			ModelRenderParam param;
			Matrix4 weapMatrix = eyeMatrix * mat;
			param.matrix = weapMatrix * CreateScaleMatrix(0.5F) *
				CreateTranslateMatrix(-0.5F, 0.0F, 0.0F);
			param.depthHack = true;
			renderer.AddModel(gunModel, param);
			
			// draw sights
			Matrix4 sightMat = weapMatrix;
			sightMat *= CreateTranslateMatrix(0.0125F, -8.0F, -4.5F);
			sightMat *= CreateScaleMatrix(0.025F);
			param.matrix = sightMat;
			renderer.AddModel(sightModel1, param); // rear
			
			sightMat = weapMatrix;
			sightMat *= CreateTranslateMatrix(0.025F, 16.0F, -4.6F);
			sightMat *= CreateScaleMatrix(0.05F);
			param.matrix = sightMat;
			renderer.AddModel(sightModel2, param); // front pin
			
			// magazine/reload action
			mat *= CreateTranslateMatrix(0.0F, 1.0F, 1.0F);
			reload *= 2.5F;
			if(reloading) {
				if(reload < 0.1F){
					// move hand to magazine
					float per = reload / 0.1F;
					leftHand = Mix(leftHand,
						mat * Vector3(0.0F, 0.0F, 4.0F),
						SmoothStep(per));
				}else if(reload < 0.7F){
					// magazine release
					float per = (reload - 0.1F) / 0.6F;
					if(per < 0.2F){
						// non-smooth pull out
						per *= 4.0F; per -= 0.4F;
						per = Clamp(per, 0.0F, 0.2F);
					}
					if(per > 0.5F) {
						// when per = 0.5F, the hand no longer holds the magazine,
						// so the free fall starts
						per += per - 0.5F;
					}
					mat *= CreateTranslateMatrix(0.0F, 0.0F, per*per*10.0F);
					
					leftHand = mat * Vector3(0.0F, 0.0F, 4.0F);
					if(per > 0.5F){
						per = (per - 0.5F);
						leftHand = Mix(leftHand, leftHand2, SmoothStep(per));
					}
				}else if(reload < 1.4F) {
					// insert magazine
					float per = (1.4F - reload) / 0.7F;
					if(per < 0.3F) {
						// non-smooth insertion
						per *= 4.0F; per -= 0.4F;
						per = Clamp(per, 0.0F, 0.3F);
					}
					
					mat *= CreateTranslateMatrix(0.0F, 0.0F, per*per*10.0F);
					leftHand = mat * Vector3(0.0F, 0.0F, 4.0F);
				}else if(reload < 1.9F){
					// move the left hand to the original position
					// and start doing something with the right hand
					float per = (reload - 1.4F) / 0.5F;
					Vector3 orig = leftHand;
					leftHand = mat * Vector3(0.0F, 0.0F, 4.0F);
					leftHand = Mix(leftHand, orig, SmoothStep(per));
					rightHand = Mix(rightHand, rightHand3, SmoothStep(per));
				}else if(reload < 2.2F){
					float per = (reload - 1.9F) / 0.3F;
					rightHand = Mix(rightHand3, rightHand4, SmoothStep(per));
				}else{
					float per = (reload - 2.2F) / 0.3F;
					rightHand = Mix(rightHand4, rightHand, SmoothStep(per));
				}
			}
			
			param.matrix = eyeMatrix * mat;
			renderer.AddModel(magazineModel, param);
			
			LeftHandPosition = leftHand;
			RightHandPosition = rightHand;
		}
	}
	
	IWeaponSkin@ CreateViewRifleSkin(Renderer@ r, AudioDevice@ dev) {
		return ViewRifleSkin(r, dev);
	}
}
