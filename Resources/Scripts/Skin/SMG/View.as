/*
 Copyright (c) 2013 yvt
 
 This file is part of OpenSpades.
 
 OpenSpades is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 OpenSpades is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with OpenSpades.  If not, see <http://www.gnu.org/licenses/>.
 
 */
 
 namespace spades {
	class ViewSMGSkin: 
	IToolSkin, IViewToolSkin, IWeaponSkin,
	BasicViewWeapon {
		
		private AudioDevice@ audioDevice;
		private Model@ gunModel;
		private Model@ magazineModel;
		private Model@ sightModel1;
		private Model@ sightModel2;
		private Model@ sightModel3;
		
		private AudioChunk@[] fireSounds(4);
		private AudioChunk@ fireFarSound;
		private AudioChunk@ fireStereoSound;
		private AudioChunk@ reloadSound;
		
		ViewSMGSkin(Renderer@ r, AudioDevice@ dev){
			super(r);
			@audioDevice = dev;
			@gunModel = renderer.RegisterModel
				("Models/Weapons/SMG/WeaponNoMagazine.kv6");
			@magazineModel = renderer.RegisterModel
				("Models/Weapons/SMG/Magazine.kv6");
			@sightModel1 = renderer.RegisterModel
				("Models/Weapons/SMG/Sight1.kv6");
			@sightModel2 = renderer.RegisterModel
				("Models/Weapons/SMG/Sight2.kv6");
			@sightModel3 = renderer.RegisterModel
				("Models/Weapons/SMG/Sight3.kv6");
				
			@fireSounds[0] = dev.RegisterSound
				("Sounds/Weapons/SMG/FireLocal1.wav");
			@fireSounds[1] = dev.RegisterSound
				("Sounds/Weapons/SMG/FireLocal2.wav");
			@fireSounds[2] = dev.RegisterSound
				("Sounds/Weapons/SMG/FireLocal3.wav");
			@fireSounds[3] = dev.RegisterSound
				("Sounds/Weapons/SMG/FireLocal4.wav");
			@fireFarSound = dev.RegisterSound
				("Sounds/Weapons/SMG/FireFar.wav");
			@fireStereoSound = dev.RegisterSound
				("Sounds/Weapons/SMG/FireStereo.wav");
			@reloadSound = dev.RegisterSound
				("Sounds/Weapons/SMG/ReloadLocal.wav");
				
		}
		
		void Update(float dt) {
			BasicViewWeapon::Update(dt);
		}
		
		void WeaponFired(){
			BasicViewWeapon::WeaponFired();
			
			if(!IsMuted){
				Vector3 origin = Vector3(0.4F, -0.3F, 0.5F);
				AudioParam param;
				param.volume = 8.0F;
				audioDevice.PlayLocal(fireSounds[GetRandom(fireSounds.length)], origin, param);
				
				param.volume = 4.0F;
				audioDevice.PlayLocal(fireFarSound, origin, param);
				param.volume = 1.0F;
				audioDevice.PlayLocal(fireStereoSound, origin, param);
				
			}
		}
		
		void ReloadingWeapon() {
			if(!IsMuted){
				Vector3 origin = Vector3(0.4F, -0.3F, 0.5F);
				AudioParam param;
				param.volume = 0.2F;
				audioDevice.PlayLocal(reloadSound, origin, param);
			}
		}
		
		float GetZPos() {
			return 0.2F - AimDownSightStateSmooth * 0.038F;
		}
		
		// rotates gun matrix to ensure the sight is in
		// the center of screen (0, ?, 0).
		Matrix4 AdjustToAlignSight(Matrix4 mat, Vector3 sightPos, float fade) {
			Vector3 p = mat * sightPos;
			mat = CreateRotateMatrix(Vector3(0.0F, 0.0F, 1.0F), atan(p.x / p.y) * fade) * mat;
			mat = CreateRotateMatrix(Vector3(-1.0F, 0.0F, 0.0F), atan(p.z / p.y) * fade) * mat;
			return mat;
		}
		
		void Draw2D() {
			if(AimDownSightState > 0.6)
				return;
			BasicViewWeapon::Draw2D();
		}
		
		void AddToScene() {
			Matrix4 mat = CreateScaleMatrix(0.033F);
			mat = GetViewWeaponMatrix() * mat;
			
			bool reloading = IsReloading;
			float reload = ReloadProgress;
			Vector3 leftHand, rightHand;
			
			leftHand = mat * Vector3(1.0F, 6.0F, 1.0F);
			rightHand = mat * Vector3(0.0F, -8.0F, 2.0F);
			
			Vector3 leftHand2 = mat * Vector3(5.0F, -10.0F, 4.0F);
			Vector3 leftHand3 = mat * Vector3(1.0F, 6.0F, -4.0F);
			Vector3 leftHand4 = mat * Vector3(1.0F, 9.0F, -6.0F);
			
			if(AimDownSightStateSmooth > 0.8F){
				mat = AdjustToAlignSight(mat, Vector3(0.0F, 5.0F, -4.9F), (AimDownSightStateSmooth - 0.8F) / 0.2F);
			}
			
			ModelRenderParam param;
			Matrix4 weapMatrix = eyeMatrix * mat;
			param.matrix = weapMatrix;
			param.depthHack = true;
			renderer.AddModel(gunModel, param);
			
			// draw sights
			Matrix4 sightMat = weapMatrix;
			sightMat *= CreateTranslateMatrix(0.05F, 5.0F, -4.85F);
			sightMat *= CreateScaleMatrix(0.1F);
			param.matrix = sightMat;
			renderer.AddModel(sightModel1, param); // front
			
			sightMat = weapMatrix;
			sightMat *= CreateTranslateMatrix(0.025F, 5.0F, -4.85F);
			sightMat *= CreateScaleMatrix(0.05F);
			param.matrix = sightMat;
			renderer.AddModel(sightModel3, param); // front pin
			
			sightMat = weapMatrix;
			sightMat *= CreateTranslateMatrix(0.04F, -9.0F, -4.9F);
			sightMat *= CreateScaleMatrix(0.08F);
			param.matrix = sightMat;
			renderer.AddModel(sightModel2, param); // rear
			
			// magazine/reload action
			mat *= CreateTranslateMatrix(0.0F, 3.0F, 1.0F);
			reload *= 2.5F;
			if(reloading) {
				if(reload < 0.7F){
					// magazine release
					float per = reload / 0.7F;
					mat *= CreateTranslateMatrix(0.0F, 0.0F, per*per*50.0F);
					leftHand = Mix(leftHand, leftHand2, SmoothStep(per));
				}else if(reload < 1.4F) {
					// insert magazine
					float per = (1.4F - reload) / 0.7F;
					if(per < 0.3F) {
						// non-smooth insertion
						per *= 4.0F; per -= 0.4F;
						per = Clamp(per, 0.0F, 0.3F);
					}
					
					mat *= CreateTranslateMatrix(0.0F, 0.0F, per*per*10.0F);
					leftHand = mat * Vector3(0.0F, 0.0F, 4.0F);
				}else if(reload < 1.9F){
					// move the left hand to the original position
					// and start doing something with the right hand
					float per = (reload - 1.4F) / 0.5F;
					leftHand = mat * Vector3(0.0F, 0.0F, 4.0F);
					leftHand = Mix(leftHand, leftHand3, SmoothStep(per));
				}else if(reload < 2.2F){
					float per = (reload - 1.9F) / 0.3F;
					leftHand = Mix(leftHand3, leftHand4, SmoothStep(per));
				}else{
					float per = (reload - 2.2F) / 0.3F;
					leftHand = Mix(leftHand4, leftHand, SmoothStep(per));
				}
			}
			
			param.matrix = eyeMatrix * mat;
			renderer.AddModel(magazineModel, param);
			
			LeftHandPosition = leftHand;
			RightHandPosition = rightHand;
		}
		
	}
	
	IWeaponSkin@ CreateViewSMGSkin(Renderer@ r, AudioDevice@ dev) {
		return ViewSMGSkin(r, dev);
	}
}
